#include "headers/MiniComputer.hpp"

int
main()
{
    /// Create and run MiniComputer
    MiniComputer program("Mini", "1.0");
    return program.run();
}

