#include "../headers/MiniComputer.hpp"

#include <iostream>
#include <unistd.h>
#include <cstdlib>
#include <cassert>

MiniComputer::MiniComputer(std::string programName, std::string version)
{
    setProgramName(programName);
    setVersion(version);
}

int
MiniComputer::run()
{
    /// Print program name and version
    printProgramNameAndVersion();

    int command = -1;
    while (command != 0) {
        /// Print main menu
        printMenu(MAIN_MENU);

        /// Get command from user
        command = getCommandFromUser();
        validateCommand(command);

        /// Execute the command
        executeCommand(command);
    } /// Start again from main menu
    return 0;
}

std::string
MiniComputer::getProgramName()
{
    return programName_;
}

std::string
MiniComputer::getVersion()
{
    return version_;
}

void
MiniComputer::setProgramName(std::string programName)
{
    programName_ = programName;
}

void
MiniComputer::setVersion(std::string version)
{
    version_ = version;
}

void
MiniComputer::printProgramNameAndVersion()
{
    std::cout << getProgramName() << ' ' << getVersion() << std::endl;
}

void
MiniComputer::printMenu(Menu menu)
{
    if (::isatty(STDIN_FILENO)) {
        static const std::string header[MENU_SIZE] = {
            "Main Menu", "Print Menu", "Load (into register)",
            "Add Menu (Register 1 + Register 2 = Register 3)"
        };
        static const unsigned int size[MENU_SIZE] = { 5, 2, 4, 4 };
        static const std::string menuItem[MENU_SIZE][5] = {
            { "exit", "load", "store", "print", "add" },
            { "register", "string" },
            { "a", "b", "c", "d" },
            { "a", "b", "c", "d" }
        };
        static const std::string prompt[MENU_SIZE] = { "> Command", "> Choose", "> Register", "" };
        std::cout << header[menu] << ":\n";
        for (unsigned int command = 0; command < size[menu]; ++command) {
            printMenuItem(menuItem[menu], command);
        }
        if (!prompt[menu].empty()) {
            std::cout << prompt[menu] << ": ";
        }
    }
}

void
MiniComputer::printMenuItem(const std::string menuItem[], const unsigned int command)
{

    std::cout << '\t' << command << " - " << menuItem[command] << std::endl;
}

int
MiniComputer::getCommandFromUser()
{
    int command;
    std::cin >> command;
    return command;
}

void
MiniComputer::executeCommand(const int command)
{
    assert(command >= 0);
    assert(command <= 4);
    switch (command) {
    case 0: executeExitCommand();  break;
    case 1: executeLoadCommand();  break;
    case 2: executeStoreCommand(); break;
    case 3: executePrintCommand(); break;
    case 4: executeAddCommand();   break;
    }
}

void
MiniComputer::validateCommand(const int command)
{
    if (command < 0) {
        std::cout << "Error 1: Invalid Command!" << std::endl;
	    ::exit(1);
    }
    if (command > 4) {
	    std::cout << "Error 1: Invalid Command!" << std::endl;
	    ::exit(1);
    }
}

void
MiniComputer::validatePrint(const int command)
{
    if (command < 0) {
        std::cout << "Error 2: Invalid Print Command!" << std::endl;
	    ::exit(2);
    }
    if (command > 1) {
	    std::cout << "Error 2: Invalid Print Command!" << std::endl;
	    ::exit(2);
    }
}

void
MiniComputer::validateLoad(const int command)
{
    if (command < 0) {
        std::cout << "Error 3: Invalid Load Register!" << std::endl;
	    ::exit(3);
    }
    if (command > 3) {
	    std::cout << "Error 3: Invalid Load Register!" << std::endl;
	    ::exit(3);
    }
}

void
MiniComputer::executeExitCommand()
{
    std::cout << "Exiting..." << std::endl;
}

void
MiniComputer::executeLoadCommand()
{
    printMenu(LOAD_MENU);
    const int registerNumber = getCommandFromUser();
    validateLoad(registerNumber);
    executeLoadRegister(registerNumber);
}

void
MiniComputer::executeStoreCommand()
{
    /// Not implemented yet
    ::abort();
}

void
MiniComputer::executePrintCommand()
{
    printMenu(PRINT_MENU);
    int print = getCommandFromUser();
    validatePrint(print);
     0 == print ? executePrintRegister() : executePrintString();
}

void
MiniComputer::executePrintRegister()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Registers" << std::endl;
    }
    const std::string registers[4] = { "a", "b", "c", "d" };
    if (::isatty(STDIN_FILENO)) {   
        for (int command = 0; command < 4; ++command) {
            printMenuItem(registers, command);
        }
    }
    if (::isatty(STDIN_FILENO)) {
        std::cout << "> Register: ";
    }
    int registerNumber = getCommandFromUser();
    validateCommand(registerNumber);
    int registerValue = getRegisterValue(registerNumber);
    std::cout << registerValue << std::endl;
    
}

void
MiniComputer::executePrintString()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "> String Length: ";
    }
    const int length = getCommandFromUser();
    char* string1 = new char[length];
    if (::isatty(STDIN_FILENO)) {
        std::cout << "> String: ";
    }
    std::cin.ignore();
    std::cin.getline(string1, length);
//    std::cin.ignore();
    std::cout << string1;
    delete [] string1;
}

void
MiniComputer::executeAddCommand()
{
    printMenu(ADD_MENU);

    if (::isatty(STDIN_FILENO)) {
        std::cout << "Register 1: ";
    }
    const int register1Number = getCommandFromUser();
    validateLoad(register1Number);
    
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Register 2: ";
    }
    const int register2Number = getCommandFromUser();
    validateLoad(register2Number);
    
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Register 3: ";
    }
    const int register3Number = getCommandFromUser();
    validateLoad(register3Number);

    const int register1Value = getRegisterValue(register1Number);
    const int register2Value = getRegisterValue(register2Number);
    const int register3Value = register1Value + register2Value;
    setRegisterValue(register3Number, register3Value);
    
    std::cout << getRegisterName(register3Number) << " = "
	      << getRegisterName(register1Number) << " + "
	      << getRegisterName(register2Number) << " = "
	      << register1Value << " + " << register2Value << " = "
	      << register3Value << std::endl;
}

char
MiniComputer::getRegisterName(const int registerNumber)
{
    return static_cast<char>('a' + registerNumber);
}

void
MiniComputer::setRegisterValue(const int registerNumber, const int registerValue) 
{
    assert(registerNumber >=0 && registerNumber < REGISTER_COUNT);
    registers_[registerNumber] = registerValue;
}

int
MiniComputer::getRegisterValue(const int registerNumber)
{
    assert(registerNumber >=0 && registerNumber < REGISTER_COUNT);
    return registers_[registerNumber];
}

void
MiniComputer::executeLoadRegister(const int registerNumber)
{
    char nameOfRegister = getRegisterName(registerNumber);
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Load into " << nameOfRegister 
                << "\n\tInput the value to load into register "
                << nameOfRegister << "." << std::endl;
        std::cout << nameOfRegister << ": ";
    }
    int value = getCommandFromUser();
    setRegisterValue(registerNumber, value);
    std::cout << nameOfRegister << " = " << value << std::endl;
}

