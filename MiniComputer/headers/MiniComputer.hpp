#ifndef __MINI_COMPUTER_HPP__
#define __MINI_COMPUTER_HPP__

#include <string>

const int REGISTER_COUNT = 4;

class MiniComputer
{
private:
    enum Menu {
        MAIN_MENU,
        PRINT_MENU,
        LOAD_MENU,
        ADD_MENU,
        MENU_SIZE
    };

public:
    MiniComputer(std::string programName, std::string version);
    int run();

private:
    void setProgramName(std::string programName);
    std::string getProgramName();

    void setVersion(std::string version);
    std::string getVersion();

    void printProgramNameAndVersion();
    void printMenu(Menu menu);
    void printMenuItem(const std::string menuItem[], const unsigned int command);
    int getCommandFromUser();
    void executeCommand(int command);
    void validateCommand(int command);
    void validatePrint(int command);
    void validateLoad(int command);
    void executeExitCommand();
    void executeLoadCommand();
    void executeStoreCommand();
    void executePrintCommand();
    void executeAddCommand();
    void executePrintRegister();
    void executePrintString();
//    void printRegisterName(int registerNumber);
    void setRegisterValue(const int registerNumber, const int registerValue);
    int getRegisterValue(int registerNumber);
    char getRegisterName(const int registerNumber);
    void executeLoadRegister(int registerNumber);
private:
    std::string programName_;
    std::string version_;
    int registers_[REGISTER_COUNT];

};

#endif

