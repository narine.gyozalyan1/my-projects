#include "tictactoe.h"

#include <QPushButton>
#include <QLabel>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QMessageBox>

TicTacToe::TicTacToe()
    : QDialog()
{
    for (int i = 0; i < 3; ++i) {
        for (int j = 0; j < 3; ++j) {
            board[i][j] = new QPushButton(" ");
            board[i][j]->setStyleSheet("background-color: #FF9100; color: black");
        }
    }
    turn_is_ = new QLabel("It's turn of: ");
    symbol_str_ = "X";
    current_symbol_ = new QLabel(symbol_str_);
    winner_is_ = new QLabel("The winner is: ");
    winner_is_->hide();
    winner_symbol_ = new QLabel(current_symbol_);
    winner_is_->hide();
    exit_ = new QPushButton("Exit");
    exit_->setStyleSheet("background-color: #B62A00");
    exit_->show();
    play_again_ = new QPushButton("Play again");
    play_again_->setStyleSheet("background-color: #73D449");
    play_again_->show();
    setWindowTitle("Tic-Tac-Toe");
    set_layout();
    set_up_connections();
}

void
TicTacToe::on_button_clicked()
{
    QPushButton* clicked_button = dynamic_cast<QPushButton*>(sender());
    clicked_button->setText(symbol_str_);
    clicked_button->setEnabled(false);
    if (is_winner()) {
        winner_symbol_->setText(symbol_str_);
        winner_is_->show();
        winner_symbol_->show();
        disable_buttons();
        return;
    }
    if (are_all_disabled()) {
        winner_symbol_->setText("DRAW!");
        winner_symbol_->show();
        return;
    }
    ("X" == symbol_str_) ? symbol_str_ = "O" : symbol_str_ = "X";
    current_symbol_->setText(symbol_str_);
}

bool
TicTacToe::is_winner()
{
    if ((" " != board[0][0]->text()) && board[0][0]->text() == board[1][1]->text() && board[0][0]->text() == board[2][2]->text()) {
        return true;
    }
    if ((" " != board[0][0]->text()) && board[0][0]->text() == board[0][1]->text() && board[0][0]->text() == board[0][2]->text()) {
        return true;
    }
    if ((" " != board[0][0]->text()) && board[0][0]->text() == board[1][0]->text() && board[0][0]->text() == board[2][0]->text()) {
        return true;
    }
    if ((" " != board[1][1]->text()) && board[1][1]->text() == board[1][0]->text() && board[1][1]->text() == board[1][2]->text()) {
        return true;
    }
    if ((" " != board[1][1]->text()) && board[1][1]->text() == board[0][1]->text() && board[1][1]->text() == board[2][1]->text()) {
        return true;
    }
    if ((" " != board[1][1]->text()) && board[1][1]->text() == board[0][2]->text() && board[1][1]->text() == board[2][0]->text()) {
        return true;
    }
    if ((" " != board[2][2]->text()) && board[2][2]->text() == board[1][2]->text() && board[2][2]->text() == board[0][2]->text()) {
        return true;
    }
    if ((" " != board[2][2]->text()) && board[2][2]->text() == board[2][1]->text() && board[2][2]->text() == board[2][0]->text()) {
        return true;
    }
    return false;
}

void
TicTacToe::set_up_connections()
{
    for (int i = 0; i < 3; ++i) {
        for (int j = 0; j < 3; ++j) {
            connect(board[i][j], SIGNAL(clicked(bool)), this, SLOT(on_button_clicked()));
        }
    }
    connect(exit_, SIGNAL(clicked()), this, SLOT(open_message_for_exit()));
    connect(play_again_, SIGNAL(clicked()), this, SLOT(open_message_for_play_again()));
}

void
TicTacToe::open_message_for_exit()
{
    QMessageBox::StandardButton reply = QMessageBox::warning(this
                                                             , "Exit..."
                                                             , "Do you really want to quit?"
                                                             , QMessageBox::Yes | QMessageBox::No);
    if (reply == QMessageBox::Yes) {
        QApplication::quit();
    }
}

void
TicTacToe::open_message_for_play_again()
{
    QMessageBox::StandardButton reply = QMessageBox::warning(this
                                                             , "Play Again..."
                                                             , "Do you really want to start again?"
                                                             , QMessageBox::Yes | QMessageBox::No);
    if (reply == QMessageBox::Yes) {
        symbol_str_ = "X";
        current_symbol_->setText(symbol_str_);
        for (int i = 0; i < 3; ++i) {
            for (int j = 0; j < 3; ++j) {
                board[i][j]->setText(" ");
                board[i][j]->setEnabled(true);
            }
        }
        winner_is_->hide();
        winner_symbol_->hide();
    }
}

void
TicTacToe::disable_buttons()
{
    for (int i = 0; i < 3; ++i) {
        for (int j = 0; j < 3; ++j) {
            board[i][j]->setEnabled(false);
        }
    }
}

bool
TicTacToe::are_all_disabled()
{
    for (int i = 0; i < 3; ++i) {
        for (int j = 0; j < 3; ++j) {
            if (board[i][j]->isEnabled()) {
                return false;
            }
        }
    }
    return true;
}

void
TicTacToe::set_layout()
{
    QHBoxLayout* top = new QHBoxLayout;
    top->addWidget(turn_is_);
    top->addWidget(current_symbol_);

    QGridLayout* gl = new QGridLayout;
    gl->addWidget(board[0][0], 0, 0, 1, 1);
    gl->addWidget(board[0][1], 0, 1, 1, 1);
    gl->addWidget(board[0][2], 0, 2, 1, 1);
    gl->addWidget(board[1][0], 1, 0, 1, 1);
    gl->addWidget(board[1][1], 1, 1, 1, 1);
    gl->addWidget(board[1][2], 1, 2, 1, 1);
    gl->addWidget(board[2][0], 2, 0, 1, 1);
    gl->addWidget(board[2][1], 2, 1, 1, 1);
    gl->addWidget(board[2][2], 2, 2, 1, 1);

    QHBoxLayout* winner = new QHBoxLayout;
    winner->addWidget(winner_is_);
    winner->addWidget(winner_symbol_);

    QHBoxLayout* exit_play_again = new QHBoxLayout;
    exit_play_again->addWidget(play_again_);
    exit_play_again->addWidget(exit_);

    QVBoxLayout* lyt = new QVBoxLayout;
    lyt->addLayout(top);
    lyt->addLayout(gl);
    lyt->addStretch(1);
    lyt->addLayout(winner);
    lyt->addLayout(exit_play_again);

    setLayout(lyt);
}
