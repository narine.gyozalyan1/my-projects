#include "tictactoe.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    TicTacToe t;
    t.show();
    return app.exec();
}
