#ifndef TICTACTOE_H
#define TICTACTOE_H

#include <QDialog>
#include <QApplication>

class QPushButton;
class QLabel;
class QString;

class TicTacToe : public QDialog
{
    Q_OBJECT

public:
    TicTacToe();

private slots:
    void on_button_clicked();
    void open_message_for_exit();
    void open_message_for_play_again();
private:
    void set_layout();
    void set_up_connections();
    bool is_winner();
    void disable_buttons();
    bool are_all_disabled();

private:
    QLabel* turn_is_;
    QLabel* winner_is_;
    QLabel* current_symbol_;
    QLabel* winner_symbol_;
    QPushButton* board[3][3];
    QPushButton* exit_;
    QPushButton* play_again_;
    QString symbol_str_;
};
#endif // TICTACTOE_H
