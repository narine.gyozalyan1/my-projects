import { configureStore } from "@reduxjs/toolkit";
import TableReducer from "./features/TableSlice";
import AuthSlice from "./features/AuthSlice";
import SidebarSlice from "./features/SidebarSlice";
export default configureStore({
    reducer: {
        dataTable: TableReducer,
        sidebarAction: SidebarSlice,
        loginData: AuthSlice
    },
});

