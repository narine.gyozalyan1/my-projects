import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import API from "../api";

//Extra Reducers
export const getDataForList = createAsyncThunk(
    "tableData/getDataForList",
    async (url, { thunkAPI }) => {
        try {
            const response = await API.get('/'+url);
            return response.data;
        } catch (err) {
            return thunkAPI.rejectWithValue(err.response.data);
        }
    }
);

//PARAMETERS
// A name, used in action types
const name = "tableData";

// The initial state for the reducer
const initialState = {
    loading: false,
    error: "",
    data: [],
    pageNumber: 0,
    totalPages:0,
    tableHeaders: [],
    onCitizenPage: false
};

// An object of "case reducers". Key names will be used to generate actions.
const reducers = {
    changePage: (state, action) => {
        state.pageNumber = action.payload;
    },
    showCitizenList: (state) => {
        state.onCitizenPage = true;
    }
};


//Slice declaration
const tableSlice = createSlice({
    name: name,
    initialState: initialState,
    reducers: reducers,
    extraReducers: (builder) => {
        builder.addCase(getDataForList.pending, (state) => {
            state.loading = true;
        }).addCase(getDataForList.fulfilled, (state, action)=>{
                state.loading = false;
                state.data = action.payload.content;
                state.totalPages = action.payload.totalPages;
                state.tableHeaders = Object.keys(action.payload.content[0]);
        }).addCase(getDataForList.rejected, (state, action)=>{
                state.loading = false;
                state.error = action.payload.error;
        })
    }
});



export const { changePage, showCitizenList } = tableSlice.actions;
export default tableSlice.reducer;