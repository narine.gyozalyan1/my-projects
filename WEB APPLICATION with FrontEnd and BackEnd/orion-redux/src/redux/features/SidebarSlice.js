import { createSlice } from "@reduxjs/toolkit";

//Extra Reducers
//is empty

//PARAMETERS
// A name, used in action types
const name = "sidebarAction";

// The initial state for the reducer
const initialState = {
    isOpen: true,
    darkModeOn: false

};

// An object of "case reducers". Key names will be used to generate actions.
const reducers = {
    toggleSidebar: (state) => {
        state.isOpen = !state.isOpen;
    },
    darkMode: (state) => {
        state.darkModeOn = !state.darkModeOn;
        document.body.classList.toggle("dark");
    }
};

//Slice declaration
const sidebarSlice = createSlice({
    name: name,
    initialState: initialState,
    reducers: reducers
});

export const {toggleSidebar, darkMode} = sidebarSlice.actions;
export default sidebarSlice.reducer;