import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import API from "../api";

//Extra Reducers
export const logIn = createAsyncThunk(
    "auth/logIn",
    async (userdata, { thunkAPI }) => {
        try {
            const response = await API.post('/login', userdata);
            return response.data;
        } catch (err) {
            return thunkAPI.rejectWithValue(err.response);
        }
    }
);

export const logOut = createAsyncThunk(
    "auth/logOut",
    async (data, { thunkAPI }) => {
        try {
            const response = await API.post('/logout', data);
            return response.data;
        } catch (err) {
            return thunkAPI.rejectWithValue(err.response);
        }
    }
);

//PARAMETERS
// A name, used in action types
const name = "auth";

// The initial state for the reducer
const initialState = {
    isLoggedIn: false,
    loading: false,
    user: null,
    error: ""
};

// An object of "case reducers". Key names will be used to generate actions.
const reducers = {
    testMethod: (state) => {
        debugger;
        console.log(state.isLoggedIn);
    }
};


const authSlice = createSlice({
    name: name,
    initialState: initialState,
    reducers: reducers,
    extraReducers: (builder)=> {
        builder.addCase(logIn.pending, (state) => {
            state.loading = true;
        }).addCase(logIn.fulfilled, (state, action) => {
            state.loading = false;
            state.isLoggedIn = action.payload.loggedIn;
            state.user = action.payload;
            console.log('User successfully logged in');
            debugger;
        }).addCase(logIn.rejected, (state, action) => {
            state.loading = false;
            state.error = action.error.message;
        }).addCase(logOut.pending, (state) => {
            state.loading = true;
        }).addCase(logOut.fulfilled, (state, action) => {
            state.loading = false;
            state.isLoggedIn = action.payload.loggedIn;
            state.user = action.payload;
            console.log('User successfully logged out');
        }).addCase(logOut.rejected, (state, action) => {
            state.loading = false;
            state.error = action.error.message;
        })
    },
});

export const {testMethod} =  authSlice.actions

export default authSlice.reducer;