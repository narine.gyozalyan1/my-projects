import React, {useRef} from 'react';
import {useDispatch} from "react-redux";
import {logIn, logOut} from "../../redux/features/AuthSlice";

export default function Login() {
    const dispatch = useDispatch();
    const  usernameInput = useRef("");
    const  passwordInput = useRef("");

    const handleSubmit = (event) => {
        event.preventDefault();

        let data = {
            username: usernameInput.current.value,
            password: passwordInput.current.value,
        }
        dispatch(logIn(data));

    };

    const handleLogOut = () => {
        dispatch(logOut(null));
    };
    return (
        <div className="login-container">
            <form onSubmit={(event) => {handleSubmit(event)}}>
                <h2>Login</h2>
                <div className="form-item">
                    <input
                        placeholder="Username"
                        type="text"
                        id="username"
                        ref={usernameInput}
                    />
                </div>
                <div className="form-item">
                    <input
                        placeholder="Password"
                        type="password"
                        id="password"
                        ref={passwordInput}
                        required
                    />
                </div>
                <div className="form-item">
                    <input type="submit" id="login-button" value="Save"/>
                </div>
            </form>
            <a href="#" onClick={()=>{handleLogOut()}}>Log Out</a>
        </div>
    );
}