import React from "react";
import {Link} from "react-router-dom";
export default function NotFoundPage(){
    return(
        <>
            <div>Invalid path. Try without 's'</div>
            <Link to="/home">Home</Link>
            <br></br>
            {/*<a href="/">Car</a>*/}

            <Link to="/citizen">Citizen</Link>
        </>
    )
}