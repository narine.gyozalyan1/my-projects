import React from "react";
import LicenseList from "../../components/licenses/LicenseList";
import Navigation from "../../components/Navigation";
export default function Licenses() {

    return (
        <>
            <Navigation />
            <LicenseList />
        </>
    )

}