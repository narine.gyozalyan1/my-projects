import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { Provider } from 'react-redux';
import store  from './redux/store';
import {createBrowserRouter, RouterProvider} from "react-router-dom";
import Citizens from "./pages/citizens/Citizens";
import NotFoundPage from "./pages/NotFoundPage";


// const router = createBrowserRouter([
//     {
//         path: '/',
//         element: <Header/>,
//         errorElement: <NotFoundPage/>,
//         children:[
//             {
//                 path: '/citizen',
//                 element: <Citizens/>
//             },
//         ]
//
//     },{
//         path: '/home',
//         element: <Header/>,
//         errorElement: <NotFoundPage/>,
//     },
//     // {
//     //     path: '/citizen',
//     //     element: <Citizens/>,
//     // }
// ]);
const root = ReactDOM.createRoot(document.getElementById('root'));
// root.render(
//         <Provider store={store}>
//             <RouterProvider router={router}/>
//         </Provider>
// );
root.render(
    <Provider store={store}>
        <App/>
    </Provider>
);



// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
