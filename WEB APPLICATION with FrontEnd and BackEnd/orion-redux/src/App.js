import './App.css';
import './style.css';
import Citizens from "./pages/citizens/Citizens";
import {BrowserRouter, Route, Routes} from "react-router-dom";
import Cars from "./pages/cars/Cars";
import Home from "./pages/home/Home";
import Licenses from "./pages/licenses/Licenses";
import Login from "./pages/login/Login";
import Sidebar from "./components/Sidebar/Siderbar";

function App() {

    return (
    <>
        <BrowserRouter>
            <Sidebar />
            <Routes>
                <Route path="/">
                    <Route path="login" element={<Login />} />
                    <Route path="*" element={<Home />} />
                    <Route path="citizens" element={<Citizens />} />
                    <Route path="cars" element={<Cars />} />
                    <Route path="licenses" element={<Licenses />} />
                </Route>
            </Routes>
        </BrowserRouter>
    </>
  );
}
export default App;
