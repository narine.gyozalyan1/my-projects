import React from "react";
import BottomContent from "./ButtomContent/ButtomContent";
import Menu from "./Menu/Menu";

export default function MenuBar() {
    return(
        <>
            <div className="menu-bar">
                <Menu/>
                <BottomContent/>
            </div>
        </>
    );
}