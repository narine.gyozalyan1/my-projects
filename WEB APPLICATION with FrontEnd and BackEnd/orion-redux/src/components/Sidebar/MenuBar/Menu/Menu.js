import React from "react";

export default function Menu(){
    return(
        <>
            <div className="menu">
                <ol>
                    <li className="search-box">
                        <i className='bx bx-search icon'></i>
                        <input type="search" placeholder="Search..."></input>
                    </li>
                </ol>
                <ul className="manu-links">
                    <li className="">
                        <a href="#">
                            <i className='bx bx-home-alt icon'></i>
                            <span className="text nav-text">Dashboard</span>
                        </a>
                    </li>
                    <li className="">
                        <a href="#">
                            <i className='bx bx-bar-chart-alt-2 icon'></i>
                            <span className="text nav-text">Revenue</span>
                        </a>
                    </li>
                    <li className="">
                        <a href="#">
                            <i className='bx bx-bell icon'></i>
                            <span className="text nav-text">Notifications</span>
                        </a>
                    </li>
                    <li className="">
                        <a href="#">
                            <i className='bx bx-pie-chart-alt icon'></i>
                            <span className="text nav-text">Analytics</span>
                        </a>
                    </li>
                    <li className="">
                        <a href="#">
                            <i className='bx bx-home-alt icon'></i>
                            <span className="text nav-text">Likes</span>
                        </a>
                    </li>
                    <li className="">
                        <a href="#">
                            <i className='bx bx-wallet icon'></i>
                            <span className="text nav-text">Wallets</span>
                        </a>
                    </li>
                </ul>
            </div>
        </>
    )
}