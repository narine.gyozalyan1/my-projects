import React from "react";
import {useDispatch, useSelector} from "react-redux";
import {darkMode} from "../../../../redux/features/SidebarSlice";

export default function ButtomContent(){
    const dispatch = useDispatch();
    const handleModeSwitchClick = () => {
        dispatch(darkMode());

    }

    return(
        <>
            <div className="buttom-content">
                <ul className="manu-links">
                    <li className="">
                        <a href="#">
                            <i className='bx bx-log-out icon'></i>
                            <span className="text nav-text">Logout</span>
                        </a>
                    </li>

                    <li className="mode">
                        <div className="moon-sun">
                            <i className='bx bx-moon icon moon'></i>
                            <i className='bx bx-sun icon sun'></i>
                        </div>
                        <span className="mode-text text">Dark Mode</span>

                        <div className="toggle-switch" onClick={()=>{handleModeSwitchClick()}}>
                            <span className="switch"></span>
                        </div>
                    </li>
                </ul>
            </div>
        </>
    )
}