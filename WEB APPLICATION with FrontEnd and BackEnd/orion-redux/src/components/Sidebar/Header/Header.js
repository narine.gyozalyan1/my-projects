import React from "react";
import {useDispatch} from "react-redux";
import {toggleSidebar} from "../../../redux/features/SidebarSlice";

export default function Header(){
    const dispatch = useDispatch();
    const toggleSidebarClick = () => {
        dispatch(toggleSidebar());
    };
    return(
        <>
            <header>
                <div className="image-text">
                        <span className="image">
                            {/*<img src="Logo_of_Twitter.svg.webp" alt="logo">*/}
                        </span>
                    <div className="text header-text">
                        <span className="name">CodingLab</span>
                        <span className="profession">Web developer</span>
                    </div>
                </div>
                <i className='bx bx-chevron-right toggle'  onClick={()=>{toggleSidebarClick()}}></i>
            </header>
        </>
    )
}