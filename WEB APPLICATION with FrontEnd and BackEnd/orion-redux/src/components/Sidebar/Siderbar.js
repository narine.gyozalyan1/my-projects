import React from "react";
import {useSelector} from "react-redux";
import Header from "./Header/Header";
import MenuBar from "./MenuBar/MenuBar";


export default function Sidebar() {
    const {isOpen} = useSelector((state) => state.sidebarAction);

    return (
        <>
            <nav className={`sidebar ${isOpen ? '' : 'close-sidebar'}`}>
                <Header/>
                <MenuBar/>
            </nav>
        </>
);
}