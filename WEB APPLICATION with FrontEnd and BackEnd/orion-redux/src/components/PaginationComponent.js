import React from 'react';
import { Pagination } from 'react-bootstrap';
import {useDispatch, useSelector} from "react-redux";
import {changePage} from "../redux/features/TableSlice";

function PaginationComponent() {

    const {pageNumber, totalPages} = useSelector((state) => state.dataTable);
    const dispatch = useDispatch();
    const handlePageClick = (index) => {
        dispatch(changePage(index));
    };


    return (
        <Pagination className="mb-3">
            <Pagination.First
                onClick={() => handlePageClick(0)}
                disabled={pageNumber === 0}
            />
            <Pagination.Prev
                disabled={pageNumber === 0}
                onClick={() => handlePageClick(pageNumber-1)}
            />

            {Array.from({length: totalPages}).map((_, index) => (
                <Pagination.Item
                    key={index}
                    active={index === pageNumber}
                    onClick={() => handlePageClick(index)}
                >
                    {index + 1}
                </Pagination.Item>
            ))}

            <Pagination.Next
                onClick={() => handlePageClick(pageNumber+1)}
                disabled={pageNumber === totalPages - 1}
            />
            <Pagination.Last
                onClick={() => handlePageClick(totalPages - 1)}
                disabled={pageNumber === totalPages - 1}
            />
        </Pagination>
    );
}

export default PaginationComponent;
