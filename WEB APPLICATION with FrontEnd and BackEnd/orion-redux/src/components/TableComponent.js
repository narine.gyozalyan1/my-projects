import React, { useEffect } from "react";
import {getDataForList} from "../redux/features/TableSlice";
import Spinner from "./Spinner/Spinner";
import Table from 'react-bootstrap/Table';
import PaginationComponent from "./PaginationComponent";
import {useDispatch, useSelector} from "react-redux";

export default function TableComponent(props) {
    const url = props.url;

    const {data, pageNumber, tableHeaders, loading, error} = useSelector((state) => state.dataTable)

    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(getDataForList(url + "?"+"offset=" + pageNumber));
    }, [pageNumber]);

    return (
        <>
            {loading &&
                <Spinner />
            }
            {!loading &&
                <div>
                    <PaginationComponent/>
                    <Table striped bordered hover>
                        <thead>
                        <tr>
                            {tableHeaders && tableHeaders.map((key, index) => (
                                <th key={index}>{key}</th>))}
                        </tr>
                        </thead>
                        <tbody>
                        {data && data.map((item, index) => (
                            <tr key={index}>
                                {Object.values(item).map((value, i) => (
                                    (!Array.isArray(value)
                                            ?
                                            <td key={i}>{value}</td>
                                            :
                                            <td key={i}>{value.length > 0 && value.length}</td>)))}
                            </tr>))}
                        </tbody>
                    </Table>
                </div>
            }
            {error !== "" && <div className="col-md-12 text-center ">{error}</div>}
        </>
    )
};
