import React from "react";
import { Button, Container, Form, Nav, Navbar } from "react-bootstrap";
import { useSelector, useDispatch } from "react-redux";
import { showCitizenList } from "../redux/features/TableSlice";
import {Link, Outlet, useParams} from "react-router-dom";
import Badge from 'react-bootstrap/Badge';
import Stack from 'react-bootstrap/Stack';

export default function Navigation() {
    const dispatch = useDispatch();
    const handleCitizenClick = () => {
        dispatch(showCitizenList());
    };

    const params = useParams();
    console.log(params);

    const pages = ["home", "citizens", "cars", "licenses"];

    return (
        <>
            <Navbar fixed="top" bg="dark">
                <Container>
                    <Navbar.Brand href="#home">Putuli budka</Navbar.Brand>
                    {/*<Navbar.Toggle aria-controls={`offcanvasNavbar-expand-false`} />*/}
                </Container>
                
                <Container fluid>

                </Container>
            </Navbar>
            <Outlet/>

        </>

);
};
