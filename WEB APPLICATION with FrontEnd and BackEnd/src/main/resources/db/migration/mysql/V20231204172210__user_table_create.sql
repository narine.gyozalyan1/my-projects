DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
                        `id` int auto_increment,
                        `first_name` varchar(50) DEFAULT NULL,
                        `last_name` varchar(50) DEFAULT NULL,
                        `email` varchar(50) DEFAULT NULL,
                        `gender` varchar(50) DEFAULT NULL,
                        `address` varchar(50) DEFAULT NULL,
                        `zip` varchar(50) DEFAULT NULL,
                        `country` varchar(50) DEFAULT NULL,
                        `country_code` varchar(50) DEFAULT NULL,
                        `phone` varchar(50) DEFAULT NULL,
                        PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;