DROP TABLE IF EXISTS `election`;

CREATE TABLE `election` (
                            `id` int auto_increment,
                            `election_date` varchar(50) DEFAULT NULL,
                            `election_type` varchar(50) DEFAULT NULL,
                            PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
