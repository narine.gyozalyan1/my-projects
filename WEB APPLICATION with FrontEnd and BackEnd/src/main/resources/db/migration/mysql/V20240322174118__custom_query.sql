DROP TABLE IF EXISTS `custom_query`;

CREATE TABLE `custom_query` (
                           `id` int AUTO_INCREMENT,
                           `user_id` int DEFAULT NULL,
                           `name` VARCHAR(60) DEFAULT NULL,
                           `query` VARCHAR(256) DEFAULT NULL,
                           PRIMARY KEY(`id`)
#                            `user_id` INT
#                            FOREIGN KEY (`user_id`) REFERENCES user(`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;