DROP TABLE IF EXISTS `license`;

CREATE TABLE `license` (
                         `id` int AUTO_INCREMENT,
                         `first_name` VARCHAR(50) DEFAULT NULL,
                         `last_name` VARCHAR(50) DEFAULT NULL,
                         `email` VARCHAR(50) DEFAULT NULL,
                         `gender` VARCHAR(50) DEFAULT NULL,
                         `license_number` INT DEFAULT NULL,
                         `license_type` INT DEFAULT NULL,
                         PRIMARY KEY(`id`),
                         `user_id` INT,
                         FOREIGN KEY (`user_id`) REFERENCES user(`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;