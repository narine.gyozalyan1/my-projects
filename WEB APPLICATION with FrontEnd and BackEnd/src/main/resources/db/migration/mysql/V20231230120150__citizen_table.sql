DROP TABLE IF EXISTS `citizen`;

CREATE TABLE `citizen` (
                           `id` int auto_increment,
                           `last_name` varchar(50) DEFAULT NULL,
                           `first_name` varchar(50) DEFAULT NULL,
                           `father_name` varchar(50) DEFAULT NULL,
                           `date_of_birth` varchar(50) DEFAULT NULL,
                           `region` varchar(50) DEFAULT NULL,
                           `community` varchar(50) DEFAULT NULL,
                           `settlement` varchar(50) DEFAULT NULL,
                           `address` varchar(50) DEFAULT NULL,
                           `area` varchar(50) DEFAULT NULL,
                           `section` varchar(50) DEFAULT NULL,
                           `note` varchar(50) DEFAULT NULL,
                           `election_id` int,
                           PRIMARY KEY (`id`),
                           FOREIGN KEY (`election_id`) REFERENCES election(`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
