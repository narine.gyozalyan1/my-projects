insert into Car (make, model, year, vin_code, number, user_id) values ('Mitsubishi', 'Cordia', 1985, 'WAUNE78P68A805277', '53-426-5953', 2),
                                                                      ('Lamborghini', 'Gallardo', 2006, '1GTN1TEX2DZ898533', '14-095-7846', 3),
                                                                      ('Mitsubishi', 'Pajero', 2006, 'WUAW2BFC9FN328162', '69-348-3269', 1),
                                                                      ('Mazda', 'MPV', 2001, '1G6DK8EVXA0168845', '05-528-5522', 4),
                                                                      ('Dodge', 'Ram 3500', 1998, '4A31K3DTXCE883193', '95-050-2054', 5),
                                                                      ('Ford', 'E-Series', 1988, 'KMHFH4JG2EA918321', '62-474-2292', 6);