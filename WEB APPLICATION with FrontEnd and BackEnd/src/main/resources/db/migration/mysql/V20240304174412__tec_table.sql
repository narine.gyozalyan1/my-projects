DROP TABLE IF EXISTS `territorial_electoral_commissions`;

CREATE TABLE `territorial_electoral_commissions` (
                           `id` int auto_increment,
                           `region` varchar(50) NOT NULL,
                           `commission_number` int NOT NULL,
                           `community` varchar(50) NOT NULL,
                           PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
