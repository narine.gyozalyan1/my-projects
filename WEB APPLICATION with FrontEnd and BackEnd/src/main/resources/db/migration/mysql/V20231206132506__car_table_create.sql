DROP TABLE IF EXISTS `car`;

CREATE TABLE `car` (
                        `id` int auto_increment,
                        `make` varchar(50) DEFAULT NULL,
                        `model` varchar(50) DEFAULT NULL,
                        `year` varchar(50) DEFAULT NULL,
                        `vin_code` varchar(50) DEFAULT NULL,
                        `number` varchar(50) DEFAULT NULL,
                        PRIMARY KEY (`id`),
                        `user_id` INT,
                        FOREIGN KEY (`user_id`) REFERENCES `user`(`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;