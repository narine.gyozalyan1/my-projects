
package com.orion.portal.service;


import com.orion.portal.dto.ElectionDto;
import com.orion.portal.dto.LicenseDto;
import com.orion.portal.dto.UserDto;
import java.util.List;

public interface ElectionService {

	ElectionDto getElectionById(int id);

	List<ElectionDto> getAllElections();

	void deleteElectionByID(int id);

	void createElection(ElectionDto userDto);
}
