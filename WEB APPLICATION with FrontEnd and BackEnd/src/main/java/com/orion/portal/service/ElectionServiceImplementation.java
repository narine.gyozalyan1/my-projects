
package com.orion.portal.service;


import com.orion.portal.dto.ElectionDto;
import com.orion.portal.dto.LicenseDto;
import com.orion.portal.repository.ElectionRepository;
import com.orion.portal.repository.LicenseRepository;
import jakarta.transaction.Transactional;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Transactional
@Slf4j
public class ElectionServiceImplementation implements ElectionService {

	private final ElectionRepository electionRepository;

	@Override
	public ElectionDto getElectionById(int id) {
		return electionRepository.findElectionDtoById(id);
	}

	@Override
	public List<ElectionDto> getAllElections() {
		return electionRepository.findAll();
	}

	@Override
	public void deleteElectionByID(int id) {
		electionRepository.deleteById(id);
	}

	@Override
	public void createElection(ElectionDto election) {
		electionRepository.save(election);
	}
}
