package com.orion.portal.service;

import com.orion.portal.dto.CustomQueryDto;

import java.util.List;

public interface CustomQueryService {
    List<CustomQueryDto> getAllCustomQueries();
    CustomQueryDto getCustomQueryById(int id);
    void deleteCustomQueryByID(int id);

    void createCustomQuery(CustomQueryDto customQuery);

    void updateCustomQueryByID(int id, CustomQueryDto customQueryDto);
}
