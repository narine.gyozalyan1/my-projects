
package com.orion.portal.service;


import com.orion.portal.dto.LicenseDto;
import com.orion.portal.dto.UserDto;
import java.util.List;
import org.springframework.data.domain.Page;

public interface UserService {

	UserDto getUserById(int id);

	Page<UserDto> getAllUsers(int offset, int limit, String sort);

	void deleteUserByID(int id);

	void createUser(UserDto userDto);

	void updateUserByID(int id, UserDto newUser);

}
