
package com.orion.portal.service;


import com.orion.portal.dto.CarDto;
import com.orion.portal.repository.CarRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Transactional
@Slf4j
public class CarServiceImplementation implements CarService {

	private final CarRepository carRepository;

	@Override
	public CarDto getCarById(int id) {
		return carRepository.findCarDtoById(id);
	}

	@Override
	public Page<CarDto> getAllCars(int offset, int pageSize,
		String sortString) {
		String[] sortParams = sortString.split(",");

		if (sortParams[1].equals("asc")) {
			Sort sort = Sort.by(sortParams[0]).ascending(); // asc
			return carRepository.findAll(
				PageRequest.of(offset, pageSize).withSort(sort));
		}
		else {
			Sort sort = Sort.by(sortParams[0]).descending(); // desc
			return carRepository.findAll(
				PageRequest.of(offset, pageSize).withSort(sort));
		}
	}

	@Override
	public void deleteCarByID(int id) {
		carRepository.deleteById(id);
	}

	@Override
	public void createCar(CarDto car) {
		carRepository.save(car);
	}

	@Override
	public void updateCarByID(int id, CarDto newCar) {
		CarDto existingCar = carRepository.findCarDtoById(id);

		existingCar.setMake(newCar.getMake());
		existingCar.setModel(newCar.getModel());
		existingCar.setYear(newCar.getYear());
		existingCar.setVinCode(newCar.getVinCode());
		existingCar.setNumber(newCar.getNumber());
		existingCar.setUserId(newCar.getUserId());
		carRepository.save(existingCar);
	}
}
