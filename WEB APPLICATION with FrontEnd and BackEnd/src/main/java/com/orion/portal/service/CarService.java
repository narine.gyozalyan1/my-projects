
package com.orion.portal.service;


import com.orion.portal.dto.CarDto;
import org.springframework.data.domain.Page;

public interface CarService {

	CarDto getCarById(int id);

	Page<CarDto> getAllCars(int offset, int limit, String sort);

	void deleteCarByID(int id);

	void createCar(CarDto car);

	void updateCarByID(int id, CarDto newCar);

}
