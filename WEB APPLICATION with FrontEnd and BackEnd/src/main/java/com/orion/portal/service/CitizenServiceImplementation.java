
package com.orion.portal.service;


import com.orion.portal.dto.CitizenDto;
import com.orion.portal.repository.CitizenRepository;
import jakarta.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Transactional
@Slf4j
public class CitizenServiceImplementation implements CitizenService {

	private final CitizenRepository citizenRepository;

	@Override
	public CitizenDto getCitizenById(int id) {
		return citizenRepository.findCitizenDtoById(id);
	}

	@Override
	public Page<CitizenDto> search(String first_name, String last_name,
		String father_name, String region, String community, int offset,
		int pageSize, String sortString) {
		String[] sortParams = sortString.split(",");

		if (sortParams[1].equals("asc")) {
			Sort sort = Sort.by(sortParams[0]).ascending(); // asc
			return citizenRepository.findAll(first_name, last_name, father_name,
				region, community,
				PageRequest.of(offset, pageSize).withSort(sort));
		}
		else {
			Sort sort = Sort.by(sortParams[0]).descending(); // desc
			return citizenRepository.findAll(first_name, last_name, father_name,
				region, community,
				PageRequest.of(offset, pageSize).withSort(sort));
		}
	}

	@Override
	public List<String> getAllCommunities(String region) {
		return citizenRepository.getCommunityListForSelect(region);
	}

	@Override
	public List<String> getAllRegions() {
		return citizenRepository.getRegionListForSelect();
	}

	@Override
	public void deleteCitizenByID(int id) {
		citizenRepository.deleteById(id);
	}

	@Override
	public void createCitizen(CitizenDto citizen) {
		citizenRepository.save(citizen);
	}

	@Override
	public void createCitizenList(List<CitizenDto> citizenList) {
		citizenRepository.saveAll(citizenList);
	}
}
