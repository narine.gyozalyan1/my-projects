
package com.orion.portal.service;


import com.orion.portal.dto.LicenseDto;
import com.orion.portal.entity.GetLicenseResponse;
import java.util.List;
import org.springframework.data.domain.Page;

public interface LicenseService {

	Page<LicenseDto> search(int offset, int pageSize, String sort);

	LicenseDto getLicenseById(int id);

	// List<LicenseDto> getAllLicenses();

	void deleteLicenseByID(int id);

	void createLicense(LicenseDto license);

	void updateLicenseByID(int id, LicenseDto license);

}
