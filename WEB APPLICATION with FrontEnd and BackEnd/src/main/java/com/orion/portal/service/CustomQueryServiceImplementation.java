package com.orion.portal.service;
import com.orion.portal.dto.CustomQueryDto;
import com.orion.portal.repository.CustomQueryRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
@Transactional
@Slf4j
public class CustomQueryServiceImplementation implements CustomQueryService {

    private final CustomQueryRepository customQueryRepository;

    @Override
    public List<CustomQueryDto> getAllCustomQueries() {
            return customQueryRepository.findAll();

    }
    @Override
    public CustomQueryDto getCustomQueryById(int id) {
        return customQueryRepository.findCustomQueryDtoById(id);
    }

    @Override
    public void deleteCustomQueryByID(int id) {
        customQueryRepository.deleteById(id);
    }

    @Override
    public void createCustomQuery(CustomQueryDto customQuery) {
        customQueryRepository.save(customQuery);
    }

    @Override
    public void updateCustomQueryByID(int id, CustomQueryDto customQueryDto) {
        CustomQueryDto current = customQueryRepository.findCustomQueryDtoById(id);
        current.setUserId(customQueryDto.getUserId());
        current.setName(customQueryDto.getName());
        current.setQuery(customQueryDto.getQuery());
        customQueryRepository.save(current);

    }
}
