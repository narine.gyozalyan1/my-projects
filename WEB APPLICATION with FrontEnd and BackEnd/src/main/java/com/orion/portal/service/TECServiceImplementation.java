
package com.orion.portal.service;


import com.orion.portal.dto.CitizenDto;
import com.orion.portal.dto.ElectionDto;
import com.orion.portal.dto.TECDto;
import com.orion.portal.repository.ElectionRepository;
import com.orion.portal.repository.TECRepository;
import jakarta.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Transactional
@Slf4j
public class TECServiceImplementation implements TECService {

	private final TECRepository tecRepository;

	/**
	 * TERRITORIAL ELECTORAL COMMISSIONS
	 *
	 * @param territorialElectoralCommission
	 */
	@Override
	public void saveTEC(TECDto territorialElectoralCommission) {
		String[] communities = territorialElectoralCommission.getCommunity().split(
			",");
		List<TECDto> TECList = new ArrayList<>();
		for (String community : communities) {
			TECDto singleTEC = new TECDto();
			singleTEC.setRegion(territorialElectoralCommission.getRegion());
			singleTEC.setCommission_number(
				territorialElectoralCommission.getCommission_number());
			singleTEC.setCommunity(community);

			TECList.add(singleTEC);
		}
		tecRepository.saveAll(TECList);
	}

}
