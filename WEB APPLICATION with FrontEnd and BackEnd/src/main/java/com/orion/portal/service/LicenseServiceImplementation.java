
package com.orion.portal.service;


import com.orion.portal.dto.LicenseDto;
import com.orion.portal.repository.LicenseRepository;
import jakarta.transaction.Transactional;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Transactional
@Slf4j
public class LicenseServiceImplementation implements LicenseService {

	private final LicenseRepository licenseRepository;

	@Override
	public LicenseDto getLicenseById(int id) {
		return licenseRepository.findLicenseDtoById(id);
	}

	// @Override
	// public List<LicenseDto> getAllLicenses() {
	// return licenseRepository.findAll();
	// }

	@Override
	public Page<LicenseDto> search(int offset, int pageSize,
		String sortString) {
		String[] sortParams = sortString.split(",");

		if (sortParams[1].equals("asc")) {
			Sort sort = Sort.by(sortParams[0]).ascending(); // asc
			return licenseRepository.findAll(
				PageRequest.of(offset, pageSize).withSort(sort));
		}
		else {
			Sort sort = Sort.by(sortParams[0]).descending(); // desc
			return licenseRepository.findAll(
				PageRequest.of(offset, pageSize).withSort(sort));
		}

	}

	@Override
	public void deleteLicenseByID(int id) {
		licenseRepository.deleteById(id);
	}

	@Override
	public void createLicense(LicenseDto license) {
		licenseRepository.save(license);
	}

	@Override
	public void updateLicenseByID(int id, LicenseDto license) {
		LicenseDto current = licenseRepository.findLicenseDtoById(id);
		current.setFirstName(license.getFirstName());
		current.setLastName(license.getLastName());
		current.setEmail(license.getEmail());
		current.setGender(license.getGender());
		current.setLicenseNumber(license.getLicenseNumber());
		current.setLicenseType(license.getLicenseType());
		current.setUserId(license.getUserId());
		licenseRepository.save(current);

	}
}
