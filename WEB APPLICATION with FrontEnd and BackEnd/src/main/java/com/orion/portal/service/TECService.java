
package com.orion.portal.service;


import com.orion.portal.dto.LicenseDto;
import com.orion.portal.dto.TECDto;

public interface TECService {

	void saveTEC(TECDto territorialElectoralCommission);
}
