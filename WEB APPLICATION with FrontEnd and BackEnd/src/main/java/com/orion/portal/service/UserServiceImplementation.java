
package com.orion.portal.service;


import com.orion.portal.dto.CarDto;
import com.orion.portal.dto.UserDto;
import com.orion.portal.repository.UserRepository;
import jakarta.transaction.Transactional;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Transactional
@Slf4j
public class UserServiceImplementation implements UserService {

	private final UserRepository userRepository;

	@Override
	public UserDto getUserById(int id) {
		return userRepository.findUserDtoById(id);
	}

	@Override
	public Page<UserDto> getAllUsers(int offset, int pageSize,
		String sortString) {
		String[] sortParams = sortString.split(",");

		if (sortParams[1].equals("asc")) {
			Sort sort = Sort.by(sortParams[0]).ascending(); // asc
			return userRepository.findAll(
				PageRequest.of(offset, pageSize).withSort(sort));
		}
		else {
			Sort sort = Sort.by(sortParams[0]).descending(); // desc
			return userRepository.findAll(
				PageRequest.of(offset, pageSize).withSort(sort));
		}
	}

	@Override
	public void deleteUserByID(int id) {
		userRepository.deleteById(id);
	}

	@Override
	public void createUser(UserDto userDto) {
		userRepository.save(userDto);
	}

	@Override
	public void updateUserByID(int id, UserDto newUser) {
		UserDto existingUser = userRepository.findUserDtoById(id);

		existingUser.setFirstName(newUser.getFirstName());
		existingUser.setLastName(newUser.getLastName());
		existingUser.setEmail(newUser.getEmail());
		existingUser.setGender(newUser.getGender());
		existingUser.setAddress(newUser.getAddress());
		existingUser.setZip(newUser.getZip());
		existingUser.setCountry(newUser.getCountry());
		existingUser.setCountryCode(newUser.getCountryCode());
		existingUser.setPhone(newUser.getPhone());
		userRepository.save(existingUser);
	}
}
