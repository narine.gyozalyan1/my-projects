
package com.orion.portal.service;


import com.orion.portal.dto.CitizenDto;
import java.util.List;
import org.springframework.data.domain.Page;

public interface CitizenService {

	CitizenDto getCitizenById(int id);

	Page<CitizenDto> search(String first_name, String last_name,
		String father_name, String region, String community, int offset,
		int pageSize, String sort);

	List<String> getAllCommunities(String region);

	List<String> getAllRegions();

	void deleteCitizenByID(int id);

	void createCitizen(CitizenDto citizen);

	void createCitizenList(List<CitizenDto> citizenList);
}
