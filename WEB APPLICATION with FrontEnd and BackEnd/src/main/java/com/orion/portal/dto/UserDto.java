
package com.orion.portal.dto;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import java.util.ArrayList;
import java.util.List;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Entity
@Data
@Table(name = "user", uniqueConstraints = {
		@UniqueConstraint(columnNames = { "id" }) })
@JsonIgnoreProperties(ignoreUnknown = true)
@RequiredArgsConstructor
public class UserDto {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	private String firstName;

	private String lastName;

	private String email;

	private String gender;

	private String address;

	private String zip;

	private String country;

	private String countryCode;

	private String phone;

	private boolean loggedIn;

	@OneToMany(cascade = CascadeType.REMOVE, orphanRemoval = true, mappedBy = "userId", targetEntity = LicenseDto.class, fetch = FetchType.EAGER)
	private List<LicenseDto> licenses = new ArrayList<>();

	@OneToMany(cascade = CascadeType.REMOVE, orphanRemoval = true, mappedBy = "userId", targetEntity = CarDto.class, fetch = FetchType.EAGER)
	private List<CarDto> cars = new ArrayList<>();

}
