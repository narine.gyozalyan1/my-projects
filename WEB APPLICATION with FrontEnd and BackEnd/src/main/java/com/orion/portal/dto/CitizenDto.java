
package com.orion.portal.dto;


import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import java.util.ArrayList;
import java.util.List;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Entity
@Data
@Table(name = "citizen", uniqueConstraints = {
		@UniqueConstraint(columnNames = { "id" }) })
@RequiredArgsConstructor
public class CitizenDto {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	private String lastName;

	private String firstName;

	private String fatherName;

	private String dateOfBirth;

	private String region;

	private String community;

	private String settlement;

	private String address;

	private String area;

	private String section;

	private String note;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "election_id", referencedColumnName = "id")
	private ElectionDto election;

}
