
package com.orion.portal.dto;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import jakarta.persistence.*;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Entity
@Data
@Table(name = "license", uniqueConstraints = {
		@UniqueConstraint(columnNames = { "id" }) })
@JsonIgnoreProperties(ignoreUnknown = true)
@RequiredArgsConstructor
public class LicenseDto {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	private String firstName;

	private String lastName;

	private String email;

	private String gender;

	private Integer licenseNumber;

	private String licenseType;

	private Integer userId;
}
