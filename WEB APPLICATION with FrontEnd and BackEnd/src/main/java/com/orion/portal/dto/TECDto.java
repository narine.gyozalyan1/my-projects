
package com.orion.portal.dto;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Entity
@Data
@Table(name = "territorial_electoral_commissions", uniqueConstraints = {
		@UniqueConstraint(columnNames = { "id" }) })
@JsonIgnoreProperties(ignoreUnknown = true)
@RequiredArgsConstructor
public class TECDto {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	private String region;

	private int commission_number;

	private String community;

}
