
package com.orion.portal.dto;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.*;
import java.util.ArrayList;
import java.util.List;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Entity
@Data
@Table(name = "election", uniqueConstraints = {
		@UniqueConstraint(columnNames = { "id" }) })
@JsonIgnoreProperties(ignoreUnknown = true)
@RequiredArgsConstructor
public class ElectionInListDto {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	@JsonProperty("election_date")
	private String electionDate;

	@JsonProperty("election_type")
	private String electionType;

	@OneToMany(mappedBy = "election")
	private List<CitizenInElectionDto> citizens = new ArrayList<>();

}
