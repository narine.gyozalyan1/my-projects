package com.orion.portal.dto;

import jakarta.persistence.*;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Entity
@Data
@Table(name = "custom_query", uniqueConstraints = {
        @UniqueConstraint(columnNames = { "id" }) })
@RequiredArgsConstructor
public class CustomQueryDto {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private Integer userId = 1;
    private String name;
    private String query;
}
