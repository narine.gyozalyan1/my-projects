
package com.orion.portal.dto;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Entity
@Data
@Table(name = "car", uniqueConstraints = {
		@UniqueConstraint(columnNames = { "id" }) })
@JsonIgnoreProperties(ignoreUnknown = true)
@RequiredArgsConstructor
public class CarDto {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	private String make;

	private String model;

	private String year;

	private String vinCode;

	private String number;

	private Integer userId;
}
