
package com.orion.portal.dto.response;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@RequiredArgsConstructor
public class UserForTable {

	private Integer id;

	private String firstName;

	private String lastName;

	private String email;

	private String gender;

	private String address;

	private String zip;

	private String country;

	private String countryCode;

	private String phone;

}
