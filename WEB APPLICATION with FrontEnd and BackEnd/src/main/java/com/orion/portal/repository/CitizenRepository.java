
package com.orion.portal.repository;


import com.orion.portal.dto.CitizenDto;
import com.orion.portal.dto.ElectionDto;
import java.awt.print.Pageable;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface CitizenRepository extends JpaRepository<CitizenDto, Integer> {

	CitizenDto findCitizenDtoById(int id);

	@Query("select ci from CitizenDto ci where ci.firstName LIKE %?1%")
	Page<CitizenDto> findAll(String keyword, PageRequest of);

	@Query("SELECT c FROM CitizenDto c WHERE c.firstName LIKE %?1% AND c.lastName LIKE %?2% AND c.fatherName LIKE %?3% AND c.region LIKE %?4% AND c.community LIKE %?5%")
	Page<CitizenDto> findAll(String first_name, String last_name,
		String father_name, String region, String community, PageRequest of);

	@Query("SELECT DISTINCT c.region FROM CitizenDto c")
	List<String> getRegionListForSelect();

	@Query("SELECT DISTINCT c.community FROM CitizenDto c where c.region LIKE %?1%")
	List<String> getCommunityListForSelect(String region);
}
