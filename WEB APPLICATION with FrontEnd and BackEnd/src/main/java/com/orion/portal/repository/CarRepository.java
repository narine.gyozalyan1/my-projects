
package com.orion.portal.repository;


import com.orion.portal.dto.CarDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CarRepository extends JpaRepository<CarDto, Integer> {

	CarDto findCarDtoById(int id);
}
