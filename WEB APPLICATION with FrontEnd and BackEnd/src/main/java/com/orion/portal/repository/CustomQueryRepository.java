package com.orion.portal.repository;

import com.orion.portal.dto.CustomQueryDto;
import com.orion.portal.dto.LicenseDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomQueryRepository extends JpaRepository<CustomQueryDto, Integer> {
    CustomQueryDto findCustomQueryDtoById(int id);
}
