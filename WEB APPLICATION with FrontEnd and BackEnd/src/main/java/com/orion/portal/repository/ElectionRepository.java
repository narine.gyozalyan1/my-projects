
package com.orion.portal.repository;


import com.orion.portal.dto.ElectionDto;
import com.orion.portal.dto.LicenseDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ElectionRepository
	extends JpaRepository<ElectionDto, Integer> {

	ElectionDto findElectionDtoById(int id);
}
