
package com.orion.portal.repository;


import com.orion.portal.dto.UserDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<UserDto, Integer> {

	UserDto findUserDtoById(int id);
}
