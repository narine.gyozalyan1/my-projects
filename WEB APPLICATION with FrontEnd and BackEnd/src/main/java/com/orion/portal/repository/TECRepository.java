
package com.orion.portal.repository;


import com.orion.portal.dto.TECDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TECRepository extends JpaRepository<TECDto, Integer> {

	// TERRITORIAL ELECTORAL COMMISSIONS
}
