
package com.orion.portal.repository;


import com.orion.portal.dto.CitizenDto;
import com.orion.portal.dto.LicenseDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface LicenseRepository extends JpaRepository<LicenseDto, Integer> {

	LicenseDto findLicenseDtoById(int id);
}
