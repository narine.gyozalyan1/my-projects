
package com.orion.portal.entity;


import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.orion.portal.dto.UserDto;
import jakarta.persistence.*;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@RequiredArgsConstructor
public class GetLicenseResponse {

	private Integer id;

	private String first_name;

	private String last_name;

	private String email;

	private String gender;

	private Integer license_number;

	private String license_type;

	private String user_id;
}
