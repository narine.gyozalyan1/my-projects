
package com.orion.portal.entity;


import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.orion.portal.dto.UserDto;
import jakarta.persistence.*;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@RequiredArgsConstructor
public class GetCarResponse {

	private Integer id;

	private String make;

	private String model;

	private String year;

	private String vin_code;

	private String number;

	private String user_id;
}
