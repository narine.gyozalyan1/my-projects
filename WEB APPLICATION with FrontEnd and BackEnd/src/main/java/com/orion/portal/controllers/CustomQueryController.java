package com.orion.portal.controllers;

import com.orion.portal.dto.CustomQueryDto;
import com.orion.portal.service.CustomQueryService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping(path = "/custom_query")
@RequiredArgsConstructor
@Slf4j
public class CustomQueryController {
    private final CustomQueryService customQueryService;

    @GetMapping(path = "")
    public ResponseEntity<List<CustomQueryDto>> getAllCustomQueries(){
        List<CustomQueryDto> customQuery = customQueryService.getAllCustomQueries();
        return ResponseEntity.ok().body(customQuery);
    }
    @GetMapping(path = "/{id}")
    public ResponseEntity<?> getCustomQueryById(@PathVariable("id") int id) {
        CustomQueryDto customQuery = customQueryService.getCustomQueryById(id);
        return ResponseEntity.ok().body(customQuery);
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteCustomQueryById(@PathVariable("id") int id) {
        customQueryService.deleteCustomQueryByID(id);
        return ResponseEntity.ok().body(null);
    }

    @PostMapping("")
    public ResponseEntity<Void> createCustomQuery(@RequestBody CustomQueryDto customQuery) {
        customQueryService.createCustomQuery(customQuery);
        return ResponseEntity.ok().body(null);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Void> updateCustomQueryById(@PathVariable("id") int id,
                                                  @RequestBody CustomQueryDto customQueryDto) {
        customQueryService.updateCustomQueryByID(id, customQueryDto);
        return ResponseEntity.ok().body(null);
    }

}
