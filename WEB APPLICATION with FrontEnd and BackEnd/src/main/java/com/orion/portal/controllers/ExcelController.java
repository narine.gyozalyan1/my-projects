
package com.orion.portal.controllers;


import com.orion.portal.dto.CitizenDto;
import com.orion.portal.dto.ElectionDto;
import com.orion.portal.service.CitizenService;
import com.orion.portal.service.ElectionService;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

@Controller
@RequestMapping(path = "/excel")
@RequiredArgsConstructor
@Slf4j
public class ExcelController {

	private final CitizenService citizenService;

	private final ElectionService electionService;

	@PostMapping("/upload")
	public ResponseEntity<Void> uploadExcel(
		@RequestParam("files") MultipartFile[] files) throws Exception {
		if (files.length < 1) {
			throw new Exception("Empty file");
		}

		ElectionDto electionDto = electionService.getElectionById(1);

		for (MultipartFile file : files) {
			XSSFWorkbook workbook = new XSSFWorkbook(file.getInputStream());
			XSSFSheet worksheet = workbook.getSheetAt(0);
			List<CitizenDto> citizenList = new ArrayList<>();
			for (int i = 1; i < worksheet.getPhysicalNumberOfRows(); i++) {
				XSSFRow row = worksheet.getRow(i);

				CitizenDto citizen = new CitizenDto();
				citizen.setLastName(row.getCell(0).getStringCellValue());
				citizen.setFirstName(row.getCell(1).getStringCellValue());
				citizen.setFatherName(row.getCell(2).getStringCellValue());
				citizen.setDateOfBirth(row.getCell(3).getStringCellValue());
				citizen.setRegion(row.getCell(4).getStringCellValue());
				citizen.setCommunity(row.getCell(5).getStringCellValue());
				citizen.setSettlement(row.getCell(6).getStringCellValue());
				citizen.setAddress(row.getCell(7).getStringCellValue());
				citizen.setArea(row.getCell(8).getStringCellValue());
				citizen.setSection(row.getCell(9).getStringCellValue());
				citizen.setNote(row.getCell(10).getStringCellValue());

				citizen.setElection(electionDto);

				citizenList.add(citizen);

			}
			citizenService.createCitizenList(citizenList);
		}

		return ResponseEntity.ok().body(null);
	}
}
