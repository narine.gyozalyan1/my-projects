
package com.orion.portal.controllers;


import com.orion.portal.dto.ElectionDto;
import com.orion.portal.dto.TECDto;
import com.orion.portal.service.ElectionService;
import com.orion.portal.service.TECService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping(path = "/election")
@RequiredArgsConstructor
@Slf4j
public class ElectionController {

	private final ElectionService electionService;

	private final TECService tecService;

	@GetMapping(path = "/{id}")
	public ResponseEntity<?> getUserById(@PathVariable("id") int id) {
		ElectionDto election = electionService.getElectionById(id);
		return ResponseEntity.ok().body(election);
	}

	@PostMapping("")
	public ResponseEntity<Void> createElection(
		@RequestBody ElectionDto electionDto) {
		electionService.createElection(electionDto);
		return ResponseEntity.ok().body(null);
	}

	@PostMapping("/tec")
	public ResponseEntity<Void> add(@RequestBody TECDto tecDto) {
		tecService.saveTEC(tecDto);
		return ResponseEntity.ok().body(null);
	}
}
