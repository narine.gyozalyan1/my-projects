
package com.orion.portal.controllers;


import com.orion.portal.dto.LicenseDto;
import com.orion.portal.dto.UserDto;
import com.orion.portal.dto.response.UserForTable;
import com.orion.portal.dto.response.UserInSelect;
import com.orion.portal.service.UserService;
import com.orion.portal.service.UserServiceImplementation;
import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping(path = "/user")
@RequiredArgsConstructor
@Slf4j
public class UserController {

	private final UserService userService;

	@GetMapping(path = "")
	public ResponseEntity<Page<UserDto>> getAllUsers(
		@RequestParam(value = "offset", required = false, defaultValue = "0") int offset,
		@RequestParam(value = "limit", required = false, defaultValue = "10") int limit,
		@RequestParam(value = "sort", required = false, defaultValue = "id,asc") String sort) {
		Page<UserDto> userDtos = userService.getAllUsers(offset, limit, sort);
		// ModelMapper modelMapper = new ModelMapper();
		// List<UserForTable> dtos = userService.getAllUsers(offset, limit,
		// sort).stream().map(
		// user -> modelMapper.map(user, UserForTable.class)).collect(
		// Collectors.toList());
		return ResponseEntity.ok().body(userDtos);
	}

	@GetMapping(path = "/{id}")
	public ResponseEntity<UserForTable> getUserById(
		@PathVariable("id") int id) {
		ModelMapper modelMapper = new ModelMapper();
		UserDto user = userService.getUserById(id);
		UserForTable dto = modelMapper.map(user, UserForTable.class);
		return ResponseEntity.ok().body(dto);
	}

	// @GetMapping(path = "/{id}")
	// public ResponseEntity<?> getUserById(@PathVariable("id") int id) {
	// UserDto user = userService.getUserById(id);
	// return ResponseEntity.ok().body(user);
	// }

	@GetMapping(path = "/select/{id}")
	public ResponseEntity<?> getUserSelectById(@PathVariable("id") int id) {
		ModelMapper modelMapper = new ModelMapper();
		UserDto user = userService.getUserById(id);
		UserInSelect userDTO = modelMapper.map(user, UserInSelect.class);
		return ResponseEntity.ok().body(userDTO);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Void> deleteUser(@PathVariable("id") int id) {
		userService.deleteUserByID(id);
		return ResponseEntity.ok().body(null);
	}

	@PostMapping("")
	public ResponseEntity<Void> createUser(@RequestBody UserDto userDto) {
		userService.createUser(userDto);
		return ResponseEntity.ok().body(null);
	}

	@PutMapping("/{id}")
	public ResponseEntity<UserDto> updateUserById(@PathVariable int id,
		@RequestBody UserDto newUser) {
		userService.updateUserByID(id, newUser);
		return ResponseEntity.ok().body(null);
	}
}
