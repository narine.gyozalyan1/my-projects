
package com.orion.portal.controllers;


import com.orion.portal.dto.CarDto;
import com.orion.portal.service.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping(path = "/car")
public class CarController {

	@Autowired
	private CarService carService;

	@GetMapping(path = "")
	public ResponseEntity<Page<CarDto>> getAllCars(
		@RequestParam(value = "offset", required = false, defaultValue = "0") int offset,
		@RequestParam(value = "limit", required = false, defaultValue = "10") int limit,
		@RequestParam(value = "sort", required = false, defaultValue = "id,asc") String sort) {
		return ResponseEntity.ok().body(
			carService.getAllCars(offset, limit, sort));
	}

	@GetMapping(path = "/{id}")
	public ResponseEntity<?> getCarById(@PathVariable Integer id) {
		CarDto carDto = carService.getCarById(id);
		return ResponseEntity.ok().body(carDto);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Void> deleteCarById(@PathVariable Integer id) {
		carService.deleteCarByID(id);
		return ResponseEntity.ok().body(null);
	}

	@PostMapping("")
	public ResponseEntity<Void> createCar(@RequestBody CarDto carDto) {
		carService.createCar(carDto);
		return ResponseEntity.ok().body(null);
	}

	@PutMapping("/{id}")
	public ResponseEntity<CarDto> updateCarById(@PathVariable Integer id,
		@RequestBody CarDto newCar) {
		carService.updateCarByID(id, newCar);
		return ResponseEntity.ok().body(null);
	}
}
