
package com.orion.portal.controllers;


import com.orion.portal.dto.LicenseDto;
import com.orion.portal.dto.UserDto;
import com.orion.portal.service.LicenseService;
import com.orion.portal.service.UserService;
import jakarta.transaction.Transactional;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping(path = "/license")
@RequiredArgsConstructor
@Slf4j
public class LicenseController {

	private final LicenseService licenseService;

	@GetMapping(path = "")
	public ResponseEntity<Page<LicenseDto>> getAllLicenses(
		@RequestParam(value = "offset", required = false, defaultValue = "0") int offset,
		@RequestParam(value = "limit", required = false, defaultValue = "10") int limit,
		@RequestParam(value = "sort", required = false, defaultValue = "id,asc") String sort) {
		return ResponseEntity.ok().body(
			licenseService.search(offset, limit, sort));
	}

	@GetMapping("/{id}")
	public ResponseEntity<?> getLicenseById(@PathVariable("id") int id) {
		LicenseDto license = licenseService.getLicenseById(id);
		return ResponseEntity.ok().body(license);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Void> deleteLicenseById(@PathVariable("id") int id) {
		licenseService.deleteLicenseByID(id);
		return ResponseEntity.ok().body(null);
	}

	@PostMapping("")
	public ResponseEntity<Void> createLicense(@RequestBody LicenseDto license) {
		licenseService.createLicense(license);
		return ResponseEntity.ok().body(null);
	}

	@PutMapping("/{id}")
	public ResponseEntity<Void> updateLicenseById(@PathVariable("id") int id,
		@RequestBody LicenseDto license) {
		licenseService.updateLicenseByID(id, license);
		return ResponseEntity.ok().body(null);
	}

}
