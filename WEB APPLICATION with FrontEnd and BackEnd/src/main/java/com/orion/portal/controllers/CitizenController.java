
package com.orion.portal.controllers;


import com.orion.portal.dto.CitizenDto;
import com.orion.portal.dto.UserDto;
import com.orion.portal.service.CitizenService;
import com.orion.portal.service.UserService;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
	@RequestMapping(path = "/citizen")
@RequiredArgsConstructor
@Slf4j
public class CitizenController {

	private final CitizenService citizenService;

	@GetMapping(path = "")
	public ResponseEntity<Page<CitizenDto>> getAllCitizens(
		@RequestParam(value = "first_name", required = false, defaultValue = "") String first_name,
		@RequestParam(value = "last_name", required = false, defaultValue = "") String last_name,
		@RequestParam(value = "father_name", required = false, defaultValue = "") String father_name,
		@RequestParam(value = "region", required = false, defaultValue = "") String region,
		@RequestParam(value = "community", required = false, defaultValue = "") String community,
		@RequestParam(value = "offset", required = false, defaultValue = "0") int offset,
		@RequestParam(value = "limit", required = false, defaultValue = "100") int limit,
		@RequestParam(value = "sort", required = false, defaultValue = "id,asc") String sort) {
		return ResponseEntity.ok().body(citizenService.search(first_name,
			last_name, father_name, region, community, offset, limit, sort));
	}

	@GetMapping(path = "/community")
	public ResponseEntity<List<String>> getCommunityList(
		@RequestParam(value = "region", required = false, defaultValue = "") String region) {
		return ResponseEntity.ok().body(
			citizenService.getAllCommunities(region));
	}

	@GetMapping(path = "/region")
	public ResponseEntity<List<String>> getRegionList() {
		return ResponseEntity.ok().body(citizenService.getAllRegions());
	}

	@GetMapping(path = "/{id}")
	public ResponseEntity<?> getCitizenById(@PathVariable("id") int id) {
		CitizenDto citizen = citizenService.getCitizenById(id);
		return ResponseEntity.ok().body(citizen);
	}

}
