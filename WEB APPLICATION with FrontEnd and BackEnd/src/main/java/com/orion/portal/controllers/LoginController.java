package com.orion.portal.controllers;

import com.orion.portal.dto.CredentialsDto;
import com.orion.portal.dto.CustomQueryDto;
import com.orion.portal.dto.UserDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping(path = "/")
@RequiredArgsConstructor
@Slf4j
public class LoginController {
    @PostMapping(path = "/login")
    public ResponseEntity<?> logIn(@RequestBody CredentialsDto credentialsDto) {

        UserDto userDTO = new UserDto();
        userDTO.setEmail("email@mail.com");
        userDTO.setFirstName("Fname");
        userDTO.setLastName("Lname");

        if (!credentialsDto.getUsername().isEmpty() && !credentialsDto.getPassword().isEmpty()) {
            userDTO.setLoggedIn(true);
        }
        return ResponseEntity.ok().body(userDTO);
    }

    @PostMapping(path = "/logout")
    public ResponseEntity<?> logOut() {

        UserDto userDTO = new UserDto();
        userDTO.setLoggedIn(false);
        return ResponseEntity.ok().body(userDTO);
    }
}
