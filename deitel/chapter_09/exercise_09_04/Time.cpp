#include "Time.hpp"

#include <iostream>
#include <iomanip>
#include <ctime>

Time::Time()
{
    time_t currentTime = ::time(NULL);
    const int second = static_cast<int>(currentTime) % 60;
    currentTime /= 60;
    const int minute = static_cast<int>(currentTime) % 60;
    currentTime /= 60;
    const int hour = static_cast<int>(currentTime) % 24;

    setTime(hour, minute, second);
}

void
Time::setTime(const int h, const int m, const int s)
{
    setHour(h);
    setMinute(m);
    setSecond(s);
}

void
Time::setHour(const int h)
{
    hour_ = (h >= 0 && h < 24) ? h : 0;
}

void
Time::setMinute(const int m)
{
    minute_ = (m >= 0 && m < 60) ? m : 0;
}

void
Time::setSecond(const int s)
{
    second_ = (s >= 0 && s < 60) ? s : 0;
}

int
Time::getHour()
{
    return hour_;
}

int
Time::getMinute()
{
    return minute_;
}

int
Time::getSecond()
{
    return second_;
}

void
Time::printUniversal()
{
    std::cout << std::setfill('0') << std::setw(2) << getHour() << ":"
         << std::setw(2) << getMinute() << ":" << std::setw(2) << getSecond();
}

void
Time::printStandard()
{
    std::cout << ((getHour() == 0 || getHour() == 12) ? 12 : getHour() % 12)
              << ":" << std::setfill('0') << std::setw(2) << getMinute()
              << ":" << std::setw(2) << getSecond() << (hour_ < 12 ? " AM" : " PM");
}

