#ifndef __TIME_HPP__
#define __TIME_HPP__

class Time
{
public:
    Time();
    void setTime(const int h, const int m, const int s);
    void setHour(const int h);
    void setMinute(const int m);
    void setSecond(const int s);
    int getHour();
    int getMinute();
    int getSecond();
    void printUniversal();
    void printStandard();
private:
    int hour_;
    int minute_;
    int second_;
};
#endif

