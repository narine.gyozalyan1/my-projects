#include "Time.hpp"

#include <iostream>

int
main()
{
    Time t1;
    std::cout << "Universal: ";
    t1.printUniversal();
    std::cout << "\nStandard: ";
    t1.printStandard();
    std::cout << std::endl;

    return 0;
}

