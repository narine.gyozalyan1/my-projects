#include "DateAndTime.hpp"

#include <iostream>

int
main()
{
    DateAndTime dateAndTime1(1, 2022, 23, 23, 59, 59);
    dateAndTime1.printStandard();
    dateAndTime1.tick();
    dateAndTime1.printStandard();
    const int COUNT = 20;
    for (int i = 0; i < COUNT; ++i) {
        dateAndTime1.tick();
        dateAndTime1.printStandard();
    }

    return 0;
}

