#include "DateAndTime.hpp"

#include <iostream>
#include <iomanip>

const int DateAndTime::MONTH_ARRAY[DateAndTime::MONTH_SIZE] = { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

DateAndTime::DateAndTime(const int month, const int year, const int day, const int hour, const int minute, const int second)
{
    setDateAndTime(month, year, day, hour, minute, second);
}

void
DateAndTime::tick()
{
    setSecond(getSecond() + 1);
    if (0 == getSecond()) {
        setMinute(getMinute() + 1);
        if (0 == getMinute()) {
            setHour(getHour() + 1);
            if (0 == getHour()) {
                nextDay();
            }
        }
    }
}

void
DateAndTime::nextDay()
{
    setDay(getDay() + 1);
    if (1 == getDay()) {
        setMonth(getMonth() + 1);
        if (1 == getMonth()) {
            setYear(getYear() + 1);
        }
    }
}

void
DateAndTime::setDateAndTime(const int month, const int year, const int day, const int hour, const int minute, const int second)
{
    setMonth(month);
    setYear(year);
    setDay(day);
    setHour(hour);
    setMinute(minute);
    setSecond(second);
}

void
DateAndTime::setMonth(const int month)
{
    month_ = (month > 0 && month <= 12) ? month : 1;
}

void
DateAndTime::setYear(const int year)
{
    year_ = (year > 0) ? year : 1;
}

int
DateAndTime::lastDayOfMonth(const int month) const
{
    return (2 == month && leapYear(year_) ? 29 : MONTH_ARRAY[month]);
}

void
DateAndTime::setDay(const int day)
{
    day_ = (day > 0 && day <= lastDayOfMonth(month_)) ? day : 1;
}

void
DateAndTime::setHour(const int hour)
{
    hour_ = (hour >= 0 && hour < 24) ? hour : 0;
}

void
DateAndTime::setMinute(const int minute)
{
    minute_ = (minute >= 0 && minute < 60) ? minute : 0;
}

void
DateAndTime::setSecond(const int second)
{
    second_ = (second >= 0 && second < 60) ? second : 0;
}

int
DateAndTime::getYear() const
{
    return year_;
}

int
DateAndTime::getMonth() const
{
    return month_;
}

int
DateAndTime::getDay() const
{
    return day_;
}

int
DateAndTime::getHour() const
{
    return hour_;
}

int
DateAndTime::getMinute() const
{
    return minute_;
}

int
DateAndTime::getSecond() const
{
    return second_;
}

bool
DateAndTime::leapYear(const int year) const
{
    return year % 400 == 0 || (year % 4 == 0 && year % 100 != 0);
}

void
DateAndTime::printDate() const
{
    std::cout << month_ << '/' << day_ << '/' << year_ << " ";

}

void
DateAndTime::printStandard() const
{
    printDate();
    std::cout << ((getHour() == 0 || getHour() == 12) ? 12 : getHour() % 12)
              << ":" << std::setfill('0') << std::setw(2) << getMinute()
              << ":" << std::setw(2) << getSecond() << (hour_ < 12 ? " AM" : " PM");
    std::cout << std::endl;
}

void
DateAndTime::printUniversal() const
{
    printDate();
    std::cout << std::setfill('0') << std::setw(2) << getHour() << ":"
              << std::setw(2) << getMinute() << ":" << std::setw(2) << getSecond();
}

