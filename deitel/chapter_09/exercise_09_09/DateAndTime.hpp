#ifndef __DATE_AND_TIME_HPP__
#define __DATE_AND_TIME_HPP__

class DateAndTime
{
public:
    static const int MONTH_SIZE = 13;
    static const int MONTH_ARRAY[MONTH_SIZE];
public:
    DateAndTime(const int month = 1, const int year = 2000, const int day = 1, const int hour = 0, const int minute = 0, const int second = 0);
    void setDateAndTime(const int month, const int year, const int day, const int hour, const int minute, const int second);
    void setMonth(const int month);
    void setYear(const int year);
    void setDay(const int day);
    void setHour(const int hour);
    void setMinute(const int minute);
    void setSecond(const int second);
    int getHour() const;
    int getMonth() const;
    int getYear() const;
    int getDay() const;
    int getMinute() const;
    int getSecond() const;
    int lastDayOfMonth(const int month) const;
    void printUniversal() const;
    void printStandard() const;
    void tick();
    void setDate();
    void nextDay();
    bool leapYear(const int year) const;
    void printDate() const;
private:
    int hour_;
    int minute_;
    int second_;
    int month_;
    int day_;
    int year_;
};

#endif

