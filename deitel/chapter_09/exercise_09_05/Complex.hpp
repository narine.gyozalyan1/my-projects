#ifndef __COMPLEX_HPP__
#define __COMPLEX_HPP__

class Complex
{
public:
    Complex(const double realPart = 0, const double imaginaryPart = 0);
    void setReal(const double realPart);
    void setImaginary(const double imaginaryPart);
    Complex add(const Complex& complexNum) const;
    Complex subtract(const Complex& complexNum) const;
    void print() const;
private:
    double realPart_;
    double imaginaryPart_;
};

#endif

