#include "Complex.hpp"

#include <iostream>

Complex::Complex(const double realPart, const double imaginaryPart)
{
    setReal(realPart);
    setImaginary(imaginaryPart);
}

void
Complex::setReal(const double realPart)
{
    realPart_ = realPart;
}

void
Complex::setImaginary(const double imaginaryPart)
{
    imaginaryPart_ = imaginaryPart;
}

Complex
Complex::add(const Complex& complexNum) const
{
    return Complex((realPart_ + complexNum.realPart_), (imaginaryPart_ + complexNum.imaginaryPart_));
}

Complex
Complex::subtract(const Complex& complexNum) const
{
    return Complex((realPart_ - complexNum.realPart_), (imaginaryPart_ - complexNum.imaginaryPart_));
}

void
Complex::print() const
{
    std::cout << "(" << realPart_ << ", " << imaginaryPart_<< ")" << std::endl;
}

