#include "Complex.hpp"

#include <iostream>
#include <unistd.h>

int
main()
{
    double real1, imaginary1;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Enter real and imaginary parts for first number: ";
    }
    std::cin >> real1 >> imaginary1;
    const Complex complex1(real1, imaginary1);
    
    double real2, imaginary2;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Enter real and imaginary parts for first number: ";
    }
    std::cin >> real2 >> imaginary2;
    const Complex complex2(real2, imaginary2);
    
    const Complex complexSum = complex1.add(complex2);
    complexSum.print();
    const Complex complexDiff = complex1.subtract(complex2);
    complexDiff.print();

    return 0;
}

