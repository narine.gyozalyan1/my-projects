#include "Time.hpp"

#include <iostream>
#include <unistd.h>

int
main()
{
    int second;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Enter second: ";
    }
    std::cin >> second;
    if (second < 0 || second >= 60) {
        std::cout << "Error 2: Invalid value for second." << std::endl;
        return 3;
    }
    int minute;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Enter minute: ";
    }
    std::cin >> minute;
    if (minute < 0 || minute >= 60) {
        std::cout << "Error 2: Invalid value for minute." << std::endl;
        return 2;
    }
    int hour;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Enter hour: ";
    }
    std::cin >> hour;
    if (hour < 0 || hour > 24) {
        std::cout << "Error 1: Invalid value for hour." << std::endl;
        return 1;
    }

    const Time time1(hour, minute, second);
    std::cout << "Universal: ";
    time1.printUniversal();
    std::cout << "\nStandard: ";
    time1.printStandard();
    std::cout << std::endl;

    return 0;
}

