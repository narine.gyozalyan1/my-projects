#include "Time.hpp"

#include <iostream>
#include <iomanip>
#include <cassert>

Time::Time(const int hour, const int minute, const int second)
{
    setTime(hour, minute, second);
}

void
Time::setTime(const int hour, const int minute, const int second)
{
    setHour(hour);
    setMinute(minute);
    setSecond(second);
}

void
Time::setHour(const int hour)
{
    assert(hour >= 0 && hour < 24);
    hour_ = hour;
}

void
Time::setMinute(const int minute)
{
    assert(minute >= 0 && minute < 60);
    minute_ = minute;
}

void
Time::setSecond(const int second)
{
    assert(second >= 0 && second < 60);
    second_ = second;
}

int
Time::getHour() const
{
    return hour_;
}

int
Time::getMinute() const
{
    return minute_;
}

int
Time::getSecond() const
{
    return second_;
}

void
Time::printUniversal() const
{
    std::cout << std::setfill('0') << std::setw(2) << getHour() << ":"
              << std::setw(2) << getMinute() << ":" << std::setw(2) << getSecond();
}

void
Time::printStandard() const
{
    std::cout << ((getHour() == 0 || getHour() == 12) ? 12 : getHour() % 12)
              << ":" << std::setfill('0') << std::setw(2) << getMinute()
              << ":" << std::setw(2) << getSecond() << (hour_ < 12 ? " AM" : " PM");
}

