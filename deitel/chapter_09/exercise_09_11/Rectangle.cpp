#include "Rectangle.hpp"

#include <iostream>
#include <iomanip>

Rectangle::Rectangle(const double length, const double width)
{
    setRectangle(length, width);
}

void
Rectangle::setRectangle(const double length, const double width)
{
    setLength(length);
    setWidth(width);
}

void
Rectangle::setLength(const double length)
{
    length_ = (length > 0 && length < 20) ? length : 1;
}

void
Rectangle::setWidth(const double width)
{
    width_ = (width > 0 && width < 20) ? width : 1;

}

double
Rectangle::getLength() const
{
    return length_;
}

double
Rectangle::getWidth() const
{
    return width_;
}

double
Rectangle::perimeter() const
{
    return 2 * (getLength() + getWidth());
}

double
Rectangle::area() const
{
    return (getLength() * getWidth());
}

