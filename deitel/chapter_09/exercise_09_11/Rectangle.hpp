#ifndef __RECTANGLE_HPP__
#define __RECTANGLE_HPP__

class Rectangle
{
public:
    Rectangle(const double length = 1, const double width = 1);
    void setRectangle(const double length, const double width);
    void setLength(const double length);
    void setWidth(const double width);
    double getLength() const;
    double getWidth() const;
    double perimeter() const;
    double area() const;

private:
    double length_;
    double width_;
};

#endif /// __RECTANGLE_HPP__

