#include "Rectangle.hpp"

#include <iostream>
#include <unistd.h>

int
main()
{
    int length;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Enter length: ";
    }
    std::cin >> length;
    int width;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Enter width: ";
    }
    std::cin >> width;
    const Rectangle rectangle1(length, width);
    std::cout << "Perimeter is " << rectangle1.perimeter() << std::endl;
    std::cout << "Area is " << rectangle1.area() << std::endl;

    return 0;
}
