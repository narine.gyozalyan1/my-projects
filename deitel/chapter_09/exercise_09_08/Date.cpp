#include "Date.hpp"

#include <iostream>
#include <ctime>

const int Date::MONTH_ARRAY[Date::MONTH_SIZE] = { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

Date::Date(const int month, const int year, const int day)
{
    setDate(month, year, day);
}

void
Date::setDate(const int month, const int year, const int day)
{
    setMonth(month);
    setYear(year);
    setDay(day);
}

void
Date::setMonth(const int month)
{
    month_ = (month > 0 && month <= 12) ? month : 1;
}

void
Date::setYear(const int year)
{
    year_ = (year > 0) ? year : 1;
}

int
Date::lastDayOfMonth(const int month) const
{
    return (2 == month && leapYear(year_) ? 29 : MONTH_ARRAY[month]);
}

void
Date::setDay(const int day)
{
    day_ = (day > 0 && day <= lastDayOfMonth(month_)) ? day : 1;
}

int
Date::getYear() const
{
    return year_;
}

int
Date::getMonth() const
{
    return month_;
}

int
Date::getDay() const
{
    return day_;
}

void
Date::nextDay()
{
    setDay(getDay() + 1);
    if (1 == getDay()) {
        setMonth(getMonth() + 1);
        if (1 == getMonth()) {
            setYear(getYear() + 1);
        }
    }
}

bool
Date::leapYear(const int year) const
{
    return year % 400 == 0 || (year % 4 == 0 && year % 100 != 0);
}

void
Date::print() const
{
    std::cout << month_ << '/' << day_ << '/' << year_;
    std::cout << std::endl;
}

