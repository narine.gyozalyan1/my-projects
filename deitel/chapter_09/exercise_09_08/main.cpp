#include "Date.hpp"

#include <iostream>

int
main()
{
    Date date1(11, 2003, 27);
    std::cout << "date1 is ";
    date1.print();
    const int COUNTER = 100;
    for (int i = 0; i < COUNTER; ++i) {
        std::cout << "date1 is ";
        date1.nextDay();
        date1.print();
    }

    return 0;
}

