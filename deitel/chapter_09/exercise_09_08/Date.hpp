#ifndef DATE_H
#define DATE_H

class Date
{
public:
    static const int MONTH_SIZE = 13;
    static const int MONTH_ARRAY[MONTH_SIZE];
public:
    Date(const int month = 1, const int year = 2000, const int day = 1);
    void setMonth(const int month);
    void setYear(const int year);
    void setDay(const int day);
    void setDate(const int month, const int year, const int day);
    int getMonth() const;
    int getYear() const;
    int lastDayOfMonth(const int month) const;
    int getDay() const;
    void nextDay();
    bool leapYear(const int year) const;
    void print() const;
private:
    int month_;
    int day_;
    int year_;
};
#endif

