#include "Time.hpp"

#include <iostream>

int
main()
{
    Time time1(23, 59, 59);
    std::cout << "Universal: ";
    time1.printUniversal();
    std::cout << "\nStandard: ";
    time1.printStandard();
    std::cout << std::endl;
 
    time1.tick();
    time1.printStandard();
    std::cout << std::endl;

    const int COUNT = 60;
    for (int i = 0; i < COUNT; ++i) {
        time1.tick();
        time1.printStandard();
        std::cout << std::endl;
    }

    return 0;
}

