#include "Point.hpp"

Point::Point(const double x, const double y)
{
    setCoordinateX(x);
    setCoordinateY(y);
}

void
Point::setCoordinateX(const double x)
{
    x_ = (x > 0 || x <= 20) ? x : 0;
}

void
Point::setCoordinateY(const double y)
{
    y_ = (y > 0 || y <= 20) ? y : 0;
}

double
Point::getCoordinateX() const
{
    return x_;
}

double
Point::getCoordinateY() const
{
    return y_;
}

