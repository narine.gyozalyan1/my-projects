#ifndef __POINT_HPP__
#define __POINT_HPP__

class Point
{
public:
    Point(const double x = 0, const double y = 0);
    void setCoordinateX(const double x);
    void setCoordinateY(const double y);
    double getCoordinateX() const;
    double getCoordinateY() const;
private:
    double x_;
    double y_;
};

#endif /// __POINT_HPP__

