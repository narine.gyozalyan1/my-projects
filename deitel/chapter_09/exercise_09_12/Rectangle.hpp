#ifndef __RECTANGLE_HPP__
#define __RECTANGLE_HPP__

#include "Point.hpp"

class Rectangle
{
public:
    Rectangle(const Point& point1, const Point& point2, const Point& point3, const Point& point4);
    void setPoint1(const Point& point1);
    void setPoint2(const Point& point2);
    void setPoint3(const Point& point3);
    void setPoint4(const Point& point4);
    Point getPoint1() const;
    Point getPoint2() const;
    Point getPoint3() const;
    Point getPoint4() const;
    double distance(const Point& point1, const Point& point2) const;
    bool isRectangle(const Point& point1, const Point& point2, const Point& point3, const Point& point4) const;
    bool isSquare() const;
    double length() const;
    double width() const;
    double perimeter() const;
    double area() const;

private:
    Point point1_;
    Point point2_;
    Point point3_;
    Point point4_;
};

#endif /// __RECTANGLE_HPP__

