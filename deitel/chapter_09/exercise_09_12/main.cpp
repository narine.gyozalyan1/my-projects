#include "Rectangle.hpp"

#include <iostream>
#include <unistd.h>

double
inputCoordinate()
{
    double coordinate;
    std::cin >> coordinate;
    return coordinate;
}

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Enter coordinates x and y for 4 points: ";
    }
    const Point point1(inputCoordinate(), inputCoordinate());
    const Point point2(inputCoordinate(), inputCoordinate());
    const Point point3(inputCoordinate(), inputCoordinate());
    const Point point4(inputCoordinate(), inputCoordinate());
    
    Rectangle rectangle1(point1, point2, point3, point4);
    std::cout << "Perimeter is " << rectangle1.perimeter() << std::endl;
    std::cout << "Area is " << rectangle1.area() << std::endl;

    return 0;
}

