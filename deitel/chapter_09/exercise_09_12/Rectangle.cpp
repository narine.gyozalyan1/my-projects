#include "Rectangle.hpp"
#include "Point.hpp"

#include <iostream>
#include <iomanip>
#include <cmath>

Rectangle::Rectangle(const Point& point1, const Point& point2, const Point& point3, const Point& point4)
{
    if (isRectangle(point1, point2, point3, point4)) {
        setPoint1(point1);
        setPoint2(point2);
        setPoint3(point3);
        setPoint4(point4);
    }
}

void
Rectangle::setPoint1(const Point& point1)
{
    point1_ = point1;
}

void
Rectangle::setPoint2(const Point& point2)
{
    point2_ = point2;
}

void
Rectangle::setPoint3(const Point& point3)
{
    point3_ = point3;
}

void
Rectangle::setPoint4(const Point& point4)
{
    point4_ = point4;
}

Point
Rectangle::getPoint1() const
{
    return point1_;
}

Point
Rectangle::getPoint2() const
{
    return point2_;
}

Point
Rectangle::getPoint3() const
{
    return point3_;
}

Point
Rectangle::getPoint4() const
{
    return point4_;
}

double
Rectangle::distance(const Point& point1, const Point& point2) const
{
    const int deltaX = point1.getCoordinateX() - point2.getCoordinateX();
    const int deltaY = point1.getCoordinateY() - point2.getCoordinateY();
    return std::sqrt(deltaX * deltaX + deltaY * deltaY);
}

double
Rectangle::length() const
{
    const double distance1 = distance(getPoint1(), getPoint2());
    const double distance2 = distance(getPoint2(), getPoint3());
    return (distance1 > distance2) ? distance1 : distance2;
}

double
Rectangle::width() const
{
    const double distance1 = distance(getPoint1(), getPoint2());
    const double distance2 = distance(getPoint2(), getPoint3());
    return (distance1 < distance2) ? distance1 : distance2;
}

bool
Rectangle::isRectangle(const Point& point1, const Point& point2, const Point& point3, const Point& point4) const
{
    return (distance(point1, point2) == distance(point3, point4) && 
            distance(point2, point3) == distance(point1, point4) &&
            distance(point1, point3) == distance(point2, point4) &&
            distance(point1, point3) > distance(point1, point2) &&
            distance(point1, point3) > distance(point1, point4));
}

bool
Rectangle::isSquare() const
{
    return length() == width();
}

double
Rectangle::perimeter() const
{
    return (length() + width()) * 2;
}

double
Rectangle::area() const
{
    return length() * width();
}

