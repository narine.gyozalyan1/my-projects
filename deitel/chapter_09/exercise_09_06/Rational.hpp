#ifndef __RATIONAL__HPP
#define __RATIONAL__HPP

class Rational
{
public:
    Rational(const int numerator = 0, const int denominator = 1);
    void setNumerator(const int numerator);
    void setDenominator(const int denominator);
    int getNumerator() const;
    int getDenominator() const;
    int gcd(const int x, const int y) const;
    Rational add(const Rational& rhv) const;
    Rational sub(const Rational& rhv) const;
    Rational mul(const Rational& rhv) const;
    Rational div(const Rational& rhv) const;
    void printRational() const;
    void printFloating() const;

private:
    void reduce(const int reduceNumber);

private:
    int numerator_;
    int denominator_;
};

#endif

