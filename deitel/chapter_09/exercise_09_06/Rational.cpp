#include "Rational.hpp"

#include <iostream>
#include <iomanip>
#include <cassert>

Rational::Rational(const int numerator, const int denominator)
{
    setNumerator(numerator);
    setDenominator(denominator); 
    const int reduceNumber = gcd(numerator, denominator);
    reduce(reduceNumber);
}

void
Rational::setNumerator(const int numerator)
{
    numerator_ = numerator;
}

void
Rational::setDenominator(const int denominator)
{
    assert(0 != denominator);
    denominator_ = denominator;
}

int
Rational::getNumerator() const
{
    return numerator_;
}

int
Rational::getDenominator() const
{
    return denominator_;
}

int
Rational::gcd(const int x, const int y) const
{
    if (0 == y) {
        return x;
    }                                                                 
    if (x < y) {
        return gcd(y, x);
    }
    return gcd(y, x % y);
}

void
Rational::reduce(const int reduceNumber)
{
    numerator_ /= reduceNumber;
    denominator_ /= reduceNumber;
}

Rational
Rational::add(const Rational& rhv) const
{
    const int numerator = getNumerator() * rhv.getDenominator() + rhv.getNumerator() * getDenominator();
    const int denominator = getDenominator() * rhv.getDenominator();
    const Rational result(numerator, denominator);
    return result;
}

Rational
Rational::sub(const Rational& rhv) const
{
    const int numerator = getNumerator() * rhv.getDenominator() - rhv.getNumerator() * getDenominator();
    const int denominator = getDenominator() * rhv.getDenominator();
    const Rational result(numerator, denominator);
    return result;
}

Rational
Rational::mul(const Rational& rhv) const
{
    const int numerator = getNumerator() * rhv.getNumerator();
    const int denominator= getDenominator() * rhv.getDenominator();
    const Rational result(numerator, denominator);
    return result;
}

Rational
Rational::div(const Rational& rhv) const
{
    assert(0 != rhv.getNumerator());
    const int numerator = rhv.getNumerator() * getDenominator();
    const int denominator = getDenominator() * rhv.getNumerator();
    const Rational result(numerator, denominator);
    return result;
}

void
Rational::printRational() const
{
    std::cout << numerator_ << "/" << denominator_;
}

void
Rational::printFloating() const
{
    std::cout << std::fixed << std::setprecision(4) << (static_cast<double>(numerator_) / denominator_);
}

