#include "Rational.hpp"

#include <iostream>
#include <unistd.h>

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Enter numerator and denominator for the first fraction: ";
    }
    int numerator1;
    int denominator1;
    std::cin >> numerator1 >> denominator1;
    const Rational rational1(numerator1, denominator1);
    
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Enter numerator and denominator for the first fraction: ";
    }
    int numerator2;
    int denominator2;
    std::cin >> numerator2 >> denominator2;
    const Rational rational2(numerator2, denominator2);

    const Rational sum = rational1.add(rational2);
    std::cout << "Sum is ";
    sum.printRational();
    std::cout << " = ";
    sum.printFloating();
    std::cout << std::endl;
    const Rational diff = rational1.sub(rational2);
    std::cout << "Difference is ";
    diff.printRational();
    std::cout << " = ";
    diff.printFloating();
    std::cout << std::endl;
    const Rational mul = rational1.mul(rational2);
    std::cout << "Product is ";
    mul.printRational();
    std::cout << " = ";
    mul.printFloating();
    std::cout << std::endl;
    const Rational div = rational1.div(rational2);
    std::cout << "Quotient is ";
    div.printRational();
    std::cout << " = ";
    div.printFloating();
    std::cout << std::endl;

    return 0;
}

