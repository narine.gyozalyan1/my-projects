#include "SavingsAccount.hpp"

#include <iostream>
#include <unistd.h>

int
main()
{
    SavingsAccount saver1(2000);
    SavingsAccount saver2(3000);
    int rate = 3;
    saver1.modifyInterestRate(rate);
    saver1.calculateMonthlyInterest();
    std::cout << "For Saver1 ";
    saver1.printBalance();
    saver2.modifyInterestRate(rate);
    saver2.calculateMonthlyInterest();
    std::cout << "For Saver2 ";
    saver2.printBalance();

    rate = 4;
    saver1.modifyInterestRate(rate);
    saver1.calculateMonthlyInterest();
    std::cout << "For Saver1 ";
    saver1.printBalance();
    saver2.modifyInterestRate(rate);
    saver2.calculateMonthlyInterest();
    std::cout << "For Saver2 ";
    saver2.printBalance();
    
    return 0;
}

