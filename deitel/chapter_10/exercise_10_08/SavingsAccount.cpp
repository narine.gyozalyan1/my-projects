#include "SavingsAccount.hpp"

#include <iostream>
#include <iomanip>

int SavingsAccount::annualInterestRate_ = 0;

SavingsAccount::SavingsAccount(const int savingsBalance)
{
    setSavingsBalance(savingsBalance);
}

void
SavingsAccount::setSavingsBalance(const int savingsBalance)
{
    savingsBalance_ = savingsBalance;
}

int
SavingsAccount::getSavingsBalance() const
{
    return savingsBalance_;
}

void
SavingsAccount::modifyInterestRate(const int rate)
{
    annualInterestRate_ = rate;
}

void
SavingsAccount::calculateMonthlyInterest()
{
    const int interest = savingsBalance_ + savingsBalance_ * annualInterestRate_ / 12 / 100;
    setSavingsBalance(interest);
}

void
SavingsAccount::printBalance() const
{
    std::cout << "The Balance Is " << std::fixed << std::setprecision(2) << getSavingsBalance() << "$" << std::endl;
}

