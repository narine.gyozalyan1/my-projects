#ifndef __SAVINGS_ACCOUNT_HPP__
#define __SAVINGS_ACCOUNT_HPP__

class SavingsAccount
{
public:
    static void modifyInterestRate(const int rate);
private:    
    static int annualInterestRate_;
public:
    SavingsAccount(const int savingsBalance);
    void calculateMonthlyInterest();
    void setSavingsBalance(const int savingsBalance);
    int getSavingsBalance() const;
    void printBalance() const;
private:
    int savingsBalance_;
};

#endif /// __SAVINGS_ACCOUNT_HPP__

