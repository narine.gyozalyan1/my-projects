#include "IntegerSet.hpp"

#include <iostream>
#include <cassert>

IntegerSet::IntegerSet()
{
    for (int i = 0; i < SIZE; ++i) {
        integerSet_[i] = false;
    }
}

IntegerSet::IntegerSet(const int integerSet[], const int size)

{
    assert(size >= 0 && size < SIZE);
    for (int i = 0; i < SIZE; ++i) {
        integerSet_[i] = false;
    }
    for (int i = 0; i < size; ++i) {
        const int j = integerSet[i];
        assert(j >= 0 && j < SIZE);
        integerSet_[j] = true;
    }
}

IntegerSet
IntegerSet::unionOfSets(const IntegerSet& rhv)
{
    IntegerSet newSet;
    for (int i = 0; i < SIZE; ++i) {
        newSet.integerSet_[i] = integerSet_[i] || rhv.integerSet_[i];
    }
    return newSet;
}

IntegerSet
IntegerSet::intersectionOfSets(const IntegerSet& rhv)
{
    IntegerSet newSet;
    for (int i = 0; i < SIZE; ++i) {
        newSet.integerSet_[i] = integerSet_[i] && rhv.integerSet_[i];
    }
    return newSet;
}

void
IntegerSet::insertElement(const int element)
{
    if (!integerSet_[element]) {
        integerSet_[element] = true;
    }
}

void
IntegerSet::deleteElement(const int element)
{
    if (integerSet_[element]) {
        integerSet_[element] = false;
    }
}

void
IntegerSet::printSet() const
{
    int count = 0;
    for (int i = 0; i < SIZE; ++i) {
        if (integerSet_[i]) {
            std::cout << i << " ";
            ++count;
        }
    }
    if (0 == count) {
        std::cout << "---";
    }
    std::cout << std::endl;
}

bool
IntegerSet::isEqualTo(const IntegerSet& rhv) const
{
    for (int i = 0; i < SIZE; ++i) {
        if (integerSet_[i] != rhv.integerSet_[i]) {
            return false;
        }
    }
    return true;
}

