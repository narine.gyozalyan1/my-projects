#include "IntegerSet.hpp"

#include <iostream>

int
main()
{
    const int SIZE = 5;
    int integerSet[SIZE] = { 24, 5, 2, 87, 1};
    IntegerSet set1(integerSet, SIZE);
    set1.printSet();

    IntegerSet set2;

    set2.insertElement(45);
    set2.insertElement(5);
    set2.insertElement(6);
    set2.insertElement(24);

    set2.printSet();
    set2.deleteElement(6);

    set2.printSet();
    const IntegerSet set3 = set1.intersectionOfSets(set2);
    set3.printSet();
    std::cout << "Set1 and set2 are" << (set1.isEqualTo(set2) ? " " : " not ") << "equal." << std::endl;

    return 0;
}

