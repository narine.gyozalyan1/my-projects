#ifndef __INTEGER_SET_HPP__
#define __INTEGER_SET_HPP__

static const int SIZE = 100;

class IntegerSet
{
public:
    IntegerSet();
    IntegerSet(const int integerSet[], const int size);
    IntegerSet unionOfSets(const IntegerSet& rhv);
    IntegerSet intersectionOfSets(const IntegerSet& rhv);
    void insertElement(const int element);
    void deleteElement(const int element);
    void printSet() const;
    bool isEqualTo(const IntegerSet& rhv) const;
private:
    bool integerSet_[SIZE];
};

#endif /// __INTEGER_SET_HPP__

