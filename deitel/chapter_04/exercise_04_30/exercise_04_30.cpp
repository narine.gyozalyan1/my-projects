#include <iostream>
#include <iomanip>

int
main()
{
    double radius;
    std::cout << "Enter the radius: ";
    std::cin >> radius;
    
    if (radius < 0) {
        std::cout << "Error 1: Radius can't be negative." << std::endl;
        return 1;
    }

    double diameter = 2 * radius;
    std::cout << "Diameter is " << std::setprecision(5) << std::fixed << diameter << std::endl;
    double pi = 3.14159;
    double circumference = diameter * pi;
    std::cout << "Circumference is " << std::setprecision(5) << std::fixed << circumference << std::endl;
    double area = circumference * radius;
    std::cout << "Area is " << std::setprecision(5) << std::fixed << area << std::endl;
    
    return 0;
}
