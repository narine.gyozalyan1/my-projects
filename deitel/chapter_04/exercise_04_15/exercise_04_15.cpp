#include <iostream>
#include <unistd.h>
#include <iomanip>

int
main()
{
    while (true) {
        float sales;
        if (::isatty(STDIN_FILENO)) {
            std::cout << "Enter sales in dollars (-1 to end): ";
        }
        std::cin >> sales;
        if (-1 == static_cast<int>(sales)) {
            return 0;
        }
        if (sales <= 0) {
            std::cout << "Error 1: Sales can't be negative." << std::endl;
            return 1;
        }
        float salary = (200 + (sales * 0.09));
        std::cout << "Salary is: $" << std::setprecision(2) << std::fixed << salary << std::endl;
    }

    return 0;
}

