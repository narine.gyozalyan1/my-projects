#include <iostream>

int
main()
{
    int number = 1;
    std::cout << "N\t10*N\t100*N\t1000*N\n\n";
    while (number <= 5) {
        std::cout << number << "\t" 
                  << number * 10 << "\t"
                  << number * 100 << "\t"
                  << number * 1000 << "\t\n";
        ++number;
    }

    return 0;
}

