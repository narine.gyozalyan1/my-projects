#include <iostream>
#include <unistd.h>

int 
main()
{
    int size;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Enter the size of side of a square (between 1 and 20): ";
    }
    std::cin >> size;

    if (size < 1 || size > 20) {
        std::cout << "Error 1: Size of side is not valid value." << std::endl;
        return 1;
    }

    int row = 1;
    while (row <= size) {
        int column = 1;
        while (column <= size) {
            if (1 == row) {
                std::cout << "*";
            } else if (size == row) {
                std::cout << "*";
            } else if (1 == column) {
                std::cout << "*";
            } else if (size == column) {
                std::cout << "*";
            } else {
                std::cout << " ";
            }

            ++column;
        }
        std::cout << std::endl;
        ++row;
    }

    return 0;
}

