#include <iostream>
#include <unistd.h>

int
main()
{
    int number1;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Enter five digit number: ";
    }
    std::cin >> number1;
    if (number1 < 10000 || number1 > 99999) {
        std::cout << "Error 1: Invalid number." << std::endl;
        return 1;
    }
    int first = number1 / 10000;
    int second = number1 / 1000 % 10;
    int fourth = number1 / 10 % 10;
    int fifth = number1 % 10;
    
    std::cout << number1;
    if (first == fifth) {
        if (second == fourth) {
            std::cout << " is palindrome." << std::endl;
            return 0;
        }
    }
    std::cout << " is not palindrome." << std::endl;
    
    return 0;
}

