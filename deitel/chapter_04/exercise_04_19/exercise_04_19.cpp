#include <iostream>
#include <bits/stdc++.h>

int
main()
{
    int largest1 = INT_MIN;
    int largest2 = INT_MIN;
    int counter = 1;
    while (counter <= 10) {
        std::cout << "Enter the number: ";
        int number;
        std::cin >> number;
        
        if (number > largest1) {
            largest2 = largest1;
            largest1 = number;
        } else if (number > largest2){
            largest2 = number;
        }
        counter++;
    }
    std::cout << "The two largest values are " << largest1 << " and " << largest2 << std::endl;

    return 0;
}

