#include <iostream> 
#include <iomanip>
#include <unistd.h>

int
main()
{
    while (true) {
        int workedHours;
        if (::isatty(STDIN_FILENO)) {
            std::cout << "Enter hours worked (-1 to end): ";
        }
        std::cin >> workedHours;
        if (-1 == workedHours) {
            return 0;
        }
        if (workedHours < 0) {
            std::cout << "Error 1: Hours worked can't be negative." << std::endl;
            return 1;
        }
        float hourlyRate;
        if (::isatty(STDIN_FILENO)) {
            std::cout << "Enter hourly rate of the worker ($00.00): ";
        }
        std::cin >> hourlyRate;
        if (hourlyRate < 0) {
            std::cout << "Error 2: Hourly rate can't be negative." << std::endl;
            return 2;
        }
        float salary = workedHours * hourlyRate;
        if (workedHours > 40) {
            salary += (workedHours - 40) * 0.5 * hourlyRate;
        }
        std::cout << "Salary is $" << std::setprecision(2) << std::fixed << salary << std::endl;
    }
    return 0;
}

