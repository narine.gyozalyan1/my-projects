#include <iostream>
#include <iomanip>
#include <unistd.h>

int
main()
{
    while (true) {
        int accountNumber;
        if (::isatty(STDIN_FILENO)) {
            std::cout << "Enter account number (-1 to end): ";
        }
        std::cin >> accountNumber;
        if (-1 == accountNumber) {
            return 0;
        }
        if (accountNumber < 0) {
            std::cout << "Error 1: Account number can't be negative." << std::endl;
            return 1;
        }

        if (::isatty(STDIN_FILENO)) {
            std::cout << "Enter beginning balance: ";
        }
        float beginningBalance;
        std::cin >> beginningBalance;
        if (beginningBalance < 0) {
            std::cout << "Error 2: Beginning balance can't be negative." << std::endl;
            return 2;
        }

        if (::isatty(STDIN_FILENO)) {
            std::cout << "Enter total charges: ";
        }
        float totalCharges;
        std::cin >> totalCharges;
        if (totalCharges < 0) {
            std::cout << "Error 3: Total charges can't be negative." << std::endl;
            return 3;
        }

        if (::isatty(STDIN_FILENO)) {
            std::cout << "Enter total credits: ";
        }
        float totalCredits;
        std::cin >> totalCredits;
        if (totalCredits < 0) {
            std::cout << "Error 4: Total credits can't be negative." << std::endl;
            return 4;
        }

        if (::isatty(STDIN_FILENO)) {
            std::cout << "Enter credit limit: ";
        }
        float creditLimit;
        std::cin >> creditLimit;
        if (creditLimit < 0) {
            std::cout << "Error 5: Credit limit can't be negative." << std::endl;
            return 5;
        }

        float newBalance = beginningBalance + totalCharges - totalCredits;
        std::cout << "New balance is " << std::setprecision(2) << std::fixed << newBalance << std::endl;

        if (newBalance >= creditLimit) {
            std::cout << "Account: " << std::setprecision(2) << std::fixed << accountNumber << std::endl;
            std::cout << "Credit limit: " << std::setprecision(2) << std::fixed << creditLimit << std::endl;
            std::cout << "Balance: " << std::setprecision(2) << std::fixed << newBalance << std::endl;
            std::cout << "Credit Limit Exceeded" << std::endl;
        }
        std::cout << std::endl;
    }
    
    return 0;
}
