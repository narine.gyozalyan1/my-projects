#include <iostream>
#include <iomanip>
#include <unistd.h>

int
main()
{

    int totalMiles = 0;
    int totalGallons = 0;
    while (true) {
        int miles;
        if (::isatty(STDIN_FILENO)) {    
            std::cout << "Enter the miles used(-1 to quit): ";
        }
        std::cin >> miles;

        if (-1 == miles) {
            return 0;
        }
        if (miles < 0) {
            std::cout << "Error 1: Miles can't be negative." << std::endl;
            return 1;
        }

        int gallons;
        if (::isatty(STDIN_FILENO)) {
            std::cout << "Enter gallons: ";
        }
        std::cin >> gallons;
        if (gallons < 0) {
            std::cout << "Error 2: Gallons can't be negative." << std::endl;
            return 2;
        }

        float thisMiles = static_cast<float>(miles) / gallons;
        std::cout << "MPG this tankful: " << std::setprecision(6) << std::fixed << thisMiles << std::endl;
        totalMiles += miles;
        totalGallons += gallons;
        float totalMilesPerGallon = static_cast<float>(totalMiles) / totalGallons;
        std::cout << "Total MPG: " << std::setprecision(6) << std::fixed << totalMilesPerGallon << std::endl;
        std::cout <<  std::endl;
    }

    return 0;
}

