#include <iostream>
#include <unistd.h>

int
main()
{
    double side1;
    double side2;
    double side3;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Enter three numbers: ";
    }
    std::cin >> side1 >> side2 >> side3;
    if (side1 <= 0) {
        std::cout << "Error 1: Sides can't be negative or zero." << std::endl;
        return 1;
    }
    if (side2 <= 0) {
        std::cout << "Error 1: Sides can't be negative or zero." << std::endl;
        return 1;
    }
    if (side3 <= 0) {
        std::cout << "Error 1: Sides can't be negative or zero." << std::endl;
        return 1;
    }

    if (side1 * side1 + side2 * side2 == side3 * side3) {
        std::cout << "They could be the sides of a right triangle." << std::endl;
        return 0;
    }
    if (side2 * side2 + side3 * side3 == side1 * side1) {
        std::cout << "They could be the sides of a right triangle." << std::endl;
        return 0;
    }
    if (side1 * side1 + side3 * side3 == side2 * side2) {
        std::cout << "They could be the sides of a right triangle." << std::endl;
        return 0;
    }
    
    std::cout << "They could not be the sides of a right triangle." << std::endl;

    return 0;
}

