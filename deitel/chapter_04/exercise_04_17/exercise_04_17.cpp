#include <iostream>
#include <unistd.h>
#include <bits/stdc++.h>

int
main()
{
    int largest = INT_MIN;
    int counter = 1;
    while (counter <= 10) {
        int number;
        if (::isatty(STDIN_FILENO)) {
            std::cout << "Input number: ";
        }
        std::cin >> number;
        if (number > largest) {
            largest = number;
        }
        ++counter;
    }
    std::cout << "The largest is " << largest << std::endl;

    return 0;
}

