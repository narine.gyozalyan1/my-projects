#include <iostream>
#include <bitset>


long int
decimalToBinary(int decimalNumber)
{
    long int binary = 0;
    long int position = 1;
    while (decimalNumber != 0) {
        int remainder = decimalNumber % 2;
        binary += remainder * position;
        position *= 10;
        decimalNumber /= 2;
    }
    return binary;
}


int
main()
{   
    int number1 = 2;
    while (true) {
        std::cout << number1 << "\t" << std::bitset<32>(number1) << std::endl;

        number1 = number1 * 2;
    }

    return 0;
}

