#include <iostream>

int 
factorialOfNumber(int number)
{
    if (number < 0) {
        std::cout << "Error 1: Number can't be negative.";
        return 1;
    }

    int factorial = 1;
    if (number >= 2) {
        int counter = 2;
        while (counter <= number) {
            factorial *= counter;
            ++counter;
        }
    }

    return factorial;
}

double
estimateValueOfE(int limit)
{
    int counter = 1;
    double e = 1;
    while (counter < limit) {
        e += 1.0 / factorialOfNumber(counter);
        ++counter;
    }
   
    return e;
}

double
estimatePowerOfE(int limit, int power)
{
    double ex = 1;
    int counter = 1;
    while (counter < limit) {
        ex += static_cast<double>(power) / factorialOfNumber(counter);
        power *= power;
        ++counter;
    }

    return ex;
}

int
main()
{
    int number1;
    std::cout << "Enter the number: ";
    std::cin >> number1;
    std::cout << number1 << "! = " << factorialOfNumber(number1) << std::endl;
    
    int limit1;
    std::cout << "Enter limit: ";
    std::cin >> limit1;
    std::cout << "e = " << estimateValueOfE(limit1) << std::endl;
    
    std::cout << "Enter limit: ";
    std::cin >> limit1;
    int power;
    std::cout << "Enter x: ";
    std::cin >> power;
    std::cout << "e^x = " << estimatePowerOfE(limit1, power) << std::endl;

    return 0;
}

