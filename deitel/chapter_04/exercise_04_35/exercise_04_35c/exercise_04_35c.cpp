#include <iostream>
#include <iomanip>
#include <unistd.h>

int
main()
{
    int accuracy;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Enter accuracy of e: ";
    }
    std::cin >> accuracy;
    if (accuracy < 0) {
        std::cout << "Error 1: Invalid accuracy." << std::endl;
        return 1;
    }
    double ex = 1;
    int power;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Enter power:";
    }
    std::cin >> power;
    int power1 = power;
    int counter = 1;
    int factorial = 1;
    while (counter < accuracy) {
        ex += static_cast<double>(power) / factorial;
        power *= power1;
         ++counter;
         factorial *= counter;
    }

    std::cout << ex << std::endl;
    return 0;
}

