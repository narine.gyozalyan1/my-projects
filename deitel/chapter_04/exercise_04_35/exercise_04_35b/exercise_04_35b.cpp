#include <iostream>
#include <unistd.h>
#include <iomanip>

int
main()
{
    int accuracy;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Enter accuracy of e: ";
    }
    std::cin >> accuracy;
    if (accuracy < 0) {
        std::cout << "Error 1: Invalid accuracy." << std::endl;
        return 1;
    }
    int counter = 1;
    int factorial = 1;
    double e = 1.0;
    while (counter <= accuracy) {
        e += 1.0 / factorial;
        ++counter;
        factorial *= counter;
    }
   std::cout << "e = " << std::setprecision(6) << std::fixed << e << std::endl;

    return 0;
}
