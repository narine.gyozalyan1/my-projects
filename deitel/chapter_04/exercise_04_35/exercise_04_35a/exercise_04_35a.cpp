#include <iostream>
#include <unistd.h>

int
main()
{
    int number1;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Enter the number: ";
    }
    std::cin >> number1;
    if (number1 < 0) {
        std::cout << "Error 1: Number cannot be negative.";
        return 1;
    }
    int number = number1;
    int factorial = 1;
    while (number1 > 1) {
        factorial *= number1;
        --number1;
    }

    std::cout << number << "! = " << factorial << std::endl;
    return 0;
}

