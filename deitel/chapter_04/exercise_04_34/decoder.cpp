#include <iostream>
#include <unistd.h>

int
main()
{
    int encryptedNumber1;
    if (::isatty(STDIN_FILENO)) {    
        std::cout << "Enter the encrypted integer: ";
    }
    std::cin >> encryptedNumber1;
    if (encryptedNumber1 > 9999 || encryptedNumber1 < 1000) {
        std::cout << "Error 1: It is not four digit number." << std::endl;
        return 1;
    }

    int first = (encryptedNumber1 / 10 % 10 + 3) % 10;
    int second = (encryptedNumber1 % 10 + 3) % 10;
    int third = (encryptedNumber1 / 1000 + 3) % 10;
    int fourth = (encryptedNumber1 / 100 % 10 + 3) % 10;

    int originalNumber = 1000 * first + 100 * second + 10 * third + fourth;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Original integer is: ";
    }
    std::cout << originalNumber << std::endl;
    return 0;
}

