#include <iostream>
#include <unistd.h>

int
main()
{
    int originalNumber;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Enter the integer: ";
    }
    std::cin >> originalNumber;
    if (originalNumber > 9999 || originalNumber < 1000) {
        std::cout << "Error 1: It is not four digit number." << std::endl;
        return 1;
    }

    int first = (originalNumber / 1000 + 7) % 10;
    int second = (originalNumber / 100 % 10 + 7) % 10;
    int third = (originalNumber / 10 % 10 + 7) % 10;
    int fourth = (originalNumber % 10 + 7) % 10;
    
    int encryptedNumber = 1000 * third + 100 * fourth + 10 * first + second;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Encrypted integer is: ";
    }
    std::cout << encryptedNumber << std::endl;
    
    
 
    return 0;
}

