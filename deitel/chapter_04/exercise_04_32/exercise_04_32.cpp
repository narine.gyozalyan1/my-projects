#include <iostream>
#include <unistd.h>

int
main()
{
    double side1;
    double side2;
    double side3;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Enter three numbers: ";
    }
    std::cin >> side1 >> side2 >> side3;
    if (side1 <= 0 || side2 <= 0 || side3 <= 0) {
        std::cout << "Error 1: Sides can't be negative or zero." << std::endl;
        return 1;
    }
    if (side1 + side2 > side3) {
        if(side1 + side3 > side2) {
            if(side2 + side3 > side1) {
                std::cout << "These values could represent the sides of a triangle." << std::endl;
                return 0;
            }
        }
    }
    std::cout << "These values could not represent the sides of a triangle." << std::endl;

    return 0;
}
