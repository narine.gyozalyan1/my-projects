#include <iostream>

int
main()
{   
//    int size = 8;
    int row = 8;
    while (row != 0 ) {
        if (row % 2 == 1) {
            std::cout << " ";
        }
        int column = 8;
        while (column != 0) {
           std::cout << "* ";
           --column;
        }
        std::cout << std::endl;
        --row;
    }

    return 0;
}

