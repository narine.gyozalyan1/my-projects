#include <iostream>
#include <unistd.h>

int
main()
{   
    long int binary1;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Enter binary number: ";
    }
    std::cin >> binary1;
    
    long int temp = binary1;
    while (0 != temp) {
        if(temp % 10 > 1) {
            std::cout << "Error 1: Number is not binary." << std::endl;
            return 1;
        }
        temp /=  10;
    }

    int positionalValue = 1;
    long int decimal1 = 0;
    while (binary1 != 0) {
        decimal1 += ((binary1 % 10) * positionalValue);
        binary1 /= 10;
        positionalValue *= 2;
    }
    std::cout << "The decimal equivalent of given integer is: " << decimal1 << std::endl;

    return 0;
}

