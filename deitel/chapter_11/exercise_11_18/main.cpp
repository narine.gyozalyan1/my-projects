#include "PhoneNumber.hpp"

#include <cstring>
#include <iostream>

int
main()
{
    PhoneNumber phone; 
    std::cout << "Enter phone number in the form (123) 456-7890:" << std::endl;
    std::cin >> phone;
    std::cout << "The phone number entered was: ";
    phone.operator<<(std::cout);
    std::cout << std::endl;
    phone << std::cout;
    std::cout << std::endl;

    return 0;
}

