#include "PhoneNumber.hpp"

#include <iostream>
#include "cstring"
#include <iomanip>

std::istream&
operator>>(std::istream& input, PhoneNumber& rhv)
{
    input.ignore();
    input >> std::setw(3) >> rhv.areaCode_;
    input.ignore(2); 
    input >> std::setw(3) >> rhv.exchange_;
    input.ignore(); 
    input >> std::setw(4) >> rhv.line_;
    return input; 
}

std::ostream&
PhoneNumber::operator<<(std::ostream& output)
{
    output << "(" << areaCode_ << ") "
           << exchange_ << "-" << line_;
    return output;
}

