#ifndef __PHONENUMBER_HPP__
#define __PHONENUMBER_HPP__

#include <iostream>
#include "cstring"

class PhoneNumber
{
    friend std::istream& operator>>(std::istream& input, PhoneNumber& rhv);
public:
    std::ostream& operator<<(std::ostream& output);
private:
    std::string areaCode_;
    std::string exchange_;
    std::string line_;
};

#endif ///__PHONENUMBER_HPP__

