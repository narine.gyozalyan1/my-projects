#include "String.hpp"

#include <iostream>
#include <cstring>
#include <iomanip>

std::ostream&
operator<<(std::ostream& out, const String& s)
{
    out << s.sPtr_;
    return out; /// enables cascading
}

std::istream&
operator>>(std::istream& in, String& s)
{
    char temp[100];
    in >> std::setw(100) >> temp;
    s = temp;
    return in;
}

String::String(const char* s) /// default/conversion constructor
    : length_(0), sPtr_(NULL)
{
    setString(s); /// sPtr_ == { '\0' }
}

String::String(const String& rhv) /// copy constructor
    : length_(rhv.length_), sPtr_(NULL)
{
    setString(rhv.sPtr_);
}

const String&
String::operator=(const String& rhv)
{
    if (&rhv != this) { /// avoid self assignment
        delete [] sPtr_;
        setString(rhv.sPtr_);
    }
    return *this; /// enables cascaded assignments
}

String::~String()
{ /// destructor
    delete [] sPtr_;
    sPtr_ = NULL;
}

void
String::setString(const char* s2)
{
    length_ = (s2 != NULL) ? ::strlen(s2) : 0;
    sPtr_ = new char[length_ + 1 ]; /// allocate memory
    if (s2 != NULL ) {
        ::strcpy(sPtr_, s2);
    } else {
        sPtr_[0] = '\0';
    }
}

String
String::operator+(const String& rhv) const
{
    char* t = new char[length_ + rhv.length_ + 1];
    std::strcpy(t, sPtr_);
    std::strcpy(t + length_, rhv.sPtr_);
    const String result(t);
    delete [] t;
    return result;
}

int
String::getLength() const
{
    return length_;
}

