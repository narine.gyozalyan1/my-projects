#include "String.hpp"

#include <iostream>
#include <unistd.h>
int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input string 1:";
    }
    String s1;
    std::cin >> s1;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input string 2:";
    }
    String s2;
    std::cin >> s2;

    String s3 = s1 + s2;
    std::cout << s3 << std::endl;

    return 0;
}

