#ifndef __STRING_HPP__
#define __STRING_HPP__

#include <iostream>

class String
{
    friend std::istream& operator>>(std::istream& in, String& s);
    friend std::ostream& operator<<(std::ostream& out, const String& s);
public:
    String(const char* rhv = ""); /// conversion/default constructor
    String(const String& rhv); /// copy constructor
    ~String(); /// destructor
    const String& operator=(const String& rhv); /// assignment operator
    String operator+(const String& rhv) const; /// concatenation operator
    int getLength() const;
private:
    void setString(const char* rhv); /// utility function    
private:
    int length_; /// string length (not counting null terminator)
    char* sPtr_; /// pointer to start of pointer-based string
};

#endif /// __STRING_HPP__

