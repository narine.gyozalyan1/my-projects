#include "Complex.hpp"

#include <iostream>

std::ostream&
operator<<(std::ostream& out, const Complex& c) {
    out << '(' << c.real_ << ", " << c.imaginary_ << ')';
    return out;
}

std::istream&
operator>>(std::istream& in, Complex& c)
{
    in >> c.real_;
    in >> c.imaginary_;
    return in;
}

Complex::Complex(const double realPart, const double imaginaryPart)
    : real_(realPart), imaginary_(imaginaryPart)
{}

Complex
Complex::operator+(const Complex& rhv) const
{
    return Complex(real_ + rhv.real_, imaginary_ + rhv.imaginary_);
}

Complex
Complex::operator-(const Complex& rhv) const
{
    return Complex(real_ - rhv.real_, imaginary_ - rhv.imaginary_);
}

Complex
Complex::operator*(const Complex& rhv) const
{
    return Complex(real_ * rhv.real_ - imaginary_ * rhv.imaginary_, imaginary_ * rhv.real_ + real_ * rhv.imaginary_);
}

bool
Complex::operator==(const Complex& rhv) const
{
    return (real_ == rhv.real_ && imaginary_ == rhv.imaginary_);
}

bool
Complex::operator!=(const Complex& rhv) const
{
    return !(*this == rhv);
}

