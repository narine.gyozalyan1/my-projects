#ifndef __COMPLEX_HPP__
#define __COMPLEX_HPP__

#include <iostream>

class Complex
{
    friend std::ostream& operator<<(std::ostream& output, const Complex& rhv);
    friend std::istream& operator>>(std::istream& input, Complex& rhv);
public:
    Complex(const double realPart = 0.0, const double imaginaryPart = 0.0);
    Complex operator+(const Complex& rhv) const;
    Complex operator-(const Complex& rhv) const;
    Complex operator*(const Complex& rhv) const;
    bool operator==(const Complex& rhv) const;
    bool operator!=(const Complex& rhv) const;
private:
    double real_;
    double imaginary_;
};

#endif /// __COMPLEX_HPP__

