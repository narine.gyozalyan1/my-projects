#include "Complex.hpp"

#include <iostream>

int
main()
{
    Complex x;
    const Complex y(4.3, 8.2);
    const Complex z(3.3, 1.1);
    std::cout << "x: " << x << std::endl;
    std::cout << "y: " << y << std::endl;
    std::cout << "z: " << z << std::endl;
    x = y + z;
    std::cout << "\nx = y + z = " << x << " = " << y << " + " << z << std::endl;
    x = y - z;
    std::cout << "\nx = y - z = " << x << " = " << y << " - " << z << std::endl;
    x = y * z;
    std::cout << "\nx = y * z = " << x << " = " << y << " * " << z << std::endl;
    if (y == z) {
        std::cout << "\ny is equal to z" << std::endl;
    } else {
        std::cout << "\ny is not equal to z" << std::endl;
    }

    return 0;
}

