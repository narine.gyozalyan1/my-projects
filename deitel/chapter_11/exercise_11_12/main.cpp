#include "Array.hpp"

#include <iostream>
#include <unistd.h>

int
main()
{
    const int SIZE1 = 7;
    int arr[SIZE1] = { 4, 87, 21, 3, 2, 1, 5 };
    std::cout << "The array is ";
    for (int i = 0; i < SIZE1; ++i) {
        std::cout << arr[i] << " ";
    }
    std::cout << std::endl;
    const Array arr1(arr, SIZE1);

    int index;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input the index: ";
    } 
    std::cin >> index;
    if (index < 0 || index >= SIZE1) {
        std::cout << "Error 1: Invalid index!" << std::endl;
        return 1;
    }
    std::cout << "The " << index << "th largest value is " << arr1[index] << std::endl;

    return 0;
}

