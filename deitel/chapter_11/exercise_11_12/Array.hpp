#ifndef __ARRAY_HPP__
#define __ARRAY_HPP__

#include <iostream>

class Array
{
public:
    Array();
    Array(const int* array, const int size);
    Array(const Array& rhv);
    void sort();
    int& operator[](const int index);
    const int& operator[](const int index) const;
    ~Array();
private:
    int* array_;
    int size_;
};

#endif /// __ARRAY_HPP__

