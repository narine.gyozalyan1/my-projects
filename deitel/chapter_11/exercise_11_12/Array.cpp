#include "Array.hpp"
#include <cassert>
#include <iostream>


Array::Array()
    : array_(NULL), size_(0)
{}

Array::Array(const int* array, const int size)
    : array_(new int[size]), size_(size)
{
    for (int i = 0; i < size; ++i) {
         array_[i] = array[i];
    }
    sort();
}

Array::Array(const Array& rhv)
    : array_(new int[rhv.size_]), size_(rhv.size_)
{
    for (int i = 0; i < size_; ++i) {
        array_[i] = rhv.array_[i];
    }
}

Array::~Array()
{
    if (array_!= NULL) {
        delete [] array_;
        array_ = NULL;
    }
}

void
Array::sort()
{
    for (int i = 0;  i < size_ - 1; ++i) {
        for (int j = 0; j < size_ - i - 1; ++j) {
            if (array_[j] < array_[j + 1]) {
                int temp = array_[j];
                array_[j] = array_[j + 1];
                array_[j + 1] = temp;
            }
        }
    }
}

int&
Array::operator[](const int index)
{
    assert(index >= 0 && index < size_);
    return array_[index];
}

const int&
Array::operator[](const int index) const
{
    assert(index >= 0 && index < size_);
    return array_[index];
}

