#include <iostream>
#include <list>
#include <iterator>
#include <algorithm>

template <typename T>
void
concatenate(std::list<T>& lhv, const std::list<T>& rhv)
{
    typename std::list<T>::const_iterator it1 = rhv.begin();
    for ( ; it1 != rhv.end(); ++it1) {
        lhv.push_back(*it1);
    }
}

template <typename T>
void
merge(const std::list<T>& lhv, const std::list<T>& rhv, std::list<T>& newList)
{
    newList = lhv;
    concatenate(newList, rhv);
    newList.sort();
}

template <typename T>
void
print(const std::list<T>& rhv)
{    
    typename std::list<T>::const_iterator it1 = rhv.begin();
    typename std::list<T>::const_iterator it2 = rhv.end();
    while (it1 != it2) {
        std::cout << *it1 << " ";
        ++it1;
    }
    std::cout << std::endl;
}

int
main()
{
    typedef std::list<int> L;

    L l1;
    l1.push_back(5);
    l1.push_back(3);
    l1.push_back(7);
    l1.push_back(9);
    l1.push_back(1);

    print(l1);
    
    L l2;
    l2.push_back(1);
    l2.push_back(2);
    l2.push_back(4);
    l2.push_back(6);
    l2.push_back(10);

    print(l2);

    L l3;

    merge(l1, l2, l3);

    std::cout << "Concatenated list is ";
    
    print(l3);

    std::cout << std::endl;

    return 0;
}

