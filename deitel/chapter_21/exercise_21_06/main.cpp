#include <iostream>
#include <list>
#include <iterator>

template <typename T>
void
concatenate(std::list<T>& lhv, const std::list<T>& rhv)
{
    typename std::list<T>::const_iterator it1 = rhv.begin();
    for ( ; it1 != rhv.end(); ++it1) {
        lhv.push_back(*it1);
    }
}

template <typename T>
void
print(const std::list<T>& rhv)
{    
    typename std::list<T>::const_iterator it1 = rhv.begin();
    typename std::list<T>::const_iterator it2 = rhv.end();
    while (it1 != it2) {
        std::cout << *it1 << " ";
        ++it1;
    }
    std::cout << std::endl;
}

int
main()
{
    typedef std::list<char> L;

    L l1(4, 'a');
    print(l1);
    const L l2(3, 'b');
    print(l2);
    concatenate(l1, l2);

    std::cout << "Concatenated list is ";
    
    print(l1);

    std::cout << std::endl;

    return 0;
}

