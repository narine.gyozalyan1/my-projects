#include <iostream>

int
main()
{
    int number1, number2;
    
    std::cout << "Enter the first integer: ";
    std::cin >> number1;
    std::cout << "Enter the second integer: ";
    std::cin >> number2;

    if (0 == number2) {
        std::cout << "Error 1: The second integer cannot be 0." << std::endl;
        return 1;
    }
    if (number1 % number2 == 0) {
        std::cout << number1 << " is a multiple of " << number2 << std::endl;
        return 0;
    }
    std::cout << number1 << " isn't a multiple of " << number2 << std::endl;

    return 0;
}

