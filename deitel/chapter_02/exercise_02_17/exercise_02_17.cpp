#include <iostream>

int
main()
{
    /// a.Using one statement with one stream insertion operator.
    std::cout << "1 2 3 4\n";
    /// b.Using one statement with four stream insertion operators.
    std::cout << "1 " << "2 " << "3 " << "4 " << std::endl;
    /// c.Using four statements
    std::cout << "1 ";
    std::cout << "2 ";
    std::cout << "3 ";
    std::cout << "4" << std::endl;

    return 0;
}

