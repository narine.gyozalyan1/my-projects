#include <iostream>
/// 2.16 Write a program that asks the user to enter two numbers, obtains the two numbers from the user and prints the sum, product, difference, and quotient of the two numbers.

int
main()
{
    int number1, number2;

    std::cout << "Enter first integer: ";
    std::cin >> number1;

    std::cout << "Enter second integer: ";
    std::cin >> number2;

    int sum = number1 + number2;
    std::cout << "Sum is " << sum << std::endl;
    int product = number1 * number2;
    std::cout << "Product is " << product << std::endl;
    int difference = number1 - number2;
    std::cout << "Difference is " << difference << std::endl;
    if (0 == number2) {
        std::cout << "Error 1: Second integer cannot be 0." << std::endl;
        return 1;
    }
    
    int quotient = number1 / number2;
    std::cout << "Quotient is " << quotient << std::endl;
    
    return 0;
}

