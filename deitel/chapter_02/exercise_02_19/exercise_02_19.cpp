#include <iostream>

int
main()
{
    int number1, number2, number3;

    std::cout << "Enter first integer: ";
    std::cin >> number1;
    std::cout << "Enter second integer: ";
    std::cin >> number2;
    std::cout << "Enter third integer: ";
    std::cin >> number3;

    int sum = number1 + number2 + number3;
    std::cout << "Sum is " << sum << std::endl;

    int average = sum / 3;
    std::cout << "Average is " << average << std::endl;

    int product = number1 * number2 * number3;
    std::cout << "Product is " << product << std::endl;

    int smallest = number1;
    int largest = number1;

    if (number2 > largest) {
        largest = number2;
    }
    if (number3 > largest) {
        largest = number3;
    }
    if (number2 < smallest) {
        smallest = number2;
    }
    if (number3 < smallest) {
        smallest = number3;
    }
    std::cout << "Smallest is " << smallest << std::endl;
    std::cout << "Largest is " << largest << std::endl;

    return 0;
}

