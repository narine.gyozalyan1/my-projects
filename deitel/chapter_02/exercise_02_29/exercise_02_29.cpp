#include <iostream>

int
main()
{
    std::cout << "integer    square     cube" << std::endl;
    std::cout << "0\t\t\t0\t\t\t0" << std::endl;
    std::cout << "1\t\t\t" << 1 * 1 << "\t\t\t" << 1 * 1 * 1 << std::endl;
    std::cout << "2\t\t\t" << 2 * 2 << "\t\t\t" << 2 * 2 * 2 << std::endl;
    std::cout << "3\t\t\t" << 3 * 3 << "\t\t\t" << 3 * 3 * 3 << std::endl;
    std::cout << "4\t\t\t" << 4 * 4 << "\t\t\t" << 4 * 4 * 4 << std::endl;
    std::cout << "5\t\t\t" << 5 * 5 << "\t\t\t" << 5 * 5 * 5 << std::endl;
    std::cout << "6\t\t\t" << 6 * 6 << "\t\t\t" << 6 * 6 * 6 << std::endl;
    std::cout << "7\t\t\t" << 7 * 7 << "\t\t\t" << 7 * 7 * 7 << std::endl;
    std::cout << "8\t\t\t" << 8 * 8 << "\t\t\t" << 8 * 8 * 8 << std::endl;
    std::cout << "9\t\t\t" << 9 * 9 << "\t\t\t" << 9 * 9 * 9 << std::endl;
    std::cout << "10\t\t\t" << 10 * 10 << "\t\t\t" << 10 * 10 * 10 << std::endl;

    return 0;
}
 
