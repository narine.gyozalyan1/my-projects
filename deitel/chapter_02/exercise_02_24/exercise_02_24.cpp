#include <iostream>

int
main()
{
    int number1;
    std::cout << "Enter an integer: ";
    std::cin >> number1;

    if (number1 % 2 == 0) {
        std::cout << number1 << " is even" << std::endl;
        return 0;
    }
    std::cout << number1 << " is odd" << std::endl;

    return 0;
}

