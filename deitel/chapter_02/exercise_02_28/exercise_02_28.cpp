#include <iostream>

int
main()
{
    int fiveDigit;
    std::cout << "Enter a five-digit integer: ";
    std::cin >> fiveDigit;
    if (fiveDigit < 10000) {
        std::cout << "Error 1: It's less than a five-digit integer." << std::endl;
        return 1;
    }
    if (fiveDigit > 99999) {
        std::cout << "Error 2: It's more than a five-digit integer." << std::endl;
        return 2;
    }
    int digit1 = fiveDigit / 10000;
    int digit2 = (fiveDigit / 1000) % 10;
    int digit3 = (fiveDigit / 100) % 10;
    int digit4 = (fiveDigit / 10) % 10;
    int digit5 = fiveDigit % 10;
    std::cout << digit1 << "   " << digit2 << "   " << digit3 << "   " << digit4 << "   " << digit5 << std::endl;

    return 0;
}

