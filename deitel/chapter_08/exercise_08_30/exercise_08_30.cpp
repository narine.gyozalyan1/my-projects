#include <iostream>
#include <unistd.h>
#include <cstring>

int
main()
{
    char string1[80];
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Enter first string: ";
    }
    std::cin.getline(string1, 80, '\n');
    char string2[80];
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Enter second string: ";
    }
    std::cin.getline(string2, 80, '\n');

    int result = std::strcmp(string1, string2);

    if (result > 0) {
        std::cout << "First string is greater than the second." << std::endl;
        return 0;
    }
    if (result < 0) {
        std::cout << "First string is less than the second." << std::endl;
        return 0;
    }
    std::cout << "The strings are equal." << std::endl;

    return 0;
}

