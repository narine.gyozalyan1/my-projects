#include <iostream>
#include <unistd.h>
#include <cstring>

int
main()
{
    const int SIZE = 81;
    char sentence[SIZE];
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Enter a sentence (max " << SIZE - 1 << "): ";
    }
    std::cin.getline(sentence, SIZE);
    char* tokenPtr = std::strtok(sentence, " ");
    char* reversed[81];
    int count = 0;
    while (NULL != tokenPtr) {
        reversed[count] = tokenPtr;
        tokenPtr = std::strtok(NULL, " ");
        ++count;
    }
    std::cout << "After reverse strtoks are: " << std::endl;
    for (int i = count - 1; i >= 0; --i) {
        std::cout << reversed[i] << std::endl;
    }

    return 0;
}

