#include <iostream>
#include <cstdlib>
#include <ctime>
#include <unistd.h>
#include <cstring>

int selectRandom();

int
main()
{
    const int LENGHT = 6;
    if (::isatty(STDIN_FILENO)) {
        ::srand(::time(0));
    }
    const char* article[] = {"the", "a", "one", "some", "any"};
    const char* noun[] = {"boy" ,"girl", "dog", "town", "car"};
    const char* verb[] = {"drove", "jumped", "ran", "walked", "skipped"};
    const char* preposition[] = {"to", "from", "over", "under", "on"};
    const char** partsOfSpeech[LENGHT] = {article, noun, verb, preposition, article, noun};

    const int SIZE = 40;
    const int COUNT = 20;
    for (int i = 0; i < COUNT; ++i) {
        char sentence[SIZE] = "";
        for (int j = 0; j < LENGHT; ++j) {
            std::strcat(sentence, partsOfSpeech[j][selectRandom()]);
            std::strcat(sentence, " ");
        }

        sentence[std::strlen(sentence) - 1] = '.';
        sentence[0] = std::toupper(sentence[0]);
        std::cout << sentence << std::endl;
    }
    std::cout << std::endl;
    return 0;
}

int
selectRandom()
{
    return (::rand() % 4);
}

