#include <iostream>
#include <cstring>

int
main()
{
    const int SIZE = 20;
    const int ARRAY_SIZE = 50;

    char sentence[ARRAY_SIZE] = {};
    std::cin.getline(sentence, ARRAY_SIZE, '\n');

    char* token = std::strtok(sentence, " ");

    while (token != NULL) {
        char word[SIZE] = {};
        std::strcat(word, token + 1);
        std::strncat(word, token, 1);
        std::strcat(word, "ay");
        std::cout << word << ' ';
        token = std::strtok(NULL, " ");
    }
    std::cout << std::endl;
    return 0;
}

