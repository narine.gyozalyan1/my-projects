#include <iostream>
#include <unistd.h>
#include <cstring>

int
main()
{
    const int SIZE = 80;
    char string1[SIZE];
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Enter first string (max " << SIZE - 1 << "): ";
    }
    std::cin.getline(string1, SIZE, '\n');
    
    char string2[SIZE];
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Enter second string (max " << SIZE - 1 << "): ";
    }
    std::cin.getline(string2, SIZE, '\n');
    
    int characterNumber;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Enter number of characters to compare: ";
    }
    std::cin >> characterNumber;
    if (characterNumber < 0 || characterNumber >= SIZE) {
        std::cout << "Error 1: Invalid number of characters." << std::endl;
        return 1;
    }
    const int result = std::strncmp(string1, string2, characterNumber);

    if (result > 0) {
        std::cout << "First string is greater than the second." << std::endl;
        return 0;
    }
    if (result < 0) {
        std::cout << "First string is less than the second." << std::endl;
        return 0;
    }
    std::cout << "The strings are equal." << std::endl;

    return 0;
}

