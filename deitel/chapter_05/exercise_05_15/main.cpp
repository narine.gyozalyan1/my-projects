#include "GradeBook.hpp"
#include <iostream>

int
main()
{
    GradeBook myGradeBook("CS101 C++ Programming");
    myGradeBook.displayMessage();
    myGradeBook.inputGrades();
    myGradeBook.displayGradeReport();
    std::cout << "Averege is " << myGradeBook.averageGrade() << std::endl;

    return 0;
}

