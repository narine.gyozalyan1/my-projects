#include "GradeBook.hpp"
#include <iostream>
#include <cstdio>
#include <unistd.h>

GradeBook::GradeBook(std::string courseName)
{
    setCourseName(courseName);
    aCount_ = 0;
    bCount_ = 0;
    cCount_ = 0;
    dCount_ = 0;
    fCount_ = 0;
} 

void
GradeBook::setCourseName(std::string courseName)
{
    if (courseName.length() <= 25) {
        courseName_ = courseName;
    } else {
        courseName_ = courseName.substr(0, 25);
        std::cout << "Course name \"" << courseName << "\" exceeds maximum length (25).\n"
                  << "Limiting course name to first 25 characters.\n" << std::endl;
    }
}

std::string
GradeBook::getCourseName()
{
    return courseName_;
}

void
GradeBook::displayMessage()
{
    std::cout << "Welcome to the grade book for\n" << getCourseName() << "!\n" << std::endl;
}

void
GradeBook::inputGrades()
{
    int grade;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Enter the letter grades." << std::endl
                  << "Enter the EOF character to end input." << std::endl;
    }
    while ((grade = std::cin.get()) != EOF) {
        switch (grade) {        
        case 'A': case 'a': aCount_++; break;
        case 'B': case 'b': bCount_++; break;
        case 'C': case 'c': cCount_++; break;
        case 'D': case 'd': dCount_++; break;
        case 'F': case 'f': fCount_++; break;
        case '\n':
        case '\t':
        case ' ':                      break;
        default: std::cout << "Incorrect letter grade entered."
                           << "Enter a new grade." << std::endl;
                                       break;
        }
    }
}

void
GradeBook::displayGradeReport()
{
    std::cout << "\n\nNumber of students who received each letter grade:"
              << "\nA: " << aCount_
              << "\nB: " << bCount_
              << "\nC: " << cCount_
              << "\nD: " << dCount_
              << "\nF: " << fCount_
              << std::endl;
}

double
GradeBook::averageGrade()
{
    int count = aCount_ + bCount_ + cCount_ + dCount_ + fCount_;
    double average;
    if (0 == count) {
        average = 0;
    } else {
        double sum = 4.0 * aCount_ + 3.0 * bCount_ + 2.0 * cCount_ + 1.0 * dCount_;
        average = sum / count;
    }
    
    return average;
}

