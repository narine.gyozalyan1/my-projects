#include <iostream>

int
main()
{
    int product = 1;
    for (int integer = 1; integer <= 15; integer += 2) {
        product *= integer;
    }
    std::cout << "Product of integers is " << product << std::endl;

    return 0;
}

