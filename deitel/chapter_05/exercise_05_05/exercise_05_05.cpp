#include <iostream>

int
main()
{
    int count;
    std::cout << "Enter the  number of values: ";
    std::cin >> count;
    if (count <=0) {
        std::cout << "Error 1: Count can't be negative" << std::endl;
        return 1;
    }
    int sum = 0;
    for (int i = 0; i < count; ++i) {
        int currentValue;
        std::cout << "Enter value: ";
        std::cin >>currentValue;
        sum += currentValue;
    }
    std::cout << "Sum is " << sum << std::endl;

    return 0;
}

