#include <iostream>
#include <unistd.h>

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Enter five numbers (each between 1 and 30): "; 
    }
    for (int i = 1; i <= 5; ++i) {
        int number1;
        std::cin >> number1;
        if (number1 < 1 || number1 > 30) {
            std::cerr << "Error 1: Invalid number." << std::endl;
            return 1;
        }
        for (int j = 1; j <= number1; ++j) {
            std::cout << "*";
        }
        std::cout << std::endl;
    }

    return 0;
}

