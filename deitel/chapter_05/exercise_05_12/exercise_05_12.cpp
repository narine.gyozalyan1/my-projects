#include <iostream>
#include <iomanip>

int
main()
{
    for (int i = 1; i <= 10; ++i) {
        for (int j = 1; j <= i; ++j) {
            std::cout << "*";
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
    for (int i = 1; i <= 10; ++i) {
        for (int j = 10; j >= i; --j) {
            std::cout << "*"; 
        }
        std::cout << std::endl;  
    }

    for (int i = 1; i <= 10; ++i) {
        for (int k = 1; k < i; ++k) {
            std::cout << " ";
        }
        for (int j = 10; j >= i; --j) {
            std::cout << "*"; 
        }
        std::cout << std::endl;
    }
    for (int i = 1; i <= 10; ++i) {
        for (int k = 10; k >= i; --k) {
            std::cout << " ";
        }
        for (int j = 1; j < i; ++j) {
            std::cout << "*"; 
        }
 
        std::cout << std::endl;  
    }

    for (int i = 1; i <= 10; ++i) { 
        for (int j = 1; j <= i; ++j) {
            std::cout << "*";
        }
        std::cout << std::setw(12 - i);
        for (int j = 10; j >= i; --j) {
            std::cout << "*"; 
        }
        std::cout << std::setw(2 * i);
        for (int j = 10; j >= i; --j) {
            std::cout << "*"; 
        }
        std::cout << std::setw(12 - i);
        for (int j = 1; j <= i; ++j) {
            std::cout << "*"; 
        }
        std::cout << std::endl;
    }

    return 0;
}

