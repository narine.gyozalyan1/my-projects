#include <iostream>

int
main()
{
    int x = 4;
    int y = 8;
    int original = !(x < 5) && !(y >= 7);
    int result = !((x < 5) || (y >= 7));
    std::cout << "!(x < 5) && !(y >= 7) is ";
    if (original != result) {
        std::cout << " not ";
    }
    std::cout << "equivalent to !((x < 5) || (y >= 7))" << std::endl;
    
    int a = 5;
    int b = 8;
    int g = 5;
    original = !(a == b) || !(g != 5);
    result = !((a == b) && (g != 5));
    std::cout << "!(a == b) || !(g != 5) is ";
    if (original != result) {
        std::cout << " not ";
    }
    std::cout << "equivalent to !((a == b) && (g != 5))" << std::endl;

    x = 3;
    y = 8;
    original = !((x <= 8) && (y > 4));
    result = !(x <= 8) || !(y > 4);
    std::cout << "!((x <= 8) && (y > 4)) is ";
    if (original != result) {
        std::cout << " not ";
    }
    std::cout << "equivalent to !(x <= 8) || !(y > 4)" << std::endl;

    x = 9;
    y = 2;
    original = !((x > 4) || (y <= 6));
    result = !(x > 4) && !(y <= 6);
    std::cout << "!((x > 4) || (y <= 6)) is ";
    if (original != result) {
        std::cout << " not ";
    }
    std::cout << "equivalent to !(x > 4) && !(j <= 6)" << std::endl;
 }

