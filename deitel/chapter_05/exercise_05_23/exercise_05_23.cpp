#include <iostream>
#include <cmath>
#include <stdlib.h> 

int
main()
{
    const int SIZE = 8;
    int size1 = SIZE / 2;
    for (int i = -size1; i <= size1; ++i) {
        for (int j = -size1; j <= size1; ++j) {
            if (std::abs(i) + std::abs(j) <= size1) {
                std::cout << "*";
            } else {
                std::cout << " ";
            }
        }
        std::cout << std::endl;
    }

    return 0;
}

