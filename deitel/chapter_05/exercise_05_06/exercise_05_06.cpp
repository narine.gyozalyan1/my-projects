#include <iostream>
#include <iomanip>
#include <unistd.h>

int
main()
{
    int sum = 0;
    int count = 0;
    while (true) {
        int current = 0;
        if (::isatty(STDIN_FILENO)) {
            std::cout << "Enter the value: ";
        }
        std::cin >> current;
        if (9999 == current) {
            break;
        }
        ++count;
        sum += current;
    }
    if (0 == count) {
        std::cout << "Cannot calculate the average. There is no number." << std::endl;
        return 0;
    }
    double average = static_cast<double>(sum) / count;
    std::cout << "The average is " << std::fixed << std::setprecision(2)<< average << std::endl;

    return 0;
}

