#include <iostream>

int
main()
{
    int factorial = 1;

    for (int current = 1; current <= 5 ; current++) {
        factorial *= current;
        std::cout << "Factorial of\t" << current << "\tis\t" << factorial << std::endl;
    }

    return 0;
}

