#include <iostream>
#include <cmath>
#include <unistd.h>

int
main()
{
    int size;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Enter an odd number in the range 1 to 19: ";
    }
    std::cin >> size;
    if (size < 1 || size > 19) {
        std::cout << "Error 1: Number is out of range." << std::endl;
        return 1;
    }
    if (size % 2 == 0) {
        std::cout << "Error 2: Number is not odd." << std::endl;
        return 2;
    }

    int size1 = size / 2;
    for (int i = -size1; i <= size1; ++i) {
        for (int j = -size1; j <= size1; ++j) {
            if (std::abs(i) + std::abs(j) <= size1) {
                std::cout << "*";
            } else {
                std::cout << " ";
            }
        }
        std::cout << std::endl;
    }

    return 0;
}

