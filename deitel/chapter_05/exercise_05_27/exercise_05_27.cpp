#include <iostream>

int
main()
{
    for (int count = 1; count <= 10; count++) {
        if (5 != count) {
            std::cout << count << " ";
        }
    }
    std::cout << std::endl;
    return 0;
}
