#include <iostream>

int
main()
{
    int count = 1;
    for (; count < 5; ++count) {
        std::cout << count << " ";
    }
    
    std::cout << "\nBroke out of loop at count = " << count << std::endl;
    
    return 0;
}
