#include <iostream>
#include <unistd.h>

int
main()
{
    if (::isatty(STDIN_FILENO)) {    
        std::cout << "Enter the count of numbers: ";
    }
    int count;
    std::cin >> count;

    if (count <= 0) {
        std::cout << "Error 1: Count can't be negative" << std::endl;
        return 1;
    }

    int smallest = 2147483647;

    for (int i = 0; i < count; ++i) {
        if (::isatty(STDIN_FILENO)) {    
            std::cout << "Enter number: ";
        }
        int current;
        std::cin >> current;
        if (current < smallest) {
            smallest = current;
        }
    }
    std::cout << "Smallest is " << smallest << std::endl;

    return 0;
}

