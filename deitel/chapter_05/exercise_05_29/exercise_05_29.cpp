#include <iostream>
#include <iomanip>

int
main()
{
    std::cout << std::setw(10) << "Year" << std::setw(41) << "Amount on deposit" << std::endl;
    std::cout << std::fixed << std::setprecision(2);
    
    for (int rate = 5; rate <= 10; ++rate) {
        double percent = 1.0 + rate / 100.0;
        double principal = 24.00;
        for (int year = 1; year <= 396; ++year) {
            principal *= percent;
            std::cout << std::setw(10) << year << std::setw(41) << principal << std::endl;   
        }
    }

    return 0;
}
