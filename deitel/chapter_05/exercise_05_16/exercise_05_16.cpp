#include <iostream>
#include <iomanip>

int
main()
{
    int percent = 5;
    std::cout << "Year" << std::setw(21) << "Amount on deposit" << std::endl;
    std::cout << std::fixed << std::setprecision(2);
    int rate = 100 + percent;
    
    int principal = 100000 * rate / 100;
    for (int year = 1; year <= 10; ++year) { 
        int dollars = principal / 100;
        int cents = principal % 100;
        std::cout << std::setw(4) << year << std::setw(21) << dollars << "." << cents << std::endl;
    
        principal = principal * rate / 100;
    }
    
    return 0;
}

