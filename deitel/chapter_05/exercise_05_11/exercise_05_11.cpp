#include <iostream>
#include <iomanip>

int
main()
{
    std::cout << std::fixed << std::setprecision(2);
    for (int percent = 5; percent <= 10; ++percent) {
        double rate = 1.0 + static_cast<double>(percent) / 100;
        std::cout << "For " << percent << " percent interest rates" << std::endl;
        std::cout << "Year" << std::setw(21) << "Amount on deposit" << std::endl;
    
        double principal = 1000.0;
        for (int year = 1; year <= 10; ++year) {
            principal *= rate;
            std::cout << std::setw(4) << year << std::setw(21) << principal << std::endl;
        }
        std::cout << std::endl;
    }
    return 0;
}
