#include <iostream>
#include <cassert>
#include <unistd.h>

int
main()
{
    int paycode;
    double employeesPay;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Employee:\n";
        for (int paycode = 1; paycode < 5; ++paycode) {
            std::cout << '\t' << paycode << " - " ;
            switch (paycode) {
            case 1: std::cout << "Managers";           break;
            case 2: std::cout << "Hourly workers";     break;
            case 3: std::cout << "Commission workers"; break;
            case 4: std::cout << "Piece workers";      break;
            default: assert(0);                        break;
            }
            std::cout << "\n";
        }
        std::cout << "Enter paycode: ";
    }
    std::cin >> paycode;

    switch (paycode) {
    case 1:
        if (::isatty(STDIN_FILENO)) {
            std::cout << "Enter weekly salary for manager: ";
        }
        double weeklySalaryM;
        std::cin >> weeklySalaryM;
        employeesPay = weeklySalaryM;
        break;
    case 2:
        int workedHours;
        if (::isatty(STDIN_FILENO)) {
            std::cout << "Enter worked hours for hourly workers: ";
        }
        std::cin >> workedHours; 
        double hourlySalary;
        if (::isatty(STDIN_FILENO)) {
            std::cout << "Enter hourly salary: ";
        }
        std::cin >> hourlySalary;
        employeesPay = workedHours * hourlySalary;
        if (workedHours > 40) {
            employeesPay += (workedHours - 40) * 0.5 * hourlySalary;
        }
        break;
    case 3:
        int weeklySalary;
        if (::isatty(STDIN_FILENO)) {
            std::cout << "Enter weekly salary for commission workers: ";
        }
        std::cin >> weeklySalary;
        employeesPay = 250.0 + 5.7 * weeklySalary;
        break;
    case 4:
        int itemCount;
        if (::isatty(STDIN_FILENO)) {
            std::cout << "Enter item count for piece workers: ";
        }
        std::cin >> itemCount;
        double itemPrice;
        if (::isatty(STDIN_FILENO)) {
            std::cout << "Enter price for item: ";
        }
        std::cin >> itemPrice;
        employeesPay = itemCount * itemPrice;
        break;
    default:
        assert(paycode > 0 && paycode < 5);
    }

    std::cout << "Employee's pay is " << employeesPay << std::endl;

    return 0;
}

