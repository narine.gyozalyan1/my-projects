#include <iostream>
#include <unistd.h>
#include <assert.h>

int
main()
{
    double total = 0;
    while (true) {
        if (::isatty(STDIN_FILENO)) {
            std::cout << "Enter product number (-1 to exit): ";
        }
        int productNumber = 0;
        std::cin >> productNumber;
        if (-1 == productNumber) {
            break;
        }
        if (productNumber < 0 || productNumber > 5) {
            std::cout << "Error 1: Invalid product number." << std::endl;
            return 0;
        }

        int quantitySold;
        if (::isatty(STDIN_FILENO)) {
            std::cout << "Enter quantity sold: ";
        }
        std::cin >> quantitySold;
        if (quantitySold < 0) {
            std::cout << "Error 2: Quantity can't be negative." << std::endl;
        }

        switch (productNumber) {
        case 1: total += quantitySold * 2.98; break;
        case 2: total += quantitySold * 4.50; break;
        case 3: total += quantitySold * 9.98; break;
        case 4: total += quantitySold * 4.49; break;
        case 5: total += quantitySold * 6.87; break;
        default: assert(0);                   break;
        }
    }

    std::cout << "Total is " << total << std::endl;

    return 0;
}

