#include <iostream>
#include <unistd.h>

double minimum(const double double1, const double double2, const double double3);

int
main()
{
    double double1, double2, double3;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input three double values: ";
    }
    std::cin >> double1 >> double2 >> double3;
    std::cout << "The minimum double value is: " <<  minimum( double1, double2, double3 ) << std::endl;

    return 0;
}

double
minimum(const double double1, const double double2, const double double3)
{
    double minimum = double1;
    if (double2 < minimum) {
        minimum = double2;
    }
    if (double3 < minimum) {
        minimum = double3;
    }
    return minimum;
}

