#include <iostream>
#include <unistd.h>
#include <iomanip>
#include <cmath>

void print(const double original, const double rounded);
double roundNumber(const double original);
double inputNumber();

int
main()
{
    int counter = 0;
    while (counter < 5) {
        const double original = inputNumber();
        const double rounded = roundNumber(original);
        print(original, rounded);
        ++counter;
    }

    return 0;
}

double
roundNumber(const double original)
{
    const double rounded = std::floor(original + .5);
    return rounded;
}

double
inputNumber()
{
    double original;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Enter number: ";
    }
    std::cin >> original;
    return original;
}

void
print(const double original, const double rounded)
{
    std::cout << std::fixed << std::setprecision(2) << "Original number is " << original <<"\n" 
                                                    << "Rounded number is " << rounded << "\n";
}

