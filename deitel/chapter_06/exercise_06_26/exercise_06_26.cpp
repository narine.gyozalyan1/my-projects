#include <iostream>
#include <unistd.h>
#include <cmath>
#include <cassert>

int timeInSeconds(int hours, const int minutes, int seconds);
int inputHours();
int inputMinutes();
int inputSeconds();

int
main()
{
        const int hours1 = inputHours();
        const int minutes1 = inputMinutes();
        const int seconds1 = inputSeconds();
        const int time1 = timeInSeconds(hours1, minutes1, seconds1);
        
        const int hours2 = inputHours();
        const int minutes2 = inputMinutes();
        const int seconds2 = inputSeconds();
        const int time2 = timeInSeconds(hours2, minutes2, seconds2);

        std::cout << "The amount of time in seconds between two times is " << ::abs(time2 - time1) << std::endl;

    return 0;
}

int
timeInSeconds(int hours, const int minutes, int seconds)
{
    assert(hours >= 0 && hours <= 23);
    assert(minutes >= 0 && minutes <= 59);
    assert(seconds >= 0 && seconds <= 59);
    seconds = hours * 3600 + minutes * 60 + seconds;
    return seconds;
}

int 
inputHours()
{
    int hours;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Enter hours: ";
    }
    std::cin >> hours;

    if (hours < 0 || hours > 23) {
        std::cerr << "Error 1: Invalid value for hours." << std::endl;
        ::exit(1);
    }
    assert(hours >= 0 && hours <= 23);
    return hours;
}

int 
inputMinutes()
{
    int minutes;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Enter minutes: ";
    }
    std::cin >> minutes;
    if (minutes < 0 || minutes > 59) {
        std::cerr << "Error 1: Invalid value for minutes." << std::endl;
        ::exit(1);
    }
    assert(minutes >= 0 && minutes <= 59);
    return minutes;
}

int 
inputSeconds()
{
    int seconds;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Enter seconds: ";
    }
    std::cin >> seconds;
    if (seconds < 0 || seconds > 59) {
        std::cerr << "Error 2: Invalid value for seconds." << std::endl;
        ::exit(2);
    }
    assert(seconds >= 0 && seconds <= 59);
    return seconds;
}

