#include <iostream>
#include <unistd.h>
#include <cmath>
#include <iomanip>

int inputNumber();
void printFunctions(const int number1, const int exponent, const int fmodY);

int
main()
{
/*    
    for (int i = 1; i < 4; ++i) {
        std::cout << setw(12) << "Value "<< i << ' ';
    }
*/    
    double number1 = inputNumber();

    if (::isatty (STDIN_FILENO)) {
        std::cout << "Enter exponent for function pow(): ";
    }
    int exponent1;
    std::cin >> exponent1;
    if (::isatty (STDIN_FILENO)) {
        std::cout << "Enter second value for function fmod(): ";
    }
    int fmodY1;
    std::cin >> fmodY1;
    printFunctions(number1, exponent1, fmodY1);
    
    double number2 = inputNumber();
    if (::isatty (STDIN_FILENO)) {
        std::cout << "Enter exponent for function pow(): ";
    }
    int exponent2;
    std::cin >> exponent2;   
    if (::isatty (STDIN_FILENO)) {
        std::cout << "Enter second value for function fmod(): ";
    }
    int fmodY2;
    std::cin >> fmodY2;
    printFunctions(number2, exponent2, fmodY2);

    return 0;
}

int
inputNumber()
{
    if (::isatty (STDIN_FILENO)) {
        std::cout << "Input number: ";
    }
    double number1;
    std::cin >> number1;
    return number1;
}

void
printFunctions(const int number1, const int exponent, const int fmodY)
{
    std::cout << "For " << number1 << "(exponent is " << exponent << ", y for fmod(x, y) is " << fmodY << ")" << std::endl;
    std::cout << "ceil()" << std::setw(20) << ::ceil(number1) << std::endl;
    std::cout << "exp()" << std::setw(20) << ::exp(number1) << std::endl;
    std::cout << "fabs()" << std::setw(20) << ::fabs(number1) << std::endl;
    std::cout << "floor()" << std::setw(20) << ::floor(number1) << std::endl;
    std::cout << "fmod()" << std::setw(20) << ::fmod(number1, fmodY) << std::endl;
    std::cout << "log()" << std::setw(20) << ::log(number1) << std::endl;
    std::cout << "log10()" << std::setw(20) << ::log10(number1) << std::endl;
    std::cout << "sin()" << std::setw(20) << ::sin(number1) << std::endl;
    std::cout << "tan()" << std::setw(20) << ::tan(number1) << std::endl;
    std::cout << "cos()" << std::setw(20) << ::cos(number1) << std::endl;
    std::cout << "pow()" << std::setw(20) << ::pow(number1, exponent) << std::endl;
    std::cout << std::endl;
}

