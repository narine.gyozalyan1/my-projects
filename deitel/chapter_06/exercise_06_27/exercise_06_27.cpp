#include <iostream>
#include <iomanip>

double celsius(const double fahrenheit);
double fahrenheit(const double celsius);

int
main()
{
    std::cout << "Fahrenheit equivalents of all Celsius temperatures from 0 to 100: " << std::endl;
    for (int i = 0; i <= 100; ++i) {
        const double celsius = static_cast<double>(i);
        std::cout << fahrenheit(celsius) << "\t";
    }
    std::cout << std::endl;
    std::cout << "Celsius equivalents of all Fahrenheit temperatures from 32 to 212: " << std::endl;
    for (int i = 32; i <= 212; ++i) {
        const double fahrenheit = static_cast<double>(i);
        std::cout << std::fixed << std::setprecision(1) << celsius(fahrenheit) << "\t";
    }
    std::cout << std::endl;

    return 0;
}

double
celsius(const double fahrenheit)
{
    return (fahrenheit - 32) * 5 / 9;
}

double
fahrenheit(const double celsius)
{
    return (celsius * 9 / 5 + 32);
}

