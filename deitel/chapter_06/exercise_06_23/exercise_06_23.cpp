#include <iostream>
#include <unistd.h>

void squareOfAsterisks(const int side, const char fillCharacter);

int
main()
{
    int side1;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Enter side of square: ";
    }
    std::cin >> side1;
    char character1;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Enter character: ";
    }
    std::cin >> character1;
    squareOfAsterisks(side1, character1);

    return 0;
}

void
squareOfAsterisks(const int side, const char fillCharacter)
{
    for (int i = 0; i < side; ++i) {
        for (int j = 0; j < side; ++j) {
            std::cout << fillCharacter;
        }
        std::cout << std::endl;
    }
}

