#include <iostream>
#include <iomanip>
#include <unistd.h>

double calculateCharges(const double hours);

int
main()
{
    std::cout << "Car" << std::setw(12) << "Hours" << std::setw(14) << "Charges" << std::endl;
    double totalHours = 0;
    double totalCharges = 0;
    for (int i = 1; i <= 3; ++i) {
        double hour;
        if (::isatty(STDIN_FILENO)) {
            std::cout << "Enter hours for 3 customers: ";
        }
        std::cin >> hour;
        totalHours += hour;
        const double charges = calculateCharges(hour);
        totalCharges += charges;
        std::cout << i << std::fixed << std::setprecision(2) << std::setw(14) << hour << std::setw(14) << charges << std::endl;
    }
    std::cout << "TOTAL" << std::setw(10) << totalHours << std::setw(14) << totalCharges << std::endl;

    return 0;
}

double
calculateCharges(const double hours)
{   
    if (hours <= 3) {
        return 2.00;
    }
    if (hours >= 19) {
        return 10.00;
    }
    return (2.00 + (hours - 3) * 0.50);
}

