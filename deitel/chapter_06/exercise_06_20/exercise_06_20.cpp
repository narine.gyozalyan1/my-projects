#include <iostream>
#include <unistd.h>
#include <cassert>

bool multiple(const int number1, const int number2);

int
main()
{
    for (int i = 0; i < 5; ++i) {
        int number1;
        if (::isatty(STDIN_FILENO)) {
            std::cout << "Enter first integer: ";
        }
        std::cin >> number1;
        if (0 == number1) {
            std::cout << "Error1: First integer can't be 0." << std::endl;
            return 1;
        }
        int number2;
        if (::isatty(STDIN_FILENO)) {
            std::cout << "Enter second integer: ";
        }
        std::cin >> number2;
        if (multiple(number1, number2)) {
            std::cout << number2 << " is a multiple of " << number1 << std::endl;
        } else {
            std::cout << number2 << " is not a multiple of " << number1 << std::endl;
        }
    }

    return 0;
}

bool 
multiple(const int number1, const int number2)
{
    assert(number1 != 0);
    return (0 == (number2 % number1));
}

