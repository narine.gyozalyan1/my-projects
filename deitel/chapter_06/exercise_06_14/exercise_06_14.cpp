#include <iostream>
#include <cmath>
#include <unistd.h>

double roundToInteger(const double originalNumber);
double roundToTenths(const double originalNumber);
double roundToHundredths(const double originalNumber);
double roundToThousandths(const double originalNumber);
void print(const double originalNumber);

int
main()
{
    int count = 0;
    while (count < 5) {
        double original;
        if (::isatty(STDIN_FILENO)) {
            std::cout << "Enter a floating point number (-1 to exit): ";
        }
        std::cin >> original;
        print(original);
        std::cout << std::endl;
        ++count;
    }

    return 0;
}

double
roundToInteger(const double originalNumber)
{
    return ::floor(originalNumber);
}

double
roundToTenths(const double originalNumber)
{
    return ::floor(originalNumber * 10 + .5) / 10;
}

double
roundToHundredths(const double originalNumber)
{
    return ::floor(originalNumber * 100 + .5) / 100;
}

double
roundToThousandths(const double originalNumber)
{
    return ::floor(originalNumber * 1000 + .5) / 1000;
}

void
print(const double originalNumber)
{
    std::cout << "Original number is " << originalNumber << std::endl
              << "Round to integer is " << roundToInteger(originalNumber) << std::endl
              << "Round to tenths is " << roundToTenths(originalNumber) << std::endl
              << "Round to hundredths is " << roundToHundredths(originalNumber) << std::endl
              << "Round to thousandths is " << roundToThousandths(originalNumber) << std::endl;
}
