#include <iostream>
#include <cstdlib>
#include <ctime>
#include <cstdio>
#include <unistd.h>

int getMultiplicants();
void printRightAnswer();
void printWrongAnswer();
int inputAnswer(const int number1, const int number2);
double studentPerformance(const int count, const int rightCount);

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::srand(std::time(0));
    }
    int counter = 0;
    const int ANSWER_COUNT = 10;
    int rightCount;
    while (counter < ANSWER_COUNT) {
        const int number1 = getMultiplicants();
        const int number2 = getMultiplicants();
        int answer = inputAnswer(number1, number2);
        if (answer == number1 * number2) {
            ++rightCount;
            printRightAnswer();
        } else {
            printWrongAnswer();
        }
        ++counter;
    }
    const double perform = studentPerformance(ANSWER_COUNT, rightCount);
    if (perform < 75.0) {
        std::cout << "Please ask your instructor for extra help" << std::endl;
    }

    return 0;
}

int
inputAnswer(const int number1, const int number2)
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "How much is " << number1 << " times " << number2 << "?" << std::endl;    
    }
    int answer = 0;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Answer: ";
    }
    std::cin >> answer;
    return answer;
}

double
studentPerformance(const int count, const int rightCount)
{
    return rightCount / count * 100;
}

int
getMultiplicants()
{
    return (1 + std::rand() % 9);
}

void
printRightAnswer()
{
    const int random = std::rand() % 4;
    switch (random) {
    case 0: std::cout << "Very good!";             break;
    case 1: std::cout << "Excellent!";             break;
    case 2: std::cout << "Nice work!";             break;
    case 3: std::cout << "Keep up the good work!"; break;
    }
    std::cout << std::endl;
}

void
printWrongAnswer()
{
    const int random = std::rand() % 4;
    switch (random) {
    case 0: std::cout << "No. Please try again."; break;
    case 1: std::cout << "Wrong. Try once more."; break;
    case 2: std::cout << "Don't give up!";        break;
    case 3: std::cout << "No. Keep trying.";      break;
    }
    std::cout << std::endl;
}

