#include <iostream>
#include <unistd.h>

int reverse(int number1);

int
main()
{
    int number1;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Enter number: ";
    }
    std::cin >> number1;
    std::cout << "Reverse of " << number1 << " is " << reverse(number1) << std::endl;

    return 0;
}

int
reverse(int number1)
{
    int reversed = 0;
    while(0 != number1) {
        reversed = reversed * 10 + number1 % 10;
        number1 /= 10;
    }
    return reversed;
}

