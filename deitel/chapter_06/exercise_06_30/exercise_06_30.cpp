#include <iostream>
#include <unistd.h>
#include <cmath>
#include <iomanip>

bool isPrimeSquareRoot(const int number);
bool isPrime(const int number);
void printPrimeRoot();
void printPrimeHalf();

int
main()
{
    printPrimeRoot();
    printPrimeHalf();

    return 0;
}

/// This function is faster than isPrime()
bool 
isPrimeSquareRoot(const int number)
{    
    if (number <= 0) {
        std::cerr << "Error 1: Integer should be positive." << std::endl;
        ::exit(1);
    }
    if (1 == number) {
        return false;
    }
    for (int i = 2; i <= ::sqrt(number); ++i) {
        if (0 == number % i) {
            return false;
        }
    }
    return true;
}

bool 
isPrime(const int number)
{    
    if (number <= 0) {
        std::cerr << "Error 1: Integer should be positive." << std::endl;
        ::exit(1);
    }
    if (1 == number) {
        return false;
    }
    for (int i = 2; i <= number / 2; ++i) {
        if (0 == number % i) {
            return false;
        }
    }
    return true;
}

void
printPrimeRoot()
{
/// We can check only odd numbers
    std::cout << "Prime numbers from 2 to 10000 (Square Root)" << std::endl;
    for (int i = 1; i <= 10000; i += 2) {
        if (1 == i) {
            std::cout << "2" << std::setw(8);
        }
        if (isPrimeSquareRoot(i)) {
            std::cout << i << std::setw(8);
        }
    }
    std::cout << std::endl;
}

void
printPrimeHalf()
{
    std::cout << "Prime numbers from 2 to 10000 (Half)" << std::endl;
    for (int i = 1; i <= 10000; i += 2) {
        if (1 == i) {
            std::cout << "2" << std::setw(8);
        }
        if (isPrimeSquareRoot(i)) {
            std::cout << i << std::setw(8);
        }
    }
    std::cout << std::endl;
}

