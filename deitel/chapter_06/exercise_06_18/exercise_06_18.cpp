#include <iostream>
#include <unistd.h>

int integerPower(int base, int exponent);

int
main()
{
    int base;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Enter the base: ";
    }
    std::cin >> base;
    int exponent;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Enter the exponent: ";
    }
    std::cin >> exponent;
    if (exponent <= 0) {
        std::cout << "Error 1: Exponent should be non zero, pozitive number." << std::endl;
        return 0;
    }
    std::cout << base << " to the power of " << exponent << " is " << integerPower(base, exponent) << std::endl;
    
    return 0;
}

int
integerPower(int base, int exponent)
{
    int result = 1;
    while ((exponent) > 1) {
        if (1 == exponent % 2) {
            result *= base;
            --exponent;
        }
        base *= base;
        exponent /= 2;
    }
    return base * result;
}

