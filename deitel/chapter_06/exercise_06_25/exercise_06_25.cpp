#include <iostream>
#include <unistd.h>
#include <cassert>

int integerPart(const int number1, const int divisor);
int integerRemainder(const int number1, const int divisor);
void printDigits(int number1);
int inputInteger();

int
main()
{
    int number1 = inputInteger();
    printDigits(number1);
    std::cout << std::endl;

    return 0;
}

int
integerPart(const int number1, const int divisor)
{
    assert(divisor != 0);
    return number1 / divisor;
}

int
integerRemainder(const int number1, const int divisor)
{
    assert(divisor != 0);
    return number1 % divisor;
}

int inputInteger()
{
    int number1;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Enter number (between 1 and 32767): ";
    }
    std::cin >> number1;
    if (number1 < 1 || number1 > 32767) {
        std::cout << "Error 1: Invalid value." << std::endl;
        return 1;
    }

    return number1;
}

void
printDigits(int number1)
{
    int divisor = 10000;
    while (0 != number1) {
        if (0 != integerPart(number1, divisor)) {
            std::cout << integerPart(number1, divisor) << "  ";
            number1 = integerRemainder(number1, divisor);
        }
        divisor /= 10;
    }
}

