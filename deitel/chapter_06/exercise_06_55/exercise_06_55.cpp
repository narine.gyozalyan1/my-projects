#include <iostream>
#include <unistd.h>
#include <iomanip>
#include <cassert>

inline double circleArea(const int radius);

int
main()
{
    int radius;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Enter the radius: ";
    }
    std::cin >> radius;
    if (radius < 0) {
        std::cout << "Error 1: Radius can not be negative." << std::endl;
        return 1;
    }
    std::cout << "The area of circle is " << std::fixed << std::setprecision(3) << circleArea(radius) << std::endl;

    return 0;
}

inline
double
circleArea(const int radius)
{
    assert(radius >= 0);
    return (3.14 * radius *radius);
}

