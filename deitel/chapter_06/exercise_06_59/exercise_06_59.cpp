#include <iostream>

template <typename T>
T
max(const T value1, const T value2)
{
    return (value1 > value2 ? value1 : value2);
}

int
main()
{
    int int1 = 5;
    int int2 = 2;
    std::cout << "Maximum of " << int1 << " and " << int2 << " is " << max(int1, int2) << std::endl;
    char char1 = 'A';
    char char2 = 'N';
    std::cout << "Maximum of " << char1 << " and " << char2 << " is " << max(char1, char2) << std::endl;   
    float float1 = 4.5;
    float float2 = 7.8;
    std::cout << "Maximum of " << float1 << " and " << float2 << " is " << max(float1, float2) << std::endl;
    
    return 0;
}

