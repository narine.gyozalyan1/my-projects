#include <iostream>

bool perfect(const int number1);
void printFactors(const int number1);
void printPerfect();

int
main()
{
    printPerfect();
    return 0;
}

bool
perfect(const int number1)
{
    int sum = 1;
    for (int i = 2; i <= number1 / 2; ++i) {
        if (0 == number1 % i) {
            sum += i;
        }
    }
    return (number1 == sum);
}

void
printPerfect()
{

    for (int i = 2; i < 1001; ++i) {
        if (perfect(i)) {
            std::cout << i << " = ";
            printFactors(i);
            std::cout << std::endl;
        }
    }
}

void
printFactors(const int number1)
{
    std::cout << "1";
    for (int i = 2; i <= number1 / 2; ++i) {
        if (0 == number1 % i) {
            std::cout << " + " << i;
        }
    }

}

