#include <iostream>
#include <cstdlib>
#include <ctime>
#include <unistd.h>

bool flip();

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::srand(std::time(0));
    }
    int headCount = 0;
    int tailCount = 0;
    for (int i = 0; i < 100; ++i) {
        const bool flipCoin = flip();
        if (flipCoin) {
            std::cout << "Tail" << std::endl;
            ++tailCount;
        } else {
            std::cout << "Head" << std::endl;
            ++headCount;
        }
    }

    std::cout << "Head count is " << headCount << std::endl;
    std::cout << "Teil count is " << tailCount << std:: endl;

    return 0;
}

bool
flip()
{
    return (std::rand() % 2);
}

