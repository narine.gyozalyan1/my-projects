#include <iostream>
#include <cmath>
#include <unistd.h>
#include <iomanip>
#include <string>

double distance(const double x1, const double y1, const double x2, const double y2);
double getCoordinate(const std::string& coordinate);

int
main()
{
    const double x1 = getCoordinate("x1");
    const double y1 = getCoordinate("y1"); 
    const double x2 = getCoordinate("x2");
    const double y2 = getCoordinate("y2");
    
    std::cout << "The distance is " << std::fixed << std::setprecision(3) << distance(x1, y1, x2, y2) << std::endl;

    return 0;
}



double
distance(const double x1, const double y1, const double x2, const double y2)
{
    const double deltaX = x1 - x2;
    const double deltaY = y1 - y2;
    return (::sqrt(deltaX * deltaX + deltaY * deltaY));
}

double
getCoordinate(const std::string& coordinate1)
{   

    if (::isatty(STDIN_FILENO)) {
        std::cout << "Get coordinate " << coordinate1;
    }
    double coordinate;
    std::cin >> coordinate;
    return coordinate;
}

