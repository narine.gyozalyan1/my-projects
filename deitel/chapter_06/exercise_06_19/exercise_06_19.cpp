#include <iostream>
#include <cmath>
#include <unistd.h>
#include <iomanip>

double hypotenuse(const double side1, const double side2);
void validate(const double side);

int
main()
{
    const int n = 3;

    for (int i = 1; i <= n; ++i) {
        if (::isatty(STDIN_FILENO)) {
            std::cout << "Enter side 1 and side 2: ";
        }
        double side1, side2;
        std::cin >> side1 >> side2;
        validate(side1);
        validate(side2);
        std::cout << "For triangle " << i << std::endl
                  << "Side 1 is " << side1 << std::endl
                  << "Side 2 is " << side2 << std::endl
                  << "Hypotenuse is " << hypotenuse(side1, side2) << std::endl;
        std::cout << std::endl;
    }
    
    return 0;
}

double
hypotenuse(const double side1, const double side2)
{
    const double hypotenuse = ::sqrt(side1 * side1 + side2 * side2);
    return hypotenuse;
}

void
validate(const double side)
{
    if (side <= 0) {
        std::cout << "Error1: Sides can't be negative." << std::endl;
        ::exit(1);
    }
}

