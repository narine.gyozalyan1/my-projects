#include <iostream>
#include <unistd.h>

void squareOfAsterisks(const int side);

int
main()
{
    int side;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Enter side of square: ";
    }
    std::cin >> side;
    squareOfAsterisks(side);

    return 0;
}

void
squareOfAsterisks(const int side)
{
    for (int i = 0; i < side; ++i) {
        for (int j = 0; j < side; ++j) {
            std::cout << "*";
        }
        std::cout << std::endl;
    }
}

