#include <iostream>
#include <unistd.h>

int mystery(const int a, const int b);

int
main()
{
    int x, y;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Enter two integers: ";
    }
    std::cin >> x >> y;
    std::cout << "The result is " << mystery(x, y) << std::endl;
    
    return 0;
}

int
mystery(const int a, const int b)
{
    if (0 == b || 0 == a) {
        return 0;
    }
    if (1 == b) {
        return a;
    }
    if (b < 0) {
        return mystery(-a, -b);
    }
    return a + mystery(a, b - 1);
}

