#include <iostream>
#include <cstdlib>
#include <ctime>
#include <cstdio>
#include <unistd.h>

void checkMultiplication(const int number1, const int number2);
int getMultiplicants();
int getAnswer();

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::srand(std::time(0));
    }
    while (true) {
        const int number1 = getMultiplicants();
        const int number2 = getMultiplicants();
        checkMultiplication(number1, number2);
        if (::isatty(STDIN_FILENO)) {
            std::cout << "Do you want to contine (Type 0 for yes, 1 for no.)?" << std::endl;
        }
        const int answer1 = getAnswer();
        if (1 == answer1) {
            break;
        }
    }

    return 0;
}

int
getAnswer()
{
    int answer;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Answer: ";
    }
    std::cin >> answer;
    return answer;
}


void
checkMultiplication(const int number1, const int number2)
{
    while (true) {
        if (::isatty(STDIN_FILENO)) {
            std::cout << "How much is " << number1 << " times " << number2 << "?" << std::endl;    
        }
        const int answer = getAnswer();
        if (answer != number1 * number2) {
            std::cout << "No. Please try again." << std::endl;
        } else {
            std::cout << "Very good!" << std::endl;
            break;
        }
    }
}

int
getMultiplicants()
{
    return (1 + std::rand() % 9);
}

