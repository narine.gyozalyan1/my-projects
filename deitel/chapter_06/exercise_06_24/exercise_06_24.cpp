#include <iostream>
#include <unistd.h>
#include <cmath>

void displaySquare(const int side, const char fillCharacter);
void displayTriangle(const int side, const char fillCharacter);
void displayRectangle(const int side1, const int side2, const char fillCharacter);
void displayDiamond(const int side, const char fillCharacter);

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Enter side: ";
    }
    int side1;
    std::cin >> side1;

    if (::isatty(STDIN_FILENO)) {
        std::cout << "Enter second side for rectangle: ";
    }
    int side2;
    std::cin >> side2;

    if (::isatty(STDIN_FILENO)) {
        std::cout << "Enter character: ";
    }
    char character1;
    std::cin >> character1;

    displaySquare(side1, character1);
    displayTriangle(side1, character1);
    displayRectangle(side1, side2, character1);
    displayDiamond(side1, character1);

    return 0;
}

void 
displaySquare(const int side, const char fillCharacter)
{
    for (int i = 1; i <= side; ++i) {
        for (int j = 1; j <= side; ++j) {
            std::cout << fillCharacter;
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
}

void 
displayTriangle(const int side, const char fillCharacter)
{
    for (int i = 1; i <= side; ++i) {
        for (int j = 1; j <= i; ++j) {
            std::cout << fillCharacter;
        }
        std::cout << std::endl; 
    }
    std::cout << std::endl;
}

void
displayRectangle(const int side1, const int side2, const char fillCharacter)
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Enter second side: " << std::endl;
    }
    for (int i = 1; i <= side2; ++i) {
        for (int j = 1; j <= side1; ++j) {
            std::cout << fillCharacter;
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
}

void
displayDiamond(const int side, const char fillCharacter)
{
    int size1 = side / 2;
    for (int i = -size1; i <= size1; ++i) {
        for (int j = -size1; j <= size1; ++j) {
            if (std::abs(i) + std::abs(j) <= size1) {
                std::cout << fillCharacter;
            } else {
                std::cout << " ";
            }
        }
        std::cout << std::endl;
    }

}

