#include <iostream>
#include <cstdlib>
#include <ctime>
#include <cstdio>
#include <unistd.h>

void checkMultiplication(const int number1, const int number2);
int getMultiplicants();
void printRightAnswer();
void printWrongAnswer();
void calculate();
int getAnswer();

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::srand(std::time(0));
    }
    calculate();

    return 0;
}

void
calculate()
{
    while (true) {
        const int number1 = getMultiplicants();
        const int number2 = getMultiplicants();
        checkMultiplication(number1, number2);
        if (::isatty(STDIN_FILENO)) {
            std::cout << "Do you want to contine (Type 0 for yes, 1 for no.): " << std::endl;
        }
        const int answer1 = getAnswer();
        if (1 == answer1) {
            break;
        }
    }
}

int
getAnswer()
{
    int answer;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Answer: ";
    }
    std::cin >> answer;
    return answer;
}

void
checkMultiplication(const int number1, const int number2)
{
    while (true) { 
        if (::isatty(STDIN_FILENO)) {
            std::cout << "How much is " << number1 << " times " << number2 << "?" << std::endl;    
        }
        const int answer = getAnswer();
        if (answer != number1 * number2) {
            printWrongAnswer();
        } else {
            printRightAnswer();
            break;
        }
    }
}

int
getMultiplicants()
{
    return (1 + std::rand() % 9);
}

void
printRightAnswer()
{
    const int random = std::rand() % 4;
    switch (random) {
    case 0: std::cout << "Very good!" << std::endl;             break;
    case 1: std::cout << "Excellent!" << std::endl;             break;
    case 2: std::cout << "Nice work!" << std::endl;             break;
    case 3: std::cout << "Keep up the good work!" << std::endl; break;
    }
}

void
printWrongAnswer()
{
    const int random = std::rand() % 4;
    switch (random) {
    case 0: std::cout << "No. Please try again." << std::endl; break;
    case 1: std::cout << "Wrong. Try once more." << std::endl; break;
    case 2: std::cout << "Don't give up!" << std::endl;        break;
    case 3: std::cout << "No. Keep trying." << std::endl;      break;
    }
}

