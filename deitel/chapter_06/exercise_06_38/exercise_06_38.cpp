#include <iostream>
#include <cstdlib>
#include <ctime>
#include <cstdio>
#include <unistd.h>

int chooseGuessedNumber();
int inputPlayersGuess();
void desplayMessage();
void checkGuess(const int guessed, const int randomNumber);

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::srand(std::time(0));
    }
    const int randomNumber = chooseGuessedNumber();
    desplayMessage();
    while (true) {
        const int guess = inputPlayersGuess();
        checkGuess(guess, randomNumber);
    }

    return 0;
}

int
chooseGuessedNumber()
{
    return (1 + std::rand() % 1000);
}

int
inputPlayersGuess()
{
    int playerGuess;
    std::cin >> playerGuess;
    return playerGuess;
}


void
desplayMessage()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "I have a number between 1 and 1000." << std::endl
                  << "Can you guess my number?" << std::endl
                  << "Please type your first guess." << std::endl;
    }
}

void
checkGuess(const int guessed, const int randomNumber)
{
    if (guessed == randomNumber) {
        std::cout << "Excellent! You guessed the number!" << std::endl
                  << "Would you like to play again (y or n)?" << std::endl;
        ::exit(0);
    }
    if (guessed > randomNumber) {
        std::cout << "Too high. Try again." << std::endl;
    }
    if (guessed < randomNumber) {
        std::cout << "Too low. Try again." << std::endl;
    }
}

