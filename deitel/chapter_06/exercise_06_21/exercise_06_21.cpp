#include <iostream>
#include <unistd.h>

bool even(const int number1);

int
main()
{
    for (int i = 0; i < 5; ++i) {
        int number1;
        if (::isatty(STDIN_FILENO)) {
            std::cout << "Enter an integer: ";
        }
        std::cin >> number1;
        if (even(number1)) {
            std::cout << number1 << " is even." << std::endl;
        } else {
            std::cout << number1 << " is odd." << std::endl;
        }
    }

    return 0;
}

bool 
even(const int number1)
{
    return (0 == (number1 % 2));
}

