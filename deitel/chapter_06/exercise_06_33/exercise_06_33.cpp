#include <iostream>
#include <cassert>
#include <unistd.h>
#include <cstdlib>

int qualityPoints(const int average);

int
main()
{
    int average;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Enter a student's average: ";
    }
    std::cin >> average;
    if (average < 0 || average > 100) {
        std::cout << "Error 1: Average should be in range 0 - 100." << std::endl;
        return 1;
    }
    std::cout << "Student's average is " << qualityPoints(average) << std::endl;

    return 0;
}

int
qualityPoints(const int average)
{
    assert(average >= 0 && average <= 100);
    if (average >= 90) {
        return 4;
    }
    if (average >= 80) {
        return 3;
    }
    if (average >= 70) {
        return 2;
    }
    if (average >= 60) {
        return 1;
    }
    return 0;
}

