#include <iostream>

template <typename T>
T
min(const T value1, const T value2)
{
    return (value1 < value2 ? value1 : value2);
}

int
main()
{
    int int1 = 5;
    int int2 = 2;
    std::cout << "Minimum of " << int1 << " and " << int2 << " is " << min(int1, int2) << std::endl;
    char char1 = 'A';
    char char2 = 'N';
    std::cout << "Minimum of " << char1 << " and " << char2 << " is " << min(char1, char2) << std::endl;   
    float float1 = 4.5;
    float float2 = 7.8;
    std::cout << "Minimum of " << float1 << " and " << float2 << " is " << min(float1, float2) << std::endl;
    
    return 0;
}

