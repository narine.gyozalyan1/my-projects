#include <iostream>
#include <unistd.h>

template <typename T>
T
tripleCallByValue(T count)
{
    return count *= 3;
}

template <typename T>
T
tripleCallByReference(T& count)
{
    return count *= 3;
}

int
main()
{
    int count;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Enter count: ";
    }
    std::cin >> count;
    
    std::cout << "Initial count is: " << count << std::endl;
    std::cout << "Count returned by tripleCallByValue: " << tripleCallByValue(count) << std::endl;
    std::cout << "After tripleCallByValue count is: " << count << std::endl;
    std::cout << "Count returned by tripleCallByReference: " << tripleCallByReference(count) << std::endl;
    std::cout << "After tripleCallByReference count is " << count << std::endl;

    return 0;
}

