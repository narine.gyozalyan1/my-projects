class Shape 
{
public:
    virtual void draw() = 0; 
}

class TwoDimensionalShape : public Shape 
{
public:
    virtual void draw() = 0; 
}

class ThreeDimensionalShape : public Shape 
{
public:
    virtual void draw() = 0; 
}

class Rectangle : public TwoDimensionalShape
{
public:
    virtual void draw() = 0; 
}

class Circle : public TwoDimensionalShape
{
public:
    virtual void draw() = 0; 
}

class Triangle : public: TwoDimensionalShape 
{
public:
    virtual void draw() = 0; 
}

class Prism : public ThreeDimensionalShape
{
public:
    virtual void draw() = 0; 
}

class Sphere : public ThreeDimensionalShape
{
public:
    virtual void draw() = 0; 
}

class EquilateralTriangle : public Triangle 
{
public:
    virtual void draw() {  }; 
}

class Square : public Rectangle
{
public:
    virtual void draw() {  }; 
}

class Cube : public Prism
{
public:
    virtual void draw() {  }; 
}

