#include <iostream>
#include <vector>
#include <unistd.h>

int recursiveMinimum(std::vector<int>& integerVector, const int start, const int end);

int
main()
{
    const int SIZE = 5;
    std::vector<int> integerVector(SIZE);
    for (int i = 0; i < SIZE; ++i) {
        if (isatty(STDIN_FILENO)) {
            std::cout << "Input " << SIZE << " numbers: ";
        }
        std::cin >> integerVector[i];
    }
    std::cout << "Smallest is " << recursiveMinimum(integerVector, 0, SIZE - 1) << std::endl;
    
    return 0;
}

int
recursiveMinimum(std::vector<int>& integerVector, const int start, const int end)
{
    if (start == end) {
        return integerVector[start];
    }
    const int minimum = recursiveMinimum(integerVector, start + 1, end);
    if (minimum < integerVector[start]) {
        return minimum;
    }    
    return integerVector[start];
}

