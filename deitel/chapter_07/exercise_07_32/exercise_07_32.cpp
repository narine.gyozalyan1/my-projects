#include <iostream>
#include <unistd.h>
#include <cstring>

bool
testPalindrome(char stringArray[], int end, int start);

int
main()
{
    const int SIZE = 21;
    char string1[SIZE];
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Enter the string: ";
    }
    std::cin.getline(string1, SIZE);

    int start = 0;
    int end = std::strlen(string1) - 1;
    const bool check = testPalindrome(string1, end, start);
    if (check) {
        std::cout << "Is palindrome." << std::endl;
        return 0;
    }
    std::cout << "Is not palindrome." << std::endl;

    return 0;
}

bool
testPalindrome(char stringArray[], int end, int start)
{
    if (start >= end) {
        return true;
    }
    while (' ' == stringArray[start] || ',' == stringArray[start]) {
        ++start;
    }
    while (' ' == stringArray[end] || ',' == stringArray[end]) {
        --end;
    }

    if (stringArray[start] != stringArray[end]) {
        return false;
    }
    return testPalindrome(stringArray, end - 1, start + 1);
}

