#include <unistd.h>
#include <iostream>

void selectionSort(int array[], const int start, const int size);
void inputNumber(int array[], const int size);
void printArray(const int array[], const int size);

int
main()
{
    const int SIZE = 10;
    const int START = 0;
    int array[SIZE] = {};
    inputNumber(array, SIZE);
    selectionSort(array, START, SIZE);
    printArray(array, SIZE);
    return 0;
    
}

void
selectionSort(int array[], const int start, const int size)
{
    if (start == size - 1) {
        return;
    }
    int minimum = start;
    for (int i = start; i < size; ++i) {
        if (array[i] < array[minimum]) {
            minimum = i;
        }
    }
    const int temp = array[minimum];
    array[minimum] = array[start];
    array[start] = temp;
    selectionSort(array, start + 1, size);
}

void 
printArray(const int array[], const int size)
{
    for (int i = 0; i < size; ++i) {
        std::cout << array[i] << " ";
    }
    std::cout << std::endl;
}

void 
inputNumber(int array[], const int size)
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Enter numbers: ";
    }
    for (int i = 0; i < size; ++i) {
        std::cin >> array[i];
    }
 
}

