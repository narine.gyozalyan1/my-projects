#include <iostream>

int
main()
{
    const int salesPerson = 20;
    int salaryArray[salesPerson] = { 2000, 220, 255, 420, 6000, 200, 390, 470, 650, 810, 940, 1000, 1020, 260, 250, 410, 870, 620, 330, 990 };
    const int frequencySize = 9;
    int frequency[frequencySize] = { 0 };
    
    for (int j = 0; j < salesPerson; ++j) {
        int salary = salaryArray[j];
        int lim1 = (salary / 100) - 2;
        if (lim1 > 8) {
            ++frequency[8];
        }
        ++frequency[lim1];
    }

    for (int i = 0; i < frequencySize; ++i) {
        int limit1 = (i + 2) * 100;
        if (8 == i) {
            std::cout << "$" << limit1 << " and over: " << frequency[i];
            break;
        }

        int limit2 = limit1 + 99;
        std::cout << "$" << limit1 << " - " << "$" << limit2 << ": " << frequency[i] << std::endl;
    }

    std::cout << std::endl;
    return 0;
}

