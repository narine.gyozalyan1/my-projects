#include <iostream>
#include <unistd.h>
#include <iomanip>

void stringReverse(const char stringArray[], const int start);

int
main()
{
    const int SIZE = 21;
    char string1[SIZE];
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Enter the string: ";
    }
    
    std::cin >> std::setw(SIZE) >> string1;
    int start = 0;
    while ('\0' != string1[start]) {
        ++start;
    }
    --start;
    stringReverse(string1, start);

    return 0;
}

void
stringReverse(const char stringArray[], const int start)
{
    if ('\0' == stringArray[start]) {
        std::cout << std::endl;
        return;
    } 
    std::cout << stringArray[start];
    stringReverse(stringArray, start - 1);
}

