#include <iostream>

int recursiveMinimum(int array1[], const int start, const int end);

int
main()
{
    const int SIZE = 4;
    int integersArray[SIZE];
    for (int i = 0; i < SIZE; ++i) {
        std::cin >> integersArray[i];
    }
    std::cout << "Smallest is " << recursiveMinimum(integersArray, 0, SIZE - 1) << std::endl;
    
    return 0;
}

int
recursiveMinimum(int array1[], const int start, const int end)
{
    if (start == end) {
        return array1[start];
    }
    const int minimum = recursiveMinimum(array1, start + 1, end);
    if (minimum < array1[start]) {
        return minimum;
    }    
    return array1[start];
}

