#include <iostream>
#include <unistd.h>

void printArray(int array1[], int start, int end);

int
main()
{
    const int size = 5;
    int array1[size] = { 2, 5, 75, 14, 23 };
    
    int start;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Enter starting subscript: ";
    }
    std::cin >> start;
    if (start < 0 || start >= size) {
        std::cout << "Error 1: Invalid value for starting." << std::endl;
        return 1;
    }
    int end;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Enter ending subscript: ";
    }
    std::cin >> end;
    if (end < 0 || end >= size) {
        std::cout << "Error 2: Invalid value for ending." << std::endl;
        return 2;
    }
    printArray(array1, start, end);
    
    return 0;
}

void
printArray(int array1[], int start, const int end)
{
    std::cout << array1[start] << " ";
    if (start == end) {
        std::cout << std::endl;
        return;
    }
 
    ++start;
    printArray(array1, start, end);
}

