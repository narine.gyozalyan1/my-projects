#include <iostream>
#include <unistd.h>

void inputArray(int array1[], const int size);
void print(const int array1[], const int size);
void bubbleSort(int array1[], const int size);

int
main()
{
    const int SIZE = 10;
    int array[SIZE] = {};
    inputArray(array, SIZE);
    bubbleSort(array, SIZE);
    std::cout << "Sorted array is the following." << std::endl;
    print(array, SIZE);

    return 0;
}

void
inputArray(int array1[], const int size)
{
    for (int i = 0; i < size; ++i) {
        if (isatty(STDIN_FILENO)) {
            std::cout << "Enter number: ";
        }
        std::cin >> array1[i];
    }
}

void
print(const int array1[], const int size)
{
    for (int i = 0; i < size; ++i) {
        std::cout << array1[i] << " ";
    }
    std::cout << std::endl;
}

void
bubbleSort(int array1[], const int size)
{
    for (int j = 0; j < size; ++j) {
        int count = 0;
        for (int i = 0; i < size - 1 - j; ++i) {
            if (array1[i] > array1[i + 1]) {
                int temp = array1[i + 1];
                array1[i + 1] = array1[i];
                array1[i] = temp;
                ++count;
            }
        }
        if (0 == count) {
            return;
        }
    }
}

