                                                                  Shape

                        |                                                                                         |
               TwoDimensionalShape                                                                      ThreeDimensionalShape

   |       |                  |          |        |        |        |                         |                                 |       |      |     |
circle Triangle         Parallelogram Ellipse Trapesoid Pentagon Hexagon                    Prism                            Pyramid Cylinder Cone Sphere
           |             |        |                                             |        |              |                       
     RigthTriangle  Rectangle  Rhombus                                        Cube HexagonalPrism TriangualarPrism       
                         |
                      Square
