#ifndef __OVERNIGHT_PACKAGE_HPP__
#define __OVERNIGHT_PACKAGE_HPP__

#include "Package.hpp"

class OvernightPackage : public Package
{
public:
    OvernightPackage(const Person& sender, const Person& recipient, const double weight,
                     const double costPerOunce, const double feePerOunce);
    double calculateCost() const;
private:
    double feePerOunce_;
};

#endif /// __OVERNIGHT_PACKAGE_HPP__

