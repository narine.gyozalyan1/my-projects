#include "Person.hpp"

std::istream&
operator>>(std::istream& input, Person& person)
{
    input >> person.name_;
    input >> person.address_;
    input >> person.city_;
    input >> person.state_;
    input >> person.zipCode_;
    return input;
}

std::ostream&
operator<<(std::ostream& output, const Person& person)
{
    output << "Name: " << person.name_ << std::endl;
    output << "Address: " << person.address_ << std::endl;
    output << "City: " << person.city_ << std::endl;
    output << "State: " << person.state_ << std::endl;
    output << "ZIP code: " << person.zipCode_ << std::endl;
    return output;
}

Person::Person()
    : name_(""), address_(""), city_(""), state_(""), zipCode_("")
{}

Person::Person(const Person& person)
    : name_(person.name_), address_(person.address_), city_(person.city_), state_(person.state_), zipCode_(person.zipCode_)
{}

Person::Person(const std::string& name, const std::string& address, const std::string& city,
               const std::string& state, const std::string& zipCode)
    : name_(name), address_(address), city_(city), state_(state), zipCode_(zipCode)
{}

