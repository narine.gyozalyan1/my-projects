#ifndef __PERSON_HPP__
#define __PERSON_HPP__

#include <iostream>

class Person
{
    friend std::istream& operator>>(std::istream& input, Person& person);
    friend std::ostream& operator<<(std::ostream& output, const Person& person);

public:
    Person();
    Person(const Person& person);
    Person(const std::string& name, const std::string& address, const std::string& city, const std::string& state, const std::string& zipCode);
private:
    std::string name_;
    std::string address_;
    std::string city_;
    std::string state_;
    std::string zipCode_;
};

#endif /// __PERSON_HPP__
