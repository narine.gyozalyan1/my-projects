#include "Package.hpp"
#include "Person.hpp"
#include "TwoDayPackage.hpp"
#include "OvernightPackage.hpp"

#include <iostream>
#include <unistd.h>

int
main()
{ 
    Person sender1;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input name, address, city, state and ZIP code for the sender: ";
    }
    std::cin >> sender1;

    Person recipient1;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input name, address, city, state and ZIP code for the recipient: ";
    }
    std::cin >> recipient1;
    
    std::cout << sender1 << std::endl;
    std::cout << recipient1 << std::endl;

    double weight1;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input weight: ";
    }
    std::cin >> weight1;
    if (weight1 < 0) {
        std::cout << "Error 1: Invalid value for weight." << std::endl;
        return 1;
    }

    double cost1;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input cost per ounce: ";
    }
    std::cin >> cost1;
    if (cost1 < 0) {
        std::cout << "Error 2: Invalid value for cost per ounce." << std::endl;
        return 2;
    }

    const Package package1(sender1, recipient1, weight1, cost1);
    const double shippingCost1 = package1.calculateCost();
    std::cout << "Cost for shipping package is " << shippingCost1<< std::endl;
    
    double flatFee1;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input flat Fee: ";
    }
    std::cin >> flatFee1;

    const TwoDayPackage twoDayPackage1(sender1, recipient1, weight1, cost1, flatFee1);
    const double cost2 = twoDayPackage1.calculateCost();
    
    std::cout << "Cost for Two Day Package when flat fee is " << flatFee1 << " is " << cost2 << std::endl;

    double fee1;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input fee per ounce: ";
    }
    std::cin >> fee1;    
    const OvernightPackage overnightPackage(sender1, recipient1, weight1, cost1, fee1);
    const double cost3 = overnightPackage.calculateCost();
    std::cout << "Cost for Overnight Package when fee per ounce is " << fee1 << " is " << cost3 << std::endl;

    return 0;
}

