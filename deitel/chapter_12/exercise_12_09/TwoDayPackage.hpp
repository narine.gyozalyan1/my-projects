#ifndef __TWO_DAY_PACKAGE_HPP__
#define __TWO_DAY_PACKAGE_HPP__

#include "Package.hpp"

class TwoDayPackage : public Package
{
public:
    TwoDayPackage(const Person& sender, const Person& recipient, const double weight, const double costPerOunce, const double flatFee);
;
    double calculateCost() const;
private:
    double flatFee_;
};

#endif /// __TWO_DAY_PACKAGE_HPP__

