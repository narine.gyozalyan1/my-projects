#ifndef __PACKAGE_HPP__
#define __PACKAGE_HPP__

#include "Person.hpp"

#include <string>

class Package
{
public:
    Package(const Person& sender, const Person& recipient, const double weight, const double costPerOunce);
    double calculateCost() const;
    double getWeight() const;
private:
    Person sender_;
    Person recipient_;
    double weight_;
    double costPerOunce_;
};

#endif /// __PACKAGE_HPP__

