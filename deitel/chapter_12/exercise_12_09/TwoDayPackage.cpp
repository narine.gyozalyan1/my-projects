#include "TwoDayPackage.hpp"

TwoDayPackage::TwoDayPackage(const Person& sender, const Person& recipient, const double weight, 
                             const double costPerOunce, const double flatFee)
    : Package(sender, recipient, weight, costPerOunce)
{
    flatFee_ = flatFee;
}

double
TwoDayPackage::calculateCost() const
{
    return Package::calculateCost() + Package::calculateCost() * flatFee_ / 100;
}

