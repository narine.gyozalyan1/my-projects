#include "Package.hpp"
#include "Person.hpp"

Package::Package(const Person& sender, const Person& recipient, const double weight, const double costPerOunce)
    : sender_(sender), recipient_(recipient)
{
    weight_ = (weight > 0) ? weight : 1;
    costPerOunce_ = (costPerOunce > 0) ? costPerOunce : 1;
}

double
Package::calculateCost() const
{
    return weight_ * costPerOunce_;
}

double
Package::getWeight() const
{
    return weight_;
}

