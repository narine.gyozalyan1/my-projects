#include "OvernightPackage.hpp"

OvernightPackage::OvernightPackage(const Person& sender, const Person& recipient, const double weight, 
                             const double costPerOunce, const double feePerOunce)
    : Package(sender, recipient, weight, costPerOunce)
{
    feePerOunce_ = feePerOunce;
}

double
OvernightPackage::calculateCost() const
{
    return Package::calculateCost() + feePerOunce_ * Package::getWeight();
}

