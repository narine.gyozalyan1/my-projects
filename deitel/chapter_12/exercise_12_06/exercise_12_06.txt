                                            Student
     UndergraduateStudent                                                GraduateStudent
Freshman  Sophomore  Junior  Senior                             DoctoralStudent    MastersStudent


This is an inheritance hierarchy with three levels. we ca see that for example UndergraduateStudent is a Student, and Freshman is an UndergraduateStudent. Student is the direct base 
class of UndergraduateStudent and GraduateStudent. In addition, Student is an indirect base class of all other classes.
