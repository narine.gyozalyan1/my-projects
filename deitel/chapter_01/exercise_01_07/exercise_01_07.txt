Object-oriented programming enables the programmer to build reusable softwarecomponents that model items in the real world. Building software quickly, correctly, andeconomically has been an elusive goal in the software industry. The modular, object-oriented design and implementation approach has been found to increase productivitywhile reducing development time, errors, and cost.

