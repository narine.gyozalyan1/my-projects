#include "Employee.hpp"

#include <iostream>
#include <string>

int
main()
{
    std::string firstName1, lastName1;
    int monthlySalary1;
    std::cout << "Enter first name: ";
    std::cin >> firstName1;
    std::cout << "Enter last name: ";
    std::cin >> lastName1;
    std::cout << "Enter monthly salary: ";
    std::cin >> monthlySalary1;

    Employee employee1(firstName1, lastName1, monthlySalary1);

    std::cout << "Employee1's name is: " << employee1.getFirstName() << " " << employee1.getLastName() << std::endl;
    std::cout << "Monthly salary is " << employee1.getMonthlySalary() << " and annual salary is " << employee1.yearlySalary() << std::endl;
    employee1.setMonthlySalary(monthlySalary1 * 110 / 100);
    std::cout << "After 10 percent increment monthly salary is " << employee1.getMonthlySalary() << std::endl;
    std::cout << std::endl;

    std::string firstName2, lastName2;
    int monthlySalary2;
    std::cout << "Enter first name: ";
    std::cin >> firstName2;
    std::cout << "Enter last name: ";
    std::cin >> lastName2;
    std::cout << "Enter monthly salary: ";
    std::cin >> monthlySalary2;

    Employee employee2(firstName2, lastName2, monthlySalary2);

    std::cout << "Employee2's name is: " << employee2.getFirstName() << " " << employee1.getLastName() << std::endl;
    std::cout << "Monthly salary is " << employee2.getMonthlySalary() << " and annual salary is " << employee2.yearlySalary() << std::endl;
    employee2.setMonthlySalary(monthlySalary2 * 110 / 100);
    std::cout << "After 10 percent increment monthly salary is " << employee2.getMonthlySalary() << std::endl;

    return 0;
}

