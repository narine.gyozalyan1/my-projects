#include "Invoice.hpp"

#include <iostream>

int
main()
{
    Invoice invoice1("45", "pen", 20, 100);
    Invoice invoice2("20", "pencil", 50, 50);

    invoice1.print();
    std::cout << "Invoice amount is " << invoice1.getInvoiceAmount() << std::endl;
    std::cout << "---------------------------" << std::endl;
    invoice2.print();
    std::cout << "Invoice amount is " << invoice2.getInvoiceAmount() << std::endl;

    return 0;
}

