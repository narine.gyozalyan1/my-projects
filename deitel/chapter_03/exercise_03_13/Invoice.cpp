#include "Invoice.hpp"
    
#include <iostream>

Invoice::Invoice(std::string partNumber, std::string partDescription, int itemQuantity, int itemPrice)
{
    setPartNumber(partNumber);
    setPartDescription(partDescription);
    setItemQuantity(itemQuantity);
    setItemPrice(itemPrice);
}

void
Invoice::setPartNumber(std::string partNumber)
{
    partNumber_ = partNumber;
}

std::string
Invoice::getPartNumber()
{
    return partNumber_;
}

void
Invoice::setPartDescription(std::string partDescription)
{
    partDescription_ = partDescription;
}

std::string
Invoice::getPartDescription()
{
    return partDescription_;
}

void
Invoice::setItemQuantity(int itemQuantity)
{
    if (itemQuantity <= 0) {
        itemQuantity_ = 0;
        return;
    }
    itemQuantity_ = itemQuantity;
}

int
Invoice::getItemQuantity()
{
    return itemQuantity_;
}

void
Invoice::setItemPrice(int itemPrice)
{
    if (itemPrice <= 0) {
        itemPrice_ = 0;
        return;
    }
    itemPrice_ = itemPrice;
}

int
Invoice::getItemPrice()
{
    return itemPrice_;
}

int
Invoice::getInvoiceAmount()
{
    return (itemQuantity_ * itemPrice_);
}

void
Invoice::print()
{
    std::cout << "The number is: " << getPartNumber() << std::endl;
    std::cout << "The description is: " << getPartDescription() << std::endl;
    std::cout << "The quntity is: " << getItemQuantity() << std::endl;
    std::cout << "The price is: " << getItemPrice() << std::endl << std::endl;
}

