#include "Date.hpp"

#include <iostream>

int
main()
{
    int month1, day1, year1;
    std::cout << "Enter month: ";
    std::cin >> month1;
    std::cout << "Enter day: ";
    std::cin >> day1;
    std::cout << "Enter year: ";
    std::cin >> year1;

    Date date1(month1, day1, year1);
    std::cout << "Date is: ";
    date1.displayDate();
    std::cout << std::endl;

    int month2, day2, year2;
    std::cout << "Enter month: ";
    std::cin >> month2;
    std::cout << "Enter day: ";
    std::cin >> day2;
    std::cout << "Enter year: ";
    std::cin >> year2;

    Date date2(month2, day2, year2);
    std::cout << "Date is: ";
    date2.displayDate();
    std::cout << std::endl;

    return 0;
}

