3.7 Explain the purpose of a data member

A class normally consists of one or more member functions that manipulate the attributes that belong to a particular object of the class. Attributes are represented as variables in a class definition. Such variables are called data members and are declared inside a class definition but outside the bodies of the class's member-function definitions. Each object of a class maintains its own copy of its attributes in memory. A class maintains a variable as a data member so that it can be used or modified at any time during a program 's execution.

