#include "GradeBook.hpp"

int
main()
{
    GradeBook gradeBook1("CS101 Introduction to C++ Programming", "Poghos");
    GradeBook gradeBook2("CS102 Data Structures in C++", "Petros");

    gradeBook1.displayMessage();
    gradeBook2.displayMessage();

    return 0;
}

