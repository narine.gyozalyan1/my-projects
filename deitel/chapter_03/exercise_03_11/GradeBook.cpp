#include "GradeBook.hpp" /// include definition of class GradeBook

#include <iostream>

GradeBook::GradeBook(std::string courseName, std::string instructorName)
{
    setCourseName(courseName);
    setInstructorName(instructorName);
}

void
GradeBook::setCourseName(std::string courseName)
{
    courseName_ = courseName;
}

std::string
GradeBook::getCourseName()
{
    return courseName_;
}

void
GradeBook::displayMessage()
{
    std::cout << "Welcome to the grade book for\n" << getCourseName() << "!\n"
              << "This course is presented by " << getInstrucorName() << std::endl;
}

void
GradeBook::setInstructorName(std::string instructorName)
{
    instructorName_ = instructorName;
}

std::string
GradeBook::getInstrucorName()
{
    return instructorName_;
}

