#include "Account.hpp"

#include <iostream>

int
main()
{
    int accountBalance1;
    std::cout << "Please enter the account balance: ";
    std::cin >> accountBalance1;
    Account account1(accountBalance1);

    int accountBalance2;
    std::cout << "Please enter the account balance: ";
    std::cin >> accountBalance2;
    Account account2(accountBalance2);

    std::cout << "Account1's balance is " << account1.getBalance() << std::endl;
    std::cout << "Account2's balance is " << account2.getBalance() << std::endl;

    account1.credit(50000);
    account2.credit(50000);
    std::cout << "Account1's balance after credit is " << account1.getBalance() << std::endl;
    std::cout << "Account2's balance after credit is " << account2.getBalance() << std::endl;    
    account1.debit(70000);
    account2.debit(70000);
    std::cout << "Account1's balance after debit is " << account1.getBalance() << std::endl;
    std::cout << "Account2's balance after debit is " << account2.getBalance() << std::endl; 

    return 0;
}

