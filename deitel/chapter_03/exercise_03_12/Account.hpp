class Account
{
public:
    Account(int accountBalance);
    void credit(int amount);
    void debit(int amount);
    int getBalance(); 
private:
    int accountBalance_;
};

