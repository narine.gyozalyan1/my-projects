#include "Account.hpp"

#include <iostream>

Account::Account(int accountBalance)
{
    if (accountBalance < 0) {
        accountBalance_ = 0;
        std::cout << "Warning 1: The initial balance was invalid. Resetting to 0" << std::endl;
        return;
    }
    accountBalance_ = accountBalance;
}

void
Account::credit(int ammount)
{
    accountBalance_ = accountBalance_ + ammount;
}

void
Account::debit(int ammount)
{
    if (accountBalance_ < ammount) {
        std::cout << "Info 1: Debit amount exceeded account balance." << std::endl;
        return;
    }
    accountBalance_ = accountBalance_ - ammount;
}

int
Account::getBalance()
{
    return accountBalance_;
}

