#include <iostream>

#include <unistd.h>

template<typename T>
void
swap(T* const element1Ptr, T* const element2Ptr )
{
    T hold = *element1Ptr;
    *element1Ptr = *element2Ptr;
    *element2Ptr = hold;
}

template<typename T>
void
selectionSort(T* array, const int size)
{
    for (int i = 0; i < size - 1; ++i) {
        int smallest = i;
        for (int index = i + 1; index < size; index++) {
            if (array[index] < array[smallest]) {
                smallest = index;
            }
        }
        swap(&array[i], &array[smallest]);
    }
}

int
main()
{
    if (::isatty(STDIN_FILENO)) {    
        std::cout << "Enter the size of array of integers: ";
    }
    int size1;
    std::cin >> size1;

    if (size1 < 0) {
        std::cout << "Error1: Invalid size!" << std::endl;
        return 1;
    }
    int intArray[size1];
    if (::isatty(STDIN_FILENO)) {    
        std::cout << "Enter array elements: ";
    }
    for (int i = 0; i < size1; ++i) {
        std::cin >> intArray[i];
    }
    std::cout << "Unsorted Array: ";
    for (int i = 0; i < size1; ++i) {
        std::cout << intArray[i] << " ";
    }
    std::cout << std::endl;
    selectionSort(intArray, size1);
    std::cout << "Sorted Array: ";
    for (int i = 0; i < size1; ++i) {
        std::cout << intArray[i] << " ";
    }
    std::cout << std::endl;

    if (::isatty(STDIN_FILENO)) {    
        std::cout << "Enter the size of array of floats: ";
    }
    int size2;
    std::cin >> size2;

    if (size2 < 0) {
        std::cout << "Error1: Invalid size!" << std::endl;
        return 1;
    }
    float floatArray[size2];
    if (::isatty(STDIN_FILENO)) {    
        std::cout << "Enter array elements: ";
    }
    for (int i = 0; i < size2; ++i) {
        std::cin >> floatArray[i];
    }
    std::cout << "Unsorted Array: ";
    for (int i = 0; i < size2; ++i) {
        std::cout << floatArray[i] << " ";
    }
    std::cout << std::endl;
    selectionSort(floatArray, size2);
    std::cout << "Sorted Array: ";
    for (int i = 0; i < size2; ++i) {
        std::cout << floatArray[i] << " ";
    }
    std::cout << std::endl;
    
    return 0;
}

