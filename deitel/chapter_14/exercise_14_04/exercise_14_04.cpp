#include <iostream>
#include <cassert>
#include <unistd.h>
#include <stdlib.h> 

template <typename T>
void
printArray(const T *array, const int count)
{
    for (int i = 0; i < count; ++i) {
        std::cout << array[i] << " ";
    }
    std::cout << std::endl;
}

template <typename T>
int
printArray(const T* array, const int count, const int lowSubscript, const int highSubscript)
{
    assert(lowSubscript >= 0);
    assert(highSubscript < count);
    assert(lowSubscript <= highSubscript);

    int elementCount = 0;
    for (int i = lowSubscript; i <= highSubscript; ++i) {
        std::cout << array[i] << " ";
        ++elementCount;
    }
    std::cout << std::endl;
    return elementCount;
}

template <typename T>
int
inputArray(T* arr, const int size)
{
    int count;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input the size (1 - " << size << " ) : ";
    }
    std::cin >> count;
    if (count < 1 || count > size) {
        std::cout << "Error 1: Invalid size." << std::endl;
        ::exit(1);
    }
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input array of size " << count << ": ";
    }
    for (int i = 0; i < count; ++i) {
        std::cin >> arr[i];
    }
    return count;
}

int
main()
{
    static const int SIZE = 60;
    
    int a[SIZE];
    int aCount = inputArray(a, SIZE);
    
    float b[SIZE];
    int bCount = inputArray(b, SIZE);

    char c[SIZE];
    int cCount = inputArray(c, SIZE);
    
    std::cout << "Array a contains:" << std::endl;
    printArray(a, aCount);
    std::cout << "Array b contains:" << std::endl;
    printArray(b, bCount);
    std::cout << "Array c contains:" << std::endl;
    printArray(c, cCount);
    
    std::cout << "Array a contains (in index 2):" << std::endl;
    printArray(a, aCount, 2, 2);
    std::cout << "Array b contains (from index 1 to 6):" << std::endl;
    printArray(b, bCount, 1, 6);
    std::cout << "Array a contains (from index 0 to 4):" << std::endl;
    printArray(c, cCount, 0, 4);

    return 0;
}

