Each class-tem plate specialization instantiated from a class template has its own copy of each static data member of the class tem plate; all objects of that
specialization share that one static data member, so there will be 3 copies. In addition, as with static data members of nontemplate classes, static data members 
of class-template specializations must be defined and, if necessary, initialized at file scope. Each class-template specialization gets its own copy of the class
template's static member functions.
