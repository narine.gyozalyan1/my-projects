#include <iostream>
#include <cassert>
#include <unistd.h>
#include <iomanip>
#include <stdlib.h> 

void
printArray(const std::string* array, const int count)
{
    for (int i = 0; i < count; ++i) {
        std::cout << std::setw(10) << array[i];
        if (0 == (i + 1) % 3) {
            std::cout << "\n";
        }
    }
    std::cout << std::endl;
}

template <typename T>
void
printArray(const T *array, const int count)
{
    for (int i = 0; i < count; ++i) {
        std::cout << array[i] << " ";
    }
    std::cout << std::endl;
}

template <typename T>
int
inputArray(T* arr, const int size)
{
    int count;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input the size (1 - " << size << " ) : ";
    }
    std::cin >> count;
    if (count < 1 || count > size) {
        std::cout << "Error 1: Invalid size." << std::endl;
        ::exit(1);
    }
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input array of size " << count << ": ";
    }
    for (int i = 0; i < count; ++i) {
        std::cin >> arr[i];
    }
    return count;
}

int
main()
{
  static const int SIZE = 60;
    
    int a[SIZE];
    int aCount = inputArray(a, SIZE);

    std::string c[SIZE];
    int cCount = inputArray(c, SIZE);

    std::cout << "Array a contains:" << std::endl;
    printArray(a, aCount);
    std::cout << "Array c contains:" << std::endl;
    printArray(c, cCount);

    return 0;
}


