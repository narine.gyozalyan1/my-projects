Function templates and overloading are intimately related. The function-template specializations generated from a function template all have the same name, so
the compiler uses overloading resolution to invoke the proper function.
A function template may be overloaded in several ways. We can provide other function templates that specify the same function name but different function
param eters, and also a function template can be overloaded by providing nontemplate functions with the same function name but different function arguments.
The com piler perform s a m atching process to determ ine what function to call when a function is invok ed. First, the com piler finds all function tem plates that m atch
the function nam ed in the function call and creates specializations based on the argum ents in the function call. Then, the com piler finds all the ordinary functions
that m atch the function nam ed in the function call. If one of the ordinary functions or function-tem plate specializations is the best m atch for the function call, that
ordinary function or specialization is used. If an ordinary function and a specialization are equally good m atches for the function call, then the ordinary function is
used. O therwise, if there are m ultiple m atches for the function call, the com piler considers the call to be am biguous and the com piler generates an error m essage.
