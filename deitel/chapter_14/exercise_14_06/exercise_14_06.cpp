#include <iostream>
#include <unistd.h>

template <typename T>
bool
isEqualTo(const T& argument1, const T& argument2)
{
    return argument1 == argument2;
}

class Complex
{
    friend std::ostream& operator<<(std::ostream& out, const Complex& c);
public:
    Complex(const int realPart = 0, const int imaginaryPart = 0) : real_(realPart), imaginary_(imaginaryPart) {}
    /// Without operator== the program will not compile, to compare equality of user-defined types we should overload operator==.
    bool operator==(const Complex& rhv) const
    {
        return (real_ == rhv.real_ && imaginary_ == rhv.imaginary_);
    }
private:
    int real_;
    int imaginary_;
};

std::ostream&
operator<<(std::ostream& out, const Complex& c) 
{
    out << '(' << c.real_ << ", " << c.imaginary_ << ')';
    return out;
}

int
main()
{
    int integer1;
    int integer2;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input two integers: ";
    }
    std::cin >> integer1 >> integer2;
    std::cout << integer1 << " and " << integer2 << " are " << (isEqualTo(integer1, integer2) ? "equal" : "not equal") << std::endl;

    char char1;
    char char2;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input two characters: ";
    }
    std::cin >> char1 >> char2;
    std::cout << char1 << " and " << char2 << " are " << (isEqualTo(char1, char2) ? "equal" : "not equal") << std::endl;

    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input real and imaginary parts for complex1 and for complex2: ";
    }
    int real1, imaginary1, real2, imaginary2;
    std::cin >> real1 >> imaginary1 >> real2 >> imaginary2;
    Complex complex1(real1, imaginary1);
    Complex complex2(real2, imaginary2);
    std::cout << complex1 << " and " << complex2 << " are " << (isEqualTo(complex1, complex2) ? "equal" : "not equal") << std::endl;

    return 0;
}

