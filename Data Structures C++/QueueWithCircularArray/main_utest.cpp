#include "headers/Queue.hpp"

#include <gtest/gtest.h>

TEST(Queue, QueueTest)
{
    mentor::Queue<int> q;
    q.insertQ(5);
    q.insertQ(4);
    q.insertQ(3);
    q.insertQ(2);
    std::cout << "Length is " << q.lengthQ() << std::endl; /// 4
    std::cout << "Deleted element is " << q.deleteQ() << std::endl; /// 5
    std::cout << "Deleted element is " << q.deleteQ() << std::endl; /// 4
    std::cout << "Deleted element is " << q.deleteQ() << std::endl; /// 3
    std::cout << "Length is " << q.lengthQ() << std::endl; /// 1

}

int
main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

