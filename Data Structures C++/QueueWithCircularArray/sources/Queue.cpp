#include "headers/Queue.hpp"

#include <cstdlib>

namespace mentor {

template <typename T>
Queue<T>::Queue()
    : front_(0), rear_(0), count_(0)
{
}

template <typename T>
void
Queue<T>::insertQ(const T& item)
{
    if (count_ == maxQSize) {
        std::cout << "Error 1: Queue overflow!" << std::endl;
        ::exit(1);
    }
    ++count_;
    qArray_[rear_] = item;
    rear_ = (rear_ + 1) % maxQSize;
}

template <typename T>
T
Queue<T>::deleteQ()
{
    if (0 == count_) {
        std::cout << "Error 2: Queue is empty!" << std::endl;
        ::exit(2);
    }
    T temp = qArray_[front_];
    --count_;
    front_ = (front_ + 1) % maxQSize;
    return temp;
}

template <typename T>
void
Queue<T>::clearQ()
{
    while (count_ != 0) {
        this->deleteQ();
    }
}

template <typename T>
bool
Queue<T>::isEmpty() const
{
    return 0 == count_;
}

template <typename T>
bool
Queue<T>::isFull() const
{
    return count_ == maxQSize - 1;
}

template <typename T>
int
Queue<T>::lengthQ() const
{
    return count_;
}

}

