#ifndef __QUEUE_HPP__
#define __QUEUE_HPP__

#include <iostream>

namespace mentor {

static const int maxQSize = 50;

template <typename T>
class Queue
{
public:
    Queue();
    void insertQ(const T& item);
    T deleteQ();
    void clearQ();
    T frontQ() const;
    bool isEmpty() const;
    bool isFull() const;
    int lengthQ() const;

private:
    T qArray_[maxQSize];
    int front_;
    int rear_;
    int count_;
};

} /// end of namespace mentor

#include <sources/Queue.cpp>

#endif /// __QUEUE_HPP__

