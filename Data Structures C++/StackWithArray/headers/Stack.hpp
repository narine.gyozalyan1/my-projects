#ifndef __STACK_HPP__
#define __STACK_HPP__

#include <iostream>

namespace mentor {

static const int maxStackSize = 50;

template <typename T>
class Stack
{
public:
    Stack();
    void push(const T& item);
    T pop();
    void clearStack();
    T peek() const;
    bool isEmpty() const;
    bool isFull() const;

private:
    T stackArray_[maxStackSize];
    int top_;
};

} /// end of namespace mentor

#include <sources/Stack.cpp>

#endif /// __STACK_HPP__

