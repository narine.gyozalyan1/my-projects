#include "headers/Stack.hpp"

#include <gtest/gtest.h>

TEST(Stack, StackTest)
{
    mentor::Stack<int> s;
    s.push(10);
    std::cout << s.peek() << std::endl; /// 10
    if (!s.isEmpty()) {
        int temp = s.pop();
        std::cout << temp << std::endl; /// 10
    }
}

int
main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

