#include "headers/Stack.hpp"

#include <cstdlib>

namespace mentor {

template <typename T>
Stack<T>::Stack()
    : top_(-1)
{ }

template <typename T>
void
Stack<T>::push(const T& item)
{
    if (top_ == maxStackSize - 1) {
        std::cout << "Error 1: Stack Overflow!" << std::endl;
        ::exit(1);
    }
    ++top_;
    stackArray_[top_] = item;
}

template <typename T>
T
Stack<T>::pop()
{
    T temp;
    if (-1 == top_) {
        std::cout << "Error 2: Stack is empty!" << std::endl;
        ::exit(2);
    }
    temp = stackArray_[top_];
    --top_;
    return temp;
}

template <typename T>
T
Stack<T>::peek() const
{
    if (-1 == top_) {
        std::cout << "Error 2: Stack is empty!" << std::endl;
        ::exit(2);
    }
    return stackArray_[top_];
}

template <typename T>
bool
Stack<T>::isEmpty() const
{
    return -1 == top_;
}

template <typename T>
bool
Stack<T>::isFull() const
{
    return maxStackSize - 1 == top_;
}

template <typename T>
void
Stack<T>::clearStack()
{
    return top_ = -1;
}

}

