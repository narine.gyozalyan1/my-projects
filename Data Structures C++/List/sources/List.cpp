#include "headers/List.hpp"

#include <cassert>
#include <cstddef>
#include <cmath>
#include <algorithm>

namespace cd05 {

template <typename T>
std::ostream&
operator<<(std::ostream& out, const List<T>& l)
{
    out << ' ';
    typename cd05::List<T>::Node* p = l.begin_;
    while (p != NULL) {
        out << p->data_ << ' ';
        p = p->next_;
    }
    return out;
}

template <typename T>
List<T>::List()
    : begin_(NULL)
    , end_(NULL)
    , size_(0)
{
}

template <typename T>
List<T>::List(const size_type size)
    : begin_(NULL)
    , end_(NULL)
    , size_(0)
{
    resize(size);
}

template <typename T>
List<T>::List(const int size, const_reference value)
    : begin_(NULL)
    , end_(NULL)
    , size_(0)
{
    assert(size >= 0);
    resize(static_cast<size_type>(size), value);
}

template <typename T>
List<T>::List(const List& rhv)
    : begin_(NULL)
    , end_(NULL)
    , size_(0)
{

    Node* rhvCurrent = rhv.begin_;

    while (rhvCurrent != NULL) {
        push_back(rhvCurrent->data_);
        rhvCurrent = rhvCurrent->next_;
    }
}

template <typename T>
template <typename InputIterator>
List<T>::List(InputIterator f, InputIterator l)
    : begin_(NULL)
    , end_(NULL)
    , size_(0)
{
    while (f != l) {
        push_back(*f);
        ++f;
    }
}

template <typename T>
List<T>::~List()
{
    clear();
}

template <typename T>
const typename List<T>::List&
List<T>::operator=(const List& rhv)
{
    if (&rhv == this) {
        return *this;
    }
    Node* current = begin_;
    Node* rhvCurrent = rhv.begin_;
    
    size_t size1 = size();
    size_t size2 = rhv.size();

    while (current != NULL && rhvCurrent != NULL) {
        current->data_ = rhvCurrent->data_;
        current = current->next_;
        rhvCurrent = rhvCurrent->next_;
    }
    while (rhvCurrent != NULL) {
        push_back(rhvCurrent->data_);
        rhvCurrent = rhvCurrent->next_;
    }
    if (size1 > size2) {
         size_type delta = size1 - size2;
         while (delta-- > 0) {
             pop_back();
         }
    }
    return *this;
}

template <typename T>
void
List<T>::clear()
{
    while (begin_ != NULL) {
        pop_back();
    }
}

template <typename T>
void
List<T>::push_back(const_reference value)
{
    if(NULL == begin_) {
        begin_ = new Node(value);
        end_ = begin_;
        ++size_;
        return;
    }
    Node* newNode = new Node(value);
    end_->next_ = newNode;
    newNode->prev_ = end_;
    end_ = newNode;
    ++size_;
}

template <typename T>
void
List<T>::pop_back()
{
    assert(begin_ != NULL);

    if (NULL == begin_->next_) {
        delete begin_;
        begin_ = end_ = NULL;
        --size_;
        return;
    }
    
    end_ = end_->prev_;
    delete end_->next_;
    end_->next_ = NULL;
    --size_;
}

template <typename T>
void
List<T>::push_front(const_reference value)
{
    if(NULL == begin_) {
        begin_ = new Node(value);
        end_ = begin_;
        ++size_;
        return;
    }
    Node* newNode = new Node(value);
    begin_->prev_ = newNode;
    newNode->next_ = begin_;
    begin_ = newNode;
    ++size_;
}

template <typename T>
void
List<T>::pop_front()
{
    assert(begin_ != NULL);

    if (NULL == begin_->next_) {
        delete begin_;
        begin_ = end_ = NULL;
        --size_;
        return;
    }
   
    begin_ = begin_->next_;
    delete begin_->prev_;
    begin_->prev_ = NULL;

    --size_;
}

template <typename T>
void
List<T>::swap(List<T>& rhv)
{
    Node* temp = begin_;
    Node* temp1 = end_;
    size_type s = size_;
    end_ = rhv.end_;
    begin_ = rhv.begin_;
    size_ = rhv.size_;
    rhv.begin_ = temp;
    rhv.end_ = temp1;
    rhv.size_ = s;
}

template <typename T>
bool
List<T>::empty() const
{
    return NULL == begin_;
}

template <typename T>
typename List<T>::size_type
List<T>::size() const
{
    return size_;
}

template <typename T>
typename List<T>::size_type
List<T>::max_size() const
{
    return ::pow(2, sizeof(pointer) * 8) / (sizeof(T) + 32); /// 16-y begin, end, 16y next, prev 
}

template <typename T>
void
List<T>::resize(const size_type s)
{
    const size_type lSize = size();
    if (s == lSize) {
        return;
    }
    for (size_t i = s; i < lSize; ++i) {
        pop_back();
    }
    for (size_t i = lSize; i < s; ++i) {
        push_back(T());
    }
}

template <typename T>
void
List<T>::resize(const size_type s, const_reference init)
{
    const size_type lSize = size();
    size_type temp = s;
    if (temp == lSize) {
        return;
    }
    for (size_t i = s; i < lSize; ++i) {
        pop_back();
    }
    for (size_t i = lSize; i < s; ++i) {
        push_back(init);
    }
}

template <typename T>
typename List<T>::iterator
List<T>::insert(iterator pos, const_reference x)
{
    if (pos == begin()) {
        push_front(x);
        pos = begin();
        return pos;
    }
    Node* newNode = new Node(x, pos.getPtr());
    newNode->prev_ = pos->prev_;
    newNode->prev_->next_ = newNode;
    newNode->next_->prev_ = newNode;
    ++size_;
    --pos;
    return pos;
}

template <typename T>
void
List<T>::insert(iterator pos, const size_type n, const_reference x)
{   
    for (size_type i = 0; i < n; ++i) {
        pos = insert(pos, x);
    }
}

template <typename T>
template <typename InputIterator>
void
List<T>::insert(iterator pos, InputIterator f, InputIterator l)
{
    for (InputIterator it = f; it != l; ++it) {
        pos = insert(pos, *it);
    }
}

template <typename T>
typename List<T>::iterator
List<T>::insert_after(iterator pos, const_reference x)
{
    if (pos == begin()) {
        push_back(x);
        pos = begin();
        return pos;
    }
    if (pos->next_ != NULL) {
        Node* newNode = new Node(x);
        newNode->next_ = pos->next_;
        newNode->prev_ = pos.getPtr();
        newNode->next_->prev_ = newNode;
        newNode->prev_->next_ = newNode;
        ++size_;
    } else {
        push_back(x);
    }
    return ++pos;
}

template <typename T>
void
List<T>::insert_after(iterator pos, const size_type n, const_reference x)
{   
    for (size_type i = 0; i < n; ++i) {
        pos = insert_after(pos, x);
    }
}

template <typename T>
template <typename InputIterator>
void
List<T>::insert_after(iterator pos, InputIterator f, InputIterator l)
{
    for (InputIterator it = f; it != l; ++it) {
        pos = insert_after(pos, *it);
    }
}

template <typename T>
typename List<T>::iterator
List<T>::erase(iterator pos)
{
    if (pos.getPtr() == begin_) {
        ++pos;
        pop_front();
        return pos;
    }
    if (pos.getPtr() == end_) {
        pop_back();
        return end();
    }
    
    Node* temp = pos.getPtr();
    pos->next_->prev_ = pos->prev_;
    pos->prev_->next_ = pos->next_;
    ++pos;
    delete temp;
    --size_;
    return pos;
}

template <typename T>
typename List<T>::iterator
List<T>::erase(iterator f, iterator l)
{
    for (iterator it = f; it != l;) {
        it = erase(it);
    }
    return l;
}

template <typename T>
void
List<T>::remove(const_reference value)
{
    for (iterator it = begin(); it != end(); ) {
        (*it == value) ? it = erase(it) : ++it; 
    }
}

template <typename T>
bool
List<T>::operator==(const List<T>& rhv) const
{
    if (size() != rhv.size()) {
        return false;
    }
    const_iterator it = rhv.begin();
    for (const_iterator it1 = begin(); it1 != end(); ++it1, ++it) {
        if (*it1 != *it) {
            return false;
        }
    }
    return true;
}

template <typename T>
bool
List<T>::operator>(const List<T>& rhv) const
{
    if (size() <= rhv.size()) {
        return false;
    }
    const_iterator it = begin();
    for (const_iterator it1 = rhv.begin(); it1 != rhv.end(); ++it1, ++it) {
        if (*it <= *it1) {
            return false;
        }
    }
    return true;
}

template <typename T>
typename List<T>::const_reference
List<T>::front() const
{
    assert(!empty());
    return begin_->data_;
}

template <typename T>
typename List<T>::reference
List<T>::front()
{
    assert(!empty());
    return begin_->data_;
}

template <typename T>
typename List<T>::const_reference
List<T>::back() const
{
    assert(!empty());
    return end_->data_;
}

template <typename T>
typename List<T>::reference
List<T>::back()
{
    assert(!empty());
    return end_->data_;
}

template <typename T>
typename List<T>::const_iterator
List<T>::begin() const
{
    return const_iterator(begin_);
}

template <typename T>
typename List<T>::iterator
List<T>::begin()
{
    return iterator(begin_);
}

template <typename T>
typename List<T>::const_iterator
List<T>::end() const
{
    return const_iterator(NULL); /// NULL
}

template <typename T>
typename List<T>::iterator
List<T>::end()
{
    return iterator(NULL);
}

template <typename T>
typename List<T>::const_reverse_iterator
List<T>::rbegin() const
{
    return const_reverse_iterator(end_);
}

template <typename T>
typename List<T>::reverse_iterator
List<T>::rbegin()
{
    return reverse_iterator(end_);
}

template <typename T>
typename List<T>::const_reverse_iterator
List<T>::rend() const
{
    return const_reverse_iterator(NULL);
}

template <typename T>
typename List<T>::reverse_iterator
List<T>::rend()
{
    return reverse_iterator(NULL);
}

/// const_iterator

template <typename T>
List<T>::const_iterator::const_iterator()
    : ptr_(NULL)
{
}

template <typename T>
List<T>::const_iterator::const_iterator(const const_iterator& rhv)
    : ptr_(rhv.ptr_)
{
}

template <typename T>
List<T>::const_iterator::const_iterator(Node* ptr)
    : ptr_(ptr)
{
}

template <typename T>
const typename List<T>::const_iterator&
List<T>::const_iterator::operator=(const const_iterator& rhv)
{
    ptr_ = rhv.ptr_;
    return *this;
}

template <typename T>
List<T>::const_iterator::~const_iterator()
{
    ptr_ = NULL;
}

template <typename T>
typename List<T>::Node*
List<T>::const_iterator::getPtr() const
{
    return ptr_;
}

template <typename T>
void
List<T>::const_iterator::setPtr(Node* ptr)
{
    ptr_ = ptr;
}

template <typename T>
bool
List<T>::const_iterator::operator==(const const_iterator& rhv) const
{
    return ptr_ == rhv.ptr_;
}

template <typename T>
bool
List<T>::const_iterator::operator!=(const const_iterator& rhv) const
{
    return ptr_ != rhv.ptr_;
}

template <typename T>
typename List<T>::const_reference
List<T>::const_iterator::operator*() const
{
    assert(ptr_ != NULL);
    return ptr_->data_;
}

template <typename T>
const typename List<T>::Node*
List<T>::const_iterator::operator->() const
{
    assert(ptr_ != NULL);
    return ptr_;
}

template <typename T>
typename List<T>::const_iterator&
List<T>::const_iterator::operator++()
{
    assert(ptr_ != NULL);
    ptr_ = ptr_->next_;
    return *this;
}

template <typename T>
typename List<T>::const_iterator
List<T>::const_iterator::operator++(int)
{
    assert(ptr_ != NULL);
    const_iterator temp = *this;
    ptr_ = ptr_->next_;
    return temp;
}

template <typename T>
typename List<T>::const_iterator&
List<T>::const_iterator::operator--()
{
    assert(ptr_ != NULL);
    ptr_ = ptr_->prev_;;
    return *this;
}

template <typename T>
typename List<T>::const_iterator
List<T>::const_iterator::operator--(int)
{
    assert(ptr_ != NULL);
    const_iterator temp = *this;
    ptr_ = ptr_->prev_;;
    return temp;
}

/// iterator

template <typename T>
List<T>::iterator::iterator()
    : const_iterator()
{
}

template <typename T>
List<T>::iterator::iterator(const iterator& rhv)
    : const_iterator(rhv)
{
}

template <typename T>
List<T>::iterator::iterator(Node* ptr)
    : const_iterator(ptr)
{
}

template <typename T>
const typename List<T>::iterator&
List<T>::iterator::operator=(const iterator& rhv)
{
    const_iterator::setPtr(rhv.const_iterator::getPtr());
    return *this;
}

template <typename T>
List<T>::iterator::~iterator()
{
}

template <typename T>
bool
List<T>::iterator::operator!=(const iterator& rhv) const
{
    return const_iterator::getPtr() != rhv.const_iterator::getPtr();
}

template <typename T>
typename List<T>::reference
List<T>::iterator::operator*() const
{
    assert(const_iterator::getPtr() != NULL);
    return const_iterator::getPtr()->data_;
}

template <typename T>
typename List<T>::Node*
List<T>::iterator::operator->() const
{
    return const_iterator::getPtr();
}

template <typename T>
typename List<T>::iterator&
List<T>::iterator::operator++()
{
    const_iterator::setPtr(const_iterator::getPtr()->next_);
    return *this;
}

template <typename T>
typename List<T>::iterator
List<T>::iterator::operator++(int)
{
    iterator temp = *this;
    const_iterator::setPtr(const_iterator::getPtr()->next_);
    return temp;
}

/// const_iterator_reverse

template <typename T>
List<T>::const_reverse_iterator::const_reverse_iterator()
    : const_iterator()
{
}

template <typename T>
List<T>::const_reverse_iterator::const_reverse_iterator(const const_reverse_iterator& rhv)
    : const_iterator(rhv)
{
}

template <typename T>
List<T>::const_reverse_iterator::const_reverse_iterator(Node* ptr)
    : const_iterator(ptr)
{
}

template <typename T>
const typename List<T>::const_reverse_iterator&
List<T>::const_reverse_iterator::operator=(const const_reverse_iterator& rhv)
{
    const_iterator::setPtr(rhv.const_iterator::getPtr());
    return *this;
}

template <typename T>
List<T>::const_reverse_iterator::~const_reverse_iterator()
{ }

template <typename T>
typename List<T>::const_reference
List<T>::const_reverse_iterator::operator*() const
{
    assert(const_iterator::getPtr() != NULL);
    return const_iterator::getPtr()->data_;
}

template <typename T>
const typename List<T>::Node*
List<T>::const_reverse_iterator::operator->() const
{
    return const_iterator::getPtr();
}

template <typename T>
typename List<T>::const_reverse_iterator&
List<T>::const_reverse_iterator::operator++()
{
    const_iterator::setPtr(const_iterator::getPtr()->prev_);
    return *this;
}

template <typename T>
typename List<T>::const_reverse_iterator
List<T>::const_reverse_iterator::operator++(int)
{
    const_reverse_iterator temp = *this;
    const_iterator::setPtr(const_iterator::getPtr()->prev_);
    return temp;
}

template <typename T>
typename List<T>::const_reverse_iterator&
List<T>::const_reverse_iterator::operator--()
{
    const_iterator::setPtr(const_iterator::getPtr()->next_);
    return *this;;
}

template <typename T>
typename List<T>::const_reverse_iterator
List<T>::const_reverse_iterator::operator--(int)
{
    const_reverse_iterator temp = *this;
    const_iterator::setPtr(const_iterator::getPtr()->next_);
    return temp;
}

/// reverse_iterator

template <typename T>
List<T>::reverse_iterator::reverse_iterator()
    : const_reverse_iterator()
{
}

template <typename T>
List<T>::reverse_iterator::reverse_iterator(const reverse_iterator& rhv)
    : const_reverse_iterator(rhv)
{
}

template <typename T>
List<T>::reverse_iterator::reverse_iterator(Node* ptr)
    : const_reverse_iterator(ptr)
{
}

template <typename T>
const typename List<T>::reverse_iterator&
List<T>::reverse_iterator::operator=(const reverse_iterator& rhv)
{
    const_iterator::setPtr(rhv.const_iterator::getPtr());
    return *this;
}

template <typename T>
List<T>::reverse_iterator::~reverse_iterator()
{ }

template <typename T>
typename List<T>::reference
List<T>::reverse_iterator::operator*() const
{
    assert(const_iterator::getPtr() != NULL);
    return const_iterator::getPtr()->data_;
}

template <typename T>
typename List<T>::Node*
List<T>::reverse_iterator::operator->() const
{
    return const_iterator::getPtr();
}

}

