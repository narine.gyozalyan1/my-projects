#include "headers/List.hpp"

#include <gtest/gtest.h>

TEST(ListTest, Constructor)
{
    cd05::List<int> l(3);
    size_t s = 4;
    cd05::List<int> l1(s, 6);
    
    std::stringstream out;
    out << l;
    EXPECT_STREQ(out.str().c_str(), " 0 0 0 ");

    std::stringstream out1;
    out1 << l1;
    EXPECT_STREQ(out1.str().c_str(), " 6 6 6 6 ");

    cd05::List<int> l2(l1);
//    cd05::List<int> l2 = l1;
    std::stringstream out2;
    out2 << l2;
    EXPECT_STREQ(out2.str().c_str(), " 6 6 6 6 ");
}

TEST(ListTest, RangeConstructor)
{
    size_t s = 5;
    cd05::List<int> l1(s, 6);

    std::stringstream out1;
    out1 << l1;
    EXPECT_STREQ(out1.str().c_str(), " 6 6 6 6 6 ");
   
    int size = 5;
    std::vector<int> v(size, 3);
    std::vector<int>::iterator it1 = v.begin();
    std::vector<int>::iterator it2 = v.begin() + 3;

    cd05::List<int> l2(it1, it2);
    std::stringstream out2;
    out2 << l2;
    EXPECT_STREQ(out2.str().c_str(), " 3 3 3 ");
}

TEST(ListTest, Operators)
{
    cd05::List<int> l1(5, 5);
    cd05::List<int> l2(3, 3);
    cd05::List<int> l3(5, 5);

    EXPECT_EQ((l1 == l2), false);
    EXPECT_EQ((l1 == l3), true);
    EXPECT_EQ((l1 > l2), true);
}

TEST(ListTest, Size)
{
    cd05::List<double> l;
    EXPECT_EQ(l.size(), 0);
    EXPECT_EQ(l.empty(), true);
    cd05::List<int> l1(5);
    EXPECT_EQ(l1.size(), 5);
    EXPECT_EQ(l1.empty(), false);

    size_t s = 4;
    cd05::List<int> l2(s, 6);
    EXPECT_EQ(l2.size(), 4);
}

TEST(ListTest, PushBack)
{
    cd05::List<int> l;
    l.push_back(5);
    EXPECT_EQ(l.size(), 1);
    std::stringstream out;
    out << l;
    EXPECT_STREQ(out.str().c_str(), " 5 ");
    
    size_t s = 4;
    cd05::List<int> l1(s, 6);
    l1.push_back(5);
    std::stringstream out1;
    out1 << l1;
    EXPECT_STREQ(out1.str().c_str(), " 6 6 6 6 5 ");
     EXPECT_EQ(l1.size(), 5);
}

TEST(ListTest, PopBack)
{
    cd05::List<int> l;
    l.push_back(5);
    std::stringstream out;
    out << l;
    EXPECT_STREQ(out.str().c_str(), " 5 ");
    l.pop_back();
    EXPECT_EQ(l.size(), 0);
    std::stringstream out1;
    out1 << l;
    EXPECT_STREQ(out1.str().c_str(), " ");

    size_t s = 4;
    cd05::List<int> l1(s, 6);
    l1.pop_back();
    EXPECT_EQ(l1.size(), 3);
}

TEST(ListTest, Clear)
{
    size_t s = 4;
    cd05::List<int> l1(s, 6);
    EXPECT_EQ(l1.size(), 4);
    l1.clear();
    EXPECT_EQ(l1.size(), 0);

    cd05::List<int> l2;
    EXPECT_EQ(l2.size(), 0);
    l1.clear();
    EXPECT_EQ(l1.size(), 0);
}

TEST(ListTest, Swap)
{
    cd05::List<int> l1(3, 6);
    EXPECT_EQ(l1.size(), 3);

    std::stringstream out1;
    out1 << l1;
    EXPECT_STREQ(out1.str().c_str(), " 6 6 6 ");
    
    cd05::List<int> l2(4, 1);
    EXPECT_EQ(l2.size(), 4);

    std::stringstream out2;
    out2 << l2;
    EXPECT_STREQ(out2.str().c_str(), " 1 1 1 1 ");
    
    l1.swap(l2);
    
    EXPECT_EQ(l1.size(), 4);
    std::stringstream out3;
    out3 << l1;
    EXPECT_STREQ(out3.str().c_str(), " 1 1 1 1 ");
 
    EXPECT_EQ(l2.size(), 3);
    std::stringstream out4;
    out4 << l2;
    EXPECT_STREQ(out4.str().c_str(), " 6 6 6 ");
}

TEST(ListTest, Resize)
{
    cd05::List<int> l1(4, 6);
    EXPECT_EQ(l1.size(), 4);

    std::stringstream out1;
    out1 << l1;
    EXPECT_STREQ(out1.str().c_str(), " 6 6 6 6 ");
    l1.resize(7);
    EXPECT_EQ(l1.size(), 7);

    std::stringstream out2;
    out2 << l1;
    EXPECT_STREQ(out2.str().c_str(), " 6 6 6 6 0 0 0 ");

    l1.resize(3);
    EXPECT_EQ(l1.size(), 3);

    std::stringstream out3;
    out3 << l1;
    EXPECT_STREQ(out3.str().c_str(), " 6 6 6 ");
}

TEST(ListTest, ResizeInit)
{
    cd05::List<int> l1(4, 6);
    EXPECT_EQ(l1.size(), 4);

    std::stringstream out1;
    out1 << l1;
    EXPECT_STREQ(out1.str().c_str(), " 6 6 6 6 ");
    l1.resize(7, 1);
    EXPECT_EQ(l1.size(), 7);

    std::stringstream out2;
    out2 << l1;
    EXPECT_STREQ(out2.str().c_str(), " 6 6 6 6 1 1 1 ");

    l1.resize(3, 1);
    EXPECT_EQ(l1.size(), 3);

    std::stringstream out3;
    out3 << l1;
    EXPECT_STREQ(out3.str().c_str(), " 6 6 6 ");
}

TEST(ListTest, MaxSize)
{
    cd05::List<int> l1(1);
    EXPECT_EQ(l1.max_size(), 512409557603043072);

    cd05::List<double> l2(1);
    EXPECT_EQ(l2.max_size(), 461168601842738816);
}

TEST(ListTest, PushFront)
{
    cd05::List<int> l1(1, 1);
    l1.push_front(2);
    EXPECT_EQ(l1.size(), 2);

    std::stringstream out1;
    out1 << l1;
    EXPECT_STREQ(out1.str().c_str(), " 2 1 ");
}

TEST(ListTest, PopFront)
{
    cd05::List<int> l1(1, 1);
    l1.pop_front();
    EXPECT_EQ(l1.size(), 0);

    std::stringstream out1;
    out1 << l1;
    EXPECT_STREQ(out1.str().c_str(), " ");
}

TEST(ListTest, Insert)
{
    cd05::List<int> l1(3, 3);
    cd05::List<int>::iterator pos = l1.begin();
    ++pos;
    ++pos;
    l1.insert(pos, 1);
    EXPECT_EQ(l1.size(), 4);

    std::stringstream out1;
    out1 << l1;
    EXPECT_STREQ(out1.str().c_str(), " 3 3 1 3 ");
}

TEST(ListTest, InsertBegin)
{
    cd05::List<int> l1;
    cd05::List<int>::iterator pos = l1.begin();
    l1.insert(pos, 1);
    EXPECT_EQ(l1.size(), 1);

    std::stringstream out1;
    out1 << l1;
    EXPECT_STREQ(out1.str().c_str(), " 1 ");
}

TEST(ListTest, InsertN)
{
    cd05::List<int> l1(3, 3);
    cd05::List<int>::iterator pos = l1.begin();
    ++pos;
    ++pos;
    size_t s = 3;
    l1.insert(pos, s, 1);
    EXPECT_EQ(l1.size(), 6);

    std::stringstream out1;
    out1 << l1;
    EXPECT_STREQ(out1.str().c_str(), " 3 3 1 1 1 3 ");
}

TEST(ListTest, InsertNBegin)
{
    cd05::List<int> l1;
    cd05::List<int>::iterator pos = l1.begin();
    size_t s = 3;
    l1.insert(pos, s, 1);
    EXPECT_EQ(l1.size(), 3);

    std::stringstream out1;
    out1 << l1;
    EXPECT_STREQ(out1.str().c_str(), " 1 1 1 ");
}

TEST(ListTest, InsertRangeB)
{
    cd05::List<int> l1(6, 3);
    cd05::List<int>::iterator f = l1.begin();
    cd05::List<int>::iterator l = l1.begin();
    ++l;
    ++l;
    ++l;
    cd05::List<int> l2;
    cd05::List<int>::iterator pos = l2.begin();
    l2.insert(pos, f, l);
    EXPECT_EQ(l2.size(), 3);

    std::stringstream out1;
    out1 << l2;
    EXPECT_STREQ(out1.str().c_str(), " 3 3 3 ");
}

TEST(ListTest, InsertRange)
{
    cd05::List<int> l1(6, 3);
    cd05::List<int>::iterator f = l1.begin();
    cd05::List<int>::iterator l = l1.begin();
    ++l;
    ++l;
    ++l;
    cd05::List<int> l2(4, 4);
    cd05::List<int>::iterator pos = ++l2.begin();
    l2.insert(pos, f, l);
    EXPECT_EQ(l2.size(), 7);

    std::stringstream out1;
    out1 << l2;
    EXPECT_STREQ(out1.str().c_str(), " 4 3 3 3 4 4 4 ");
}

TEST(ListTest, InsertAfterBegin)
{
    cd05::List<int> l1;
    cd05::List<int>::iterator pos = l1.begin();

    pos = l1.insert_after(pos, 4);
    pos = l1.insert_after(pos, 1);
    pos = l1.insert_after(pos, 5);
    EXPECT_EQ(l1.size(), 3);

    std::stringstream out1;
    out1 << l1;
    EXPECT_STREQ(out1.str().c_str(), " 4 1 5 ");
}

TEST(ListTest, InsertAfterEnd)
{
    cd05::List<int> l1(7, 7);
    cd05::List<int>::iterator pos = l1.begin();
    ++pos;
    ++pos;
    ++pos;
    ++pos;
    ++pos;
    ++pos;
    l1.insert_after(pos, 4);
    EXPECT_EQ(l1.size(), 8);

    std::stringstream out1;
    out1 << l1;
    EXPECT_STREQ(out1.str().c_str(), " 7 7 7 7 7 7 7 4 ");
}

TEST(ListTest, InsertAfter)
{
    cd05::List<int> l1(3, 3);
    cd05::List<int>::iterator pos = l1.begin();
    ++pos;
    size_t s = 2;

    l1.insert_after(pos, s, 4);
    EXPECT_EQ(l1.size(), 5);

    std::stringstream out1;
    out1 << l1;
    EXPECT_STREQ(out1.str().c_str(), " 3 3 4 4 3 ");
}

TEST(ListTest, Remove)
{
    cd05::List<int> l1;
    l1.push_back(4);
    l1.push_back(2);
    l1.push_back(3);
    l1.push_back(2);
    l1.push_back(2);
    l1.push_back(6);
    l1.push_back(1);
    l1.remove(2);

    std::stringstream out1;
    out1 << l1;
    EXPECT_STREQ(out1.str().c_str(), " 4 3 6 1 ");
}

TEST(ListTest, Erase)
{
    cd05::List<int> l1(4, 4);
    cd05::List<int>::iterator pos = l1.begin();
    ++pos;
    ++pos;
    l1.insert(pos, 1);
    EXPECT_EQ(l1.size(), 5);

    std::stringstream out1;
    out1 << l1;
    EXPECT_STREQ(out1.str().c_str(), " 4 4 1 4 4 ");
    
    cd05::List<int>::iterator pos1 = l1.begin();
    ++pos1;
    ++pos1;
    l1.erase(pos1);

    EXPECT_EQ(l1.size(), 4);
    
    std::stringstream out2;
    out2 << l1;
    EXPECT_STREQ(out2.str().c_str(), " 4 4 4 4 ");
}

TEST(ListTest, EraseIt)
{
    cd05::List<int> l1(4, 4);
    cd05::List<int>::iterator pos = l1.begin();
    ++pos;
    ++pos;
    l1.insert(pos, 1);
    EXPECT_EQ(l1.size(), 5);

    std::stringstream out1;
    out1 << l1;
    EXPECT_STREQ(out1.str().c_str(), " 4 4 1 4 4 ");
    
    cd05::List<int>::iterator pos1 = l1.begin();
    cd05::List<int>::iterator pos2 = pos1;
    ++pos2;
    ++pos2;
    l1.erase(pos1, pos2);

    EXPECT_EQ(l1.size(), 3);
    
    std::stringstream out2;
    out2 << l1;
    EXPECT_STREQ(out2.str().c_str(), " 1 4 4 ");
}

TEST(ListTest, ConstIterator)
{
    typedef cd05::List<int> L;
    typedef L::const_iterator It;

    L l;
    L l1(4, 4);
    l.resize(3, 3);

    for (It it = l1.begin(); it != l1.end(); ++it) {
        EXPECT_EQ(*it, 4);
    }
    It it = l.begin();
    EXPECT_EQ(*it, 3);
    EXPECT_EQ(*++it, 3);
    EXPECT_EQ(*it, 3);
    EXPECT_EQ(*it++, 3);
    EXPECT_EQ(*it, 3);
    It it1 = l.begin();
    It it3 = l.begin();
    It it2 = l.begin();
    ++it2;
    EXPECT_EQ(it1 != it2, true);
    EXPECT_EQ(it1 == it3, true);
}

TEST(ListTest, Iterator)
{
    typedef cd05::List<int> L;
    typedef L::iterator It;

    L l;
    L l1(4, 4);
    l.resize(3, 3);

    for (It it = l1.begin(); it != l1.end(); ++it) {
        EXPECT_EQ(*it, 4);
    }
    It it = l.begin();
    EXPECT_EQ(*it, 3);
    EXPECT_EQ(*++it, 3);
    EXPECT_EQ(*it, 3);
    EXPECT_EQ(*it++, 3);
    EXPECT_EQ(*it, 3);
    It it1 = l.begin();
    It it3 = l.begin();
    It it2 = l.begin();
    ++it2;
    EXPECT_EQ(it1 != it2, true);
    EXPECT_EQ(it1 == it3, true);
}

TEST(ListTest, ConstRevereseIterator)
{
    typedef cd05::List<int> L;
    typedef L::const_reverse_iterator It;

    L l;
    L l1(4, 4);
    l.resize(3, 3);

    for (It it = l1.rbegin(); it != l1.rend(); ++it) {
        EXPECT_EQ(*it, 4);
    }
    It it = l.rbegin();
    EXPECT_EQ(*it, 3);
    EXPECT_EQ(*++it, 3);
    EXPECT_EQ(*it, 3);
    EXPECT_EQ(*it++, 3);
    EXPECT_EQ(*it, 3);
    It it1 = l.rbegin();
    It it3 = l.rbegin();
    It it2 = l.rbegin();
    ++it2;
    EXPECT_EQ(it1 != it2, true);
    EXPECT_EQ(it1 == it3, true);
}

TEST(ListTest, RevereseIterator)
{
    typedef cd05::List<int> L;
    typedef L::reverse_iterator It;

    L l;
    L l1(4, 4);
    l.resize(3, 3);

    for (It it = l1.rbegin(); it != l1.rend(); ++it) {
        EXPECT_EQ(*it, 4);
    }
    It it = l.rbegin();
    EXPECT_EQ(*it, 3);
    EXPECT_EQ(*++it, 3);
    EXPECT_EQ(*it, 3);
    EXPECT_EQ(*it++, 3);
    EXPECT_EQ(*it, 3);
    It it1 = l.rbegin();
    It it3 = l.rbegin();
    It it2 = l.rbegin();
    ++it2;
    EXPECT_EQ(it1 != it2, true);
    EXPECT_EQ(it1 == it3, true);
}

int
main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

