#ifndef __LIST_HPP__
#define __LIST_HPP__

#include <iostream>
#include <cassert>

namespace cd05 {

template <typename T> class List;
template <typename T> std::istream& operator>>(std::istream& in, List<T>& v);
template <typename T> std::ostream& operator<<(std::ostream& out, const List<T>& v);
template <typename T> bool operator==(const List<T>& lhv, const List<T>& rhv);
template <typename T> bool operator<(const List<T>& lhv, const List<T>& rhv);

template <typename T>
class List
{
    template <typename T1> friend std::istream& operator>>(std::istream& in, List<T1>& v);
    template <typename T1> friend std::ostream& operator<<(std::ostream& out, const List<T1>& v);
public:
    typedef T value_type;
    typedef value_type& reference;
    typedef const value_type& const_reference;
    typedef value_type* pointer;
    typedef const value_type* const_pointer;
    typedef std::ptrdiff_t difference_type;
    typedef std::size_t size_type;


private:
    struct Node {
        Node() {}
        Node(const_reference data, Node* next = NULL, Node* prev = NULL)
            : data_(data), next_(next), prev_(prev) { }
        ~Node() { data_.~T(); next_ = NULL; prev_ = NULL; }
        T data_;
        Node* next_;
        Node* prev_;
    };

public:

    /// const_iterator

    class const_iterator {
        friend List<T>;
    public:
        const_iterator();
        const_iterator(const const_iterator& rhv);
        ~const_iterator();
        const const_iterator& operator=(const const_iterator& rhv);
        bool operator==(const const_iterator& rhv) const;
        bool operator!=(const const_iterator& rhv) const;
        const Node* operator->() const;
        const_reference operator*() const;
        const_iterator& operator++();
        const_iterator operator++(int);
        const_iterator& operator--();
        const_iterator operator--(int);
    protected:
        explicit const_iterator(Node* ptr);
        Node* getPtr() const;
        void setPtr(Node* ptr);
    private:
        Node* ptr_; /// pointer to the current element
    };

    /// iterator

    class iterator : public const_iterator {
        friend List<T>;
    public:
        iterator();
        iterator(const iterator& rhv);
        ~iterator();
        const iterator& operator=(const iterator& rhv);
        bool operator!=(const iterator& rhv) const;
        reference operator*() const;
        Node* operator->() const; /// dereference
        iterator& operator++();
        iterator operator++(int);
    protected:
        explicit iterator(Node* ptr);
    };

    ///  const_reverse_iterator

    class const_reverse_iterator : public const_iterator {
        friend List<T>;
    public:
        const_reverse_iterator();
        const_reverse_iterator(const const_reverse_iterator& rhv);
        ~const_reverse_iterator();
        const const_reverse_iterator& operator=(const const_reverse_iterator& rhv);
        const Node* operator->() const;
        const_reference operator*() const;
        const_reverse_iterator& operator++();
        const_reverse_iterator operator++(int);
        const_reverse_iterator& operator--();
        const_reverse_iterator operator--(int);
    protected:
        explicit const_reverse_iterator(Node* ptr);
    };

    /// reverse_iterator

    class reverse_iterator : public const_reverse_iterator {
        friend List<T>;
    public:
        reverse_iterator();
        reverse_iterator(const reverse_iterator& rhv);
        ~reverse_iterator();
        const reverse_iterator& operator=(const reverse_iterator& rhv);
        Node* operator->() const;
        reference operator*() const;
    protected:
        explicit reverse_iterator(Node* ptr);
    };

public:
    List();
    List(const size_type size);
    List(const int size, const_reference value);
    List(const List& rhv);
    template <typename InputIterator>
    List(InputIterator f, InputIterator l);
    ~List();
    const List& operator=(const List& rhv);
    void swap(List& rhv);
    size_type size() const;
    size_type max_size() const;
    bool empty() const;
    void clear();
    void resize(const size_type size);
    void resize(const size_type size, const_reference init);

    /// insertion

    iterator insert(iterator pos, const_reference x);
    void insert(iterator pos, const size_t n, const_reference x);
    template <typename InputIt>
    void insert(iterator pos, InputIt f, InputIt l);


    iterator insert_after(iterator pos, const_reference x = T());
    void insert_after(iterator pos, size_t n, const_reference x);
    template <typename InputIterator>
    void insert_after(iterator pos, InputIterator f, InputIterator l);

    /// deletion

    iterator erase(iterator pos);
    iterator erase(iterator f, iterator l);
    void remove(const_reference x);

    reference front(); /// Front Insertion Sequence
    const_reference front() const; /// Front Insertion
    void push_front(const_reference value); /// Front Insertion
    void pop_front(); /// Front Insertion
    reference back(); /// Back Insertion Sequence
    const_reference back() const; /// Back Insertion
    void push_back(const_reference value); /// Back Insertion
    void pop_back(); 

    iterator begin();
    iterator end();
    const_iterator begin() const;
    const_iterator end() const;
    reverse_iterator rbegin();
    reverse_iterator rend();
    const_reverse_iterator rbegin() const;
    const_reverse_iterator rend() const;
    
    bool operator==(const List& rhv) const;
    bool operator!=(const List& rhv) const;
    bool operator<(const List& rhv) const;
    bool operator<=(const List& rhv) const;
    bool operator>(const List& rhv) const;
    bool operator>=(const List& rhv) const;

private:
    Node* begin_;
    Node* end_;
    size_type size_;
};

} /// end of namespace cd05

#include <sources/List.cpp>

#endif /// __LIST_HPP__

