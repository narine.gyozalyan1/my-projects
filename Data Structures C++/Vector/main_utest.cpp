#include "headers/Vector.hpp"

#include <gtest/gtest.h>

TEST(VectorTest, DefaultConstSize)
{
    cd05::Vector<double> v;
    EXPECT_EQ(v.size(), 0);
}

TEST(VectorTest, StreamOperators)
{
    cd05::Vector<int> v(5);
    std::stringstream in("1 2 3 4 5");
    in >> v;
    EXPECT_EQ(v.size(), 5);
    for (int i = 0; i < 5; ++i) {
        EXPECT_EQ(v[i], i + 1);
    }

    std::stringstream out;
    out << v;
    EXPECT_STREQ(out.str().c_str(), "{ 1 2 3 4 5 }");
}

TEST(VectorTest, Copy)
{
    cd05::Vector<int> v(5);
    std::stringstream in("1 2 3 4 5");
    in >> v;
    EXPECT_EQ(v.size(), 5);
    for (int i = 0; i < 5; ++i) {
        EXPECT_EQ(v.get(i), i + 1);
    }
    cd05::Vector<int> v1 = v;
    //cd05::Vector<int> v1(v);
    v1[2] = 4;
    std::stringstream out;
    out << v1;
    EXPECT_STREQ(out.str().c_str(), "{ 1 2 4 4 5 }");
}

TEST(VectorTest, RangeConstructor)
{
    cd05::Vector<int> v(5);
    std::stringstream in("1 2 3 4 5");
    in >> v;
    
    cd05::Vector<int> v1(v.begin(), v.begin() + 2);
    
    EXPECT_EQ(v1.size(), 2);

    std::stringstream out;
    out << v1;
    EXPECT_STREQ(out.str().c_str(), "{ 1 2 }");
}

TEST(VectorTest, Assignment)
{
    cd05::Vector<int> v(5);
    std::stringstream in("1 2 3 4 5");
    in >> v;
    EXPECT_EQ(v.size(), 5);
    for (int i = 0; i < 5; ++i) {
        EXPECT_EQ(v.get(i), i + 1);
    }

    cd05::Vector<int> v1, v2;
    v1 = v2 = v; /// v1.operator=(v2.operator=(v));
    std::stringstream out;
    out << v1;
    EXPECT_STREQ(out.str().c_str(), "{ 1 2 3 4 5 }");
}

TEST(Vector, Operators)
{
    typedef cd05::Vector<int> V;
    V v1 (4, 4);
    V v2 (4, 4);
    std::stringstream out1;
    out1 << v1;
    EXPECT_STREQ(out1.str().c_str(), "{ 4 4 4 4 }");

    std::stringstream out2;
    out2 << v2;
    EXPECT_STREQ(out2.str().c_str(), "{ 4 4 4 4 }");
    
    EXPECT_EQ(v1 == v2, true);
    V v3 (4, 5);
    EXPECT_EQ(v1 < v3, true);
    V v4 (5, 4);
    EXPECT_EQ(v1 == v4, false);
    EXPECT_EQ(v4 < v3, false);
}

TEST(VectorTest, Reserve1)
{
    cd05::Vector<int> v(5);
    v.reserve(9);
    EXPECT_EQ(v.size(), 5);
    EXPECT_EQ(v.capacity(), 9);
}

TEST(VectorTest, Reserve2)
{
    cd05::Vector<int> v(5);
    v.reserve(3);
    EXPECT_EQ(v.size(), 5);
    EXPECT_EQ(v.capacity(), 5);
}

TEST(VectorTest, Resize1)
{
    cd05::Vector<int> v(5);
    std::stringstream in("1 2 3 4 5");
    in >> v;
    v.resize(7, 2);
    std::stringstream out;
    out << v;
    EXPECT_STREQ(out.str().c_str(), "{ 1 2 3 4 5 2 2 }");
    EXPECT_EQ(v.size(), 7);
}

struct A
{
///    A() { std::cout << "A::A()" << std::endl; }
///    A(const A&) { std::cout << "A::A(const A&)" << std::endl; }
///    ~A() { std::cout << "A::~A()" << std::endl; } 
///    const A& operator=(const A&) { std::cout<< "A::operator=" << std::endl; return *this; }
};

TEST(VectorTest, Resize11)
{
    cd05::Vector<A> v(5);
    A a;
    v.resize(7, a);
    EXPECT_EQ(v.size(), 7);
}

TEST(VectorTest, Resize2)
{
    cd05::Vector<int> v(5);
    std::stringstream in("1 2 3 4 5");
    in >> v;
    v.resize(3, 2);
    std::stringstream out;
    out << v;
    EXPECT_STREQ(out.str().c_str(), "{ 1 2 3 }");
    EXPECT_EQ(v.size(), 3);
}

TEST(VectorTest, Resize12)
{
    cd05::Vector<A> v(5);
    A a;
    v.resize(3, a);
    EXPECT_EQ(v.size(), 3);
}

TEST(VectorTest, PushBack)
{
    cd05::Vector<int> v(5);
    std::stringstream in("1 2 3 4 5");
    in >> v;
    v.push_back(6);
    std::stringstream out;
    out << v;
    EXPECT_STREQ(out.str().c_str(), "{ 1 2 3 4 5 6 }");
    EXPECT_EQ(v.size(), 6);
}

TEST(VectorTest, PopBack)
{
    cd05::Vector<int> v(5);
    std::stringstream in("1 2 3 4 5");
    in >> v;
    v.pop_back();
    std::stringstream out;
    out << v;
    EXPECT_STREQ(out.str().c_str(), "{ 1 2 3 4 }");
    EXPECT_EQ(v.size(), 4);
}

TEST(VectorTest, Insert1)
{
    typedef cd05::Vector<int> V;
    typedef V::iterator It;
    V v(5);
    std::stringstream in("1 2 3 4 5");
    in >> v;
    It it = v.begin() + 3;
    v.insert(it, 6);
    std::stringstream out;
    out << v;
    EXPECT_STREQ(out.str().c_str(), "{ 1 2 3 6 4 5 }");
    EXPECT_EQ(v.size(), 6);
}

TEST(VectorTest, Insert2)
{
    typedef cd05::Vector<int> V;
    typedef V::iterator It;
    V v(5);
    std::stringstream in("1 2 3 4 5");
    in >> v;
    It it = v.begin() + 3;
    size_t a = 3;
    v.insert(it, a, 7);
    std::stringstream out;
    out << v;
    EXPECT_STREQ(out.str().c_str(), "{ 1 2 3 7 7 7 4 5 }");
    EXPECT_EQ(v.size(), 8);
}

TEST(VectorTest, Insert3)
{
    typedef cd05::Vector<int> V;
    typedef V::iterator It;
    
    V v1(5);
    std::stringstream in("1 2 6 7 8");
    in >> v1;
    
    V v2(5);
    std::stringstream in1("3 4 5 11 12");
    in1 >> v2;
    
    It pos = v1.begin() + 2;
    
    It f = v2.begin();
    It e = v2.begin() + 2;
    
    v1.insert(pos, f, e);
    std::stringstream out;
    out << v1;
    EXPECT_STREQ(out.str().c_str(), "{ 1 2 3 4 5 6 7 8 }");
    EXPECT_EQ(v1.size(), 8);
}

TEST(VectorTest, Erase1)
{
    typedef cd05::Vector<int> V;
    typedef V::iterator It;
    
    V v(5);
    std::stringstream in("1 2 3 4 5");
    in >> v;
    
    It pos = v.begin() + 2;
    
    v.erase(pos); 
    std::stringstream out;
    out << v;
    EXPECT_STREQ(out.str().c_str(), "{ 1 2 4 5 }");
    EXPECT_EQ(v.size(), 4);
    EXPECT_EQ(v.capacity(), 5);

    V v1(5);
    std::stringstream in1("1 2 3 4 5");
    in1 >> v1;
    
    It pos1 = v1.begin() + 4;
    
    v1.erase(pos1);
    std::stringstream out1;
    out1 << v1;
    EXPECT_STREQ(out1.str().c_str(), "{ 1 2 3 4 }");
    EXPECT_EQ(v1.size(), 4);
    EXPECT_EQ(v1.capacity(), 5);
}

TEST(VectorTest, Erase1A)
{
    typedef cd05::Vector<A> V;
    V v(2);
    v.erase(v.begin());

    EXPECT_EQ(v.size(), 1);
}

TEST(VectorTest, Erase2)
{
    typedef cd05::Vector<int> V;
    typedef V::iterator It;
    
    V v(7);
    std::stringstream in("1 2 3 4 5 6 7");
    in >> v;
    
    It f = v.begin() + 1;
    It l = v.begin() + 4;
    
    v.erase(f, l);
    std::stringstream out;
    out << v;
    EXPECT_STREQ(out.str().c_str(), "{ 1 5 6 7 }");
    EXPECT_EQ(v.size(), 4);
    EXPECT_EQ(v.capacity(), 7);
}

TEST(VectorTest, Clear)
{
    typedef cd05::Vector<int> V;
    
    V v(7);
    std::stringstream in("1 2 3 4 5 6 7");
    in >> v;
    
    v.clear();
    std::stringstream out;
    out << v;
    EXPECT_STREQ(out.str().c_str(), "{ }");
    EXPECT_EQ(v.size(), 0);
    EXPECT_EQ(v.capacity(), 7);
}

TEST(VectorTest, ConstIterator)
{
    typedef cd05::Vector<int> V;
    typedef V::const_iterator CIt;
    V v(5);
    std::stringstream in("1 2 3 4 5");
    in >> v;
    EXPECT_EQ(v.size(), 5);

    int i = 0;
    for (CIt it = v.begin(); it != v.end(); ++it) {
        EXPECT_EQ(*it, ++i);
    }
}

TEST(VectorTest, Iterator)
{
    typedef cd05::Vector<int> V;
    typedef V::iterator It;
    V v(5);
    std::stringstream in("1 2 3 4 5");
    in >> v;
    EXPECT_EQ(v.size(), 5);
    int i = 0;

    for (It it = v.begin(); it != v.end(); ++it) {
        EXPECT_EQ(*it, ++i);
    }
    It it1 = v.begin();
    It it2 = ++it1;
    EXPECT_EQ(*it2, 2);
}

TEST(VectorTest, ConstReverseIterator)
{
    typedef cd05::Vector<int> V;
    typedef V::const_reverse_iterator It;
    V v(5);
    std::stringstream in("5 4 3 2 1");
    in >> v;
    EXPECT_EQ(v.size(), 5);
    int i = 0;
    for (It it = v.rbegin(); it != v.rend(); ++it) {
        EXPECT_EQ(*it, ++i);
    }
}

TEST(VectorTest, ReverseIterator)
{
    typedef cd05::Vector<int> V;
    typedef V::reverse_iterator It;
    V v(5);
    std::stringstream in("5 4 3 2 1");
    in >> v;
    EXPECT_EQ(v.size(), 5);
    int i = 0;
    for (It it = v.rbegin(); it != v.rend(); ++it) {
        EXPECT_EQ(*it, ++i);
    }
}

TEST(VectorTest, MaxSize)
{
    cd05::Vector<int> v(5);
    std::stringstream in("1 2 3 4 5");
    in >> v;
    EXPECT_EQ(v.max_size(), 4611686018427387904);
}

TEST(VectorTest, Empty)
{
    cd05::Vector<int> v(5);
    std::stringstream in("1 2 3 4 5");
    in >> v;
    EXPECT_EQ(v.empty(), false);
    cd05::Vector<int> v1;
    EXPECT_EQ(v1.empty(), true);
}

int
main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

