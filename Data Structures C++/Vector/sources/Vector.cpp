#include "headers/Vector.hpp"

#include <cassert>
#include <cstddef>
#include <cmath>
#include <cstring>

namespace cd05 {

static const int RESERVE_COEFF = 2;

template <typename T>
std::istream&
operator>>(std::istream& in, Vector<T>& v)
{
    for (T* p = v.begin_; p != v.end_; ++p) {
        in >> *p;
    }
    return in;
}

template <typename T>
std::ostream&
operator<<(std::ostream& out, const Vector<T>& v)
{
    out << "{ ";
    for (T* p = v.begin_; p != v.end_; ++p) {
        out << *p << ' ';
    }
    out << '}';
    return out;
}

template <typename T>
Vector<T>::Vector()
    : begin_(NULL)
    , end_(NULL)
    , bufferEnd_(NULL)
{
}

template <typename T>
Vector<T>::Vector(const size_type size)
    : begin_(NULL)
    , end_(NULL)
    , bufferEnd_(NULL)
{
    resize(size);
}

template <typename T>
Vector<T>::Vector(const int size, const_reference initialValue)
    : begin_(NULL)
    , end_(NULL)
    , bufferEnd_(NULL)
{
    assert(size >= 0);
    resize(static_cast<size_type>(size), initialValue);
}

template <typename T>
Vector<T>::Vector(const Vector<T>& rhv)
    : begin_(reinterpret_cast<T*>(::operator new(rhv.capacity() * sizeof(T))))
    , end_(begin_ + rhv.size())
    , bufferEnd_(begin_ + rhv.capacity())
{
    for (size_type i = 0; i < size(); ++i) {
        begin_[i] = rhv.begin_[i];
    }
}

template <typename T>
template <typename InputIterator>
Vector<T>::Vector(InputIterator f, InputIterator l)
    : begin_(NULL)
    , end_(NULL)
    , bufferEnd_(NULL)
{
    size_type vSize = 0;
    for (InputIterator it = f; it != l; ++it) {
        ++vSize;
    }
    resize(vSize);

    ::memcpy(reinterpret_cast<void*>(&*begin()), reinterpret_cast<void*>(&*f), vSize * sizeof(T));
}

template <typename T>
Vector<T>::~Vector()
{
    if (begin_ != NULL) {
        ::operator delete [](begin_);
        begin_ = end_ = bufferEnd_= NULL;
    }
}

template <typename T>
const Vector<T>&
Vector<T>::operator=(const Vector<T>& rhv)
{
    if (&rhv == this) {
        return *this;
    }

    if (size() != rhv.size()) {
        if (begin_ != NULL) {
            ::operator delete [](begin_);
        }
        begin_ = new int[rhv.size()];
        end_ = begin_ + rhv.size();
    }
    for (size_type i = 0; i < size(); ++i) {
        begin_[i] = rhv.begin_[i];
    }
    return *this;
}

template <typename T>
typename Vector<T>::size_type
Vector<T>::size() const
{
    return end_ - begin_;
}

template <typename T>
typename Vector<T>::size_type
Vector<T>::max_size() const
{
    return ::pow(2, sizeof(pointer) * 8) / sizeof(T);
}

template <typename T>
bool
Vector<T>::empty() const
{
    return begin_ == end_;
}

template <typename T>
size_t
Vector<T>::capacity() const
{
    return bufferEnd_ - begin_;
}

template <typename T>
void
Vector<T>::set(const size_type index, const_reference value)
{
    assert(index >= 0 && index < size());
    begin_[index] = value;
}

template <typename T>
const T&
Vector<T>::get(const size_type index) const
{
    assert(index >= 0 && index < size());
    return begin_[index];
}

template <typename T>
typename Vector<T>::const_reference
Vector<T>::operator[](const size_type index) const
{
    assert(index < size());
    return begin_[index];
}

template <typename T>
typename Vector<T>::reference
Vector<T>::operator[](const size_type index)
{
    assert(index < size());
    return begin_[index];
}

template <typename T>
bool
Vector<T>::operator==(const Vector<T>& rhv) const
{
    if (size() != rhv.size()) {
        return false;
    }
    size_type vSize = size();
    for (size_type i = 0; i < vSize; ++i) {
        if (begin_[i] != rhv.begin_[i]) {
            return false;
        }
    }
    return true;
}

template <typename T>
bool
Vector<T>::operator!=(const Vector<T>& rhv) const
{
    return !(*this == rhv);
}

template <typename T>
bool
Vector<T>::operator<(const Vector<T>& rhv) const
{
    if (size() > rhv.size()) {
        return false;
    }
    size_type vSize = size();
    for (size_type i = 0; i < vSize; ++i) {
        if (begin_[i] >= rhv.begin_[i]) {
            return false;
        }
    }
    return true;
}

template <typename T>
bool
Vector<T>::operator>(const Vector<T>& rhv) const
{
    return rhv < *this;
}

template <typename T>
bool
Vector<T>::operator<=(const Vector<T>& rhv) const
{
    return !(*this > rhv);
}

template <typename T>
bool
Vector<T>::operator>=(const Vector<T>& rhv) const
{
    return !(*this < rhv);
}

template <typename T>
void
Vector<T>::reserve(const size_type n)
{
    if (n > capacity()) {
        T* temp = reinterpret_cast<T*>(::operator new(n * sizeof(T)));
        const size_type vSize = size();
        for (size_type i = 0; i < vSize; ++i) {
            temp[i] = begin_[i];
        }
        if (begin_ != NULL) {
            ::operator delete(begin_);
        }
        begin_ = temp;
        end_ = begin_ + vSize;
        bufferEnd_ = begin_ + n;
    }
}

template <typename T>
void
Vector<T>::resize(const size_type n, const_reference init)
{
    const size_type vSize = size();

    if (n == vSize) {
        return;
    }
    if (n < vSize) {
        for (size_type i = vSize - 1; i >= n; --i) {
            begin_[i].~T();
        }
    }
    if (n > vSize) {
        reserve(n);
        for (size_type i = vSize; i < n; ++i) {
            begin_[i] = init;
        }
    }
    end_ = begin_ + n;
}

template <typename T>
void
Vector<T>::resize(const size_type n)
{
    const size_type vSize = size();

    if (n == vSize) {
        return;
    }
    if (n < vSize) {
        for (size_type i = vSize - 1; i >= n; --i) {
            begin_[i].~T();
        }
    }
    if (n > vSize) {
        reserve(n);
        for (size_type i = vSize; i < n; ++i) {
            new(&begin_[i]) T(); /// Placement new operator
        }
    }
    end_ = begin_ + n;
}

template <typename T>
void
Vector<T>::push_back(const_reference element)
{
    const size_type vSize = size();
    const size_type vCapacity = capacity();
    if (vSize == vCapacity) {
        reserve((0 == vSize) ? 1 : RESERVE_COEFF * vCapacity);
    }
    ++end_;
    begin_[vSize] = element;
}

template <typename T>
void
Vector<T>::pop_back()
{
    resize(size() - 1);
}

template <typename T>
typename Vector<T>::iterator
Vector<T>::insert(iterator pos, const_reference x)
{
    assert(pos >= begin() && pos <= end());
    const difference_type diff = pos.getPtr() - begin_;
    resize(size() + 1);
    pos.setPtr(diff + begin_);

    const difference_type d = end_ - pos.getPtr() - 1;
    ::memmove(reinterpret_cast<void*>(&*(pos + 1)), reinterpret_cast<void*>(&*(pos)), d * sizeof(T));
  
    *pos = x;

    return pos;
}

template <typename T>
void
Vector<T>::insert(iterator pos, const size_type n, const_reference x)
{
    assert(pos >= begin() && pos <= end());
    const difference_type diff = pos.getPtr() - begin_;
    resize(size() + n);
    pos.setPtr(diff + begin_);
    const difference_type d = end_ - n - pos.getPtr();
    ::memmove(reinterpret_cast<void*>(&*(pos + n)), reinterpret_cast<void*>(&*(pos)), d * sizeof(T));

    for (iterator it = pos; it != pos + n; ++it) {
        *it = x;
    }
}

template <typename T>
template <typename InputIterator>
void
Vector<T>::insert(iterator pos, InputIterator f, InputIterator l)
{
    assert(pos >= begin() && pos <= end());
    assert(f < l);
    const difference_type diff = pos.getPtr() - begin_;
    size_type vSize1 = 1;
    for (InputIterator it = f; it != l; ++it) {
        ++vSize1;
    }
    const size_type vSize = size();
    resize(vSize + (vSize1));
    pos.setPtr(diff + vSize1 + begin_);
    
    const difference_type d = end_ - pos.getPtr();
    ::memmove(reinterpret_cast<void*>(&*pos), reinterpret_cast<void*>(&*begin() + diff), d * sizeof(T));
    
    ::memcpy(reinterpret_cast<void*>(&*(begin() + diff)), reinterpret_cast<void*>(&*f), vSize1 * sizeof(T));
}

template <typename T>
typename Vector<T>::iterator
Vector<T>::erase(iterator pos)
{
    assert(pos >= begin() && pos < end());
    pos->~T();
    const difference_type diff = end_ - (pos.getPtr() + 1);
    ::memmove(reinterpret_cast<void*>(&(*(pos))), reinterpret_cast<void*>(&*(pos + 1)), static_cast<int>(diff) * sizeof(T));

    ::memset(reinterpret_cast<void*>(&*(--end_)), 0, sizeof(T));

    return pos;
}

template <typename T>
typename Vector<T>::iterator
Vector<T>::erase(iterator f, iterator l)
{
    assert(f >= begin() && f < end());
    assert(l > begin() && l <= end());
    assert(f < l);
    const difference_type d = l.getPtr() - f.getPtr();

    const difference_type d2 = end_ - f.getPtr() - d;

    ::memmove(reinterpret_cast<void*>(&*f), reinterpret_cast<void*>(&*(f + d)), d2 * sizeof(T));
    
    end_ -= d;
    
    ::memset(reinterpret_cast<void*>(&*(end_)), 0, (d + 1) * sizeof(T));

    return f;
}

template <typename T>
void
Vector<T>::clear()
{
    iterator f = begin();
    iterator l = end();
    erase(f, l);
}

template <typename T>
typename Vector<T>::const_reference
Vector<T>::front() const
{
    assert(!empty());
    return *begin_;
}

template <typename T>
typename Vector<T>::reference
Vector<T>::front()
{
    assert(!empty());
    return *begin_;
}

template <typename T>
typename Vector<T>::const_reference
Vector<T>::back() const
{
    assert(!empty());
    return *(begin_ + end_ - 1);
}

template <typename T>
typename Vector<T>::reference
Vector<T>::back()
{
    assert(!empty());
    return *(begin_ + end_ - 1);
}

template <typename T>
typename Vector<T>::const_iterator
Vector<T>::begin() const
{
    return const_iterator(begin_);
}

template <typename T>
typename Vector<T>::iterator
Vector<T>::begin()
{
    return iterator(begin_);
}

template <typename T>
typename Vector<T>::const_iterator
Vector<T>::end() const
{
    return const_iterator(end_);
}

template <typename T>
typename Vector<T>::iterator
Vector<T>::end()
{
    return iterator(end_);
}

template <typename T>
typename Vector<T>::const_reverse_iterator
Vector<T>::rbegin() const
{
    return const_reverse_iterator(end_ - 1);
}

template <typename T>
typename Vector<T>::reverse_iterator
Vector<T>::rbegin()
{
    return reverse_iterator(end_ - 1);
}

template <typename T>
typename Vector<T>::const_reverse_iterator
Vector<T>::rend() const
{
    return const_reverse_iterator(begin_ - 1);
}

template <typename T>
typename Vector<T>::reverse_iterator
Vector<T>::rend()
{
    return reverse_iterator(begin_ - 1);
}

/// const_iterator

template <typename T>
Vector<T>::const_iterator::const_iterator()
    : ptr_(NULL)
{
}

template <typename T>
Vector<T>::const_iterator::const_iterator(const const_iterator& rhv)
    : ptr_(rhv.ptr_)
{
}

template <typename T>
Vector<T>::const_iterator::const_iterator(pointer ptr)
    : ptr_(ptr)
{
}

template <typename T>
const typename Vector<T>::const_iterator&
Vector<T>::const_iterator::operator=(const const_iterator& rhv)
{
    ptr_ = rhv.ptr_;
    return *this;
}

template <typename T>
Vector<T>::const_iterator::~const_iterator()
{
    ptr_ = NULL;
}

template <typename T>
typename Vector<T>::pointer
Vector<T>::const_iterator::getPtr() const
{
    return ptr_;
}

template <typename T>
void
Vector<T>::const_iterator::setPtr(pointer ptr)
{
    ptr_ = ptr;
}

template <typename T>
bool
Vector<T>::const_iterator::operator==(const const_iterator& rhv) const
{
    return ptr_ == rhv.ptr_;
}

template <typename T>
bool
Vector<T>::const_iterator::operator!=(const const_iterator& rhv) const
{
    return ptr_ != rhv.ptr_;
}

template <typename T>
bool
Vector<T>::const_iterator::operator<(const const_iterator& rhv) const
{
    return (ptr_ - rhv.ptr_ < 0);
}

template <typename T>
bool
Vector<T>::const_iterator::operator>(const const_iterator& rhv) const
{
    return (ptr_ - rhv.ptr_ > 0);
}

template <typename T>
bool
Vector<T>::const_iterator::operator<=(const const_iterator& rhv) const
{
    return !(ptr_ > rhv.ptr_);
}

template <typename T>
bool
Vector<T>::const_iterator::operator>=(const const_iterator& rhv) const
{
    return !(ptr_ < rhv.ptr_);
}

template <typename T>
typename Vector<T>::const_reference
Vector<T>::const_iterator::operator*() const
{
    assert(ptr_ != NULL);
    return *ptr_;
}

template <typename T>
typename Vector<T>::const_pointer
Vector<T>::const_iterator::operator->() const
{
    return ptr_;
}

template <typename T>
typename Vector<T>::const_iterator&
Vector<T>::const_iterator::operator++()
{
    ++ptr_;
    return *this;
}

template <typename T>
typename Vector<T>::const_iterator
Vector<T>::const_iterator::operator++(int)
{
    const_iterator temp = *this;
    ++*this;
    return temp;
}

template <typename T>
typename Vector<T>::const_iterator&
Vector<T>::const_iterator::operator--()
{
    --ptr_;
    return *this;
}

template <typename T>
typename Vector<T>::const_iterator
Vector<T>::const_iterator::operator--(int)
{
    const_iterator temp = *this;
    --*this;
    return temp;
}

template <typename T>
const typename Vector<T>::const_iterator&
Vector<T>::const_iterator::operator+=(const size_type position)
{
    ptr_ += position;
    return *this;
}

template <typename T>
const typename Vector<T>::const_iterator&
Vector<T>::const_iterator::operator-=(const size_type position)
{
    ptr_ -= position;
    return *this;   
}

template <typename T>
typename Vector<T>::const_iterator
Vector<T>::const_iterator::operator+(const size_type position) const
{
    return const_iterator(ptr_ + position);
}

template <typename T>
typename Vector<T>::const_iterator
Vector<T>::const_iterator::operator-(const size_type position) const
{
    return const_iterator(ptr_ - position);
}

template <typename T>
typename Vector<T>::const_reference
Vector<T>::const_iterator::operator[](const size_type index) const
{
    assert(index >= 0 && index < Vector<T>::size());
    return *(ptr_ + index);
}

/// iterator

template <typename T>
Vector<T>::iterator::iterator()
    : const_iterator()
{
}

template <typename T>
Vector<T>::iterator::iterator(const iterator& rhv)
    : const_iterator(rhv)
{
}

template <typename T>
Vector<T>::iterator::iterator(pointer ptr)
    : const_iterator(ptr)
{
}

template <typename T>
const typename Vector<T>::iterator&
Vector<T>::iterator::operator=(const iterator& rhv)
{
    const_iterator::setPtr(rhv.const_iterator::getPtr());
    return *this;
}

template <typename T>
Vector<T>::iterator::~iterator()
{
}

template <typename T>
bool
Vector<T>::iterator::operator!=(const iterator& rhv) const
{
    return const_iterator::getPtr() != rhv.const_iterator::getPtr();
}

template <typename T>
typename Vector<T>::reference
Vector<T>::iterator::operator*() const
{
    assert(const_iterator::getPtr() != NULL);
    return *const_iterator::getPtr();
}

template <typename T>
typename Vector<T>::pointer
Vector<T>::iterator::operator->() const
{
    return const_iterator::getPtr();
}

template <typename T>
typename Vector<T>::reference
Vector<T>::iterator::operator[](const size_type index) const
{
    assert(index >= 0 && index < Vector<T>::size());
    return *(const_iterator::getPtr() + index);
}

template <typename T>
typename Vector<T>::iterator&
Vector<T>::iterator::operator++()
{
    const_iterator::setPtr(const_iterator::getPtr() + 1);
    return *this;
}

template <typename T>
typename Vector<T>::iterator
Vector<T>::iterator::operator++(int)
{
    iterator temp = *this;
    ++*this;
    return temp;
}

template <typename T>
typename Vector<T>::iterator
Vector<T>::iterator::operator+(const size_type position) const
{
    return iterator(const_iterator::getPtr() + position);
}

template <typename T>
typename Vector<T>::iterator
Vector<T>::iterator::operator-(const size_type position) const
{
    return iterator(const_iterator::getPtr() - position);
}

/// const_iterator_reverse

template <typename T>
Vector<T>::const_reverse_iterator::const_reverse_iterator()
    : const_iterator()
{
}

template <typename T>
Vector<T>::const_reverse_iterator::const_reverse_iterator(const const_reverse_iterator& rhv)
    : const_iterator(rhv)
{
}

template <typename T>
Vector<T>::const_reverse_iterator::const_reverse_iterator(pointer ptr)
    : const_iterator(ptr)
{
}

template <typename T>
const typename Vector<T>::const_reverse_iterator&
Vector<T>::const_reverse_iterator::operator=(const const_reverse_iterator& rhv)
{
    const_iterator::setPtr(rhv.const_iterator::getPtr());
    return *this;
}

template <typename T>
Vector<T>::const_reverse_iterator::~const_reverse_iterator()
{ }

template <typename T>
typename Vector<T>::const_reference
Vector<T>::const_reverse_iterator::operator*() const
{
    assert(const_iterator::getPtr() != NULL);
    return *const_iterator::getPtr();
}

template <typename T>
typename Vector<T>::const_pointer
Vector<T>::const_reverse_iterator::operator->() const
{
    return const_iterator::getPtr();
}

template <typename T>
typename Vector<T>::const_reverse_iterator&
Vector<T>::const_reverse_iterator::operator++()
{
    const_iterator::setPtr(const_iterator::getPtr() - 1);
    return *this;
}

template <typename T>
typename Vector<T>::const_reverse_iterator
Vector<T>::const_reverse_iterator::operator++(int)
{
    const_reverse_iterator temp = *this;
    --*this;
    return temp;
}

template <typename T>
typename Vector<T>::const_reverse_iterator&
Vector<T>::const_reverse_iterator::operator--()
{
    const_iterator::setPtr(const_iterator::getPtr() + 1);
    return *this;;
}

template <typename T>
typename Vector<T>::const_reverse_iterator
Vector<T>::const_reverse_iterator::operator--(int)
{
    const_reverse_iterator temp = *this;
    ++*this;
    return temp;
}

template <typename T>
const typename Vector<T>::const_reverse_iterator&
Vector<T>::const_reverse_iterator::operator+=(const size_type position)
{
    const_iterator::setPtr(const_iterator::getPtr() - position);
    return *this;
}

template <typename T>
const typename Vector<T>::const_reverse_iterator&
Vector<T>::const_reverse_iterator::operator-=(const size_type position)
{
    const_iterator::setPtr(const_iterator::getPtr() + position);
    return *this;   
}

template <typename T>
typename Vector<T>::const_reverse_iterator
Vector<T>::const_reverse_iterator::operator+(const size_type position)
{
    return const_iterator(const_iterator::getPtr() - position);
}

template <typename T>
typename Vector<T>::const_reverse_iterator
Vector<T>::const_reverse_iterator::operator-(const size_type position)
{
    return const_iterator(const_iterator::getPtr() + position);
}

template <typename T>
typename Vector<T>::const_reference
Vector<T>::const_reverse_iterator::operator[](const size_type index) const
{
    assert(index >= 0 && index < Vector<T>::size());
    return *(const_iterator::getPtr() + index);
}

/// reverse_iterator

template <typename T>
Vector<T>::reverse_iterator::reverse_iterator()
    : const_reverse_iterator()
{
}

template <typename T>
Vector<T>::reverse_iterator::reverse_iterator(const reverse_iterator& rhv)
    : const_reverse_iterator(rhv)
{
}

template <typename T>
Vector<T>::reverse_iterator::reverse_iterator(pointer ptr)
    : const_reverse_iterator(ptr)
{
}

template <typename T>
const typename Vector<T>::reverse_iterator&
Vector<T>::reverse_iterator::operator=(const reverse_iterator& rhv)
{
    const_iterator::setPtr(rhv.const_iterator::getPtr());
    return *this;
}

template <typename T>
Vector<T>::reverse_iterator::~reverse_iterator()
{ }

template <typename T>
typename Vector<T>::reference
Vector<T>::reverse_iterator::operator*() const
{
    assert(const_iterator::getPtr() != NULL);
    return *const_iterator::getPtr();
}

template <typename T>
typename Vector<T>::pointer
Vector<T>::reverse_iterator::operator->() const
{
    return const_iterator::getPtr();
}

template <typename T>
typename Vector<T>::reference
Vector<T>::reverse_iterator::operator[](const size_type index) const
{
    assert(index >= 0 && index < Vector<T>::size());
    return *(const_iterator::getPtr() + index);
}

}

