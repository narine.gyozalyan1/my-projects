#ifndef __VECTOR_HPP__
#define __VECTOR_HPP__

#include <iostream>
#include <cassert>

namespace cd05 {

template <typename T> class Vector;
template <typename T> std::istream& operator>>(std::istream& in, Vector<T>& v);
template <typename T> std::ostream& operator<<(std::ostream& out, const Vector<T>& v);

template <typename T>
class Vector
{
    template <typename T1> friend std::istream& operator>>(std::istream& in, Vector<T1>& v);
    template <typename T1> friend std::ostream& operator<<(std::ostream& out, const Vector<T1>& v);
public:
    typedef T value_type;
    typedef value_type& reference;
    typedef const value_type& const_reference;
    typedef value_type* pointer;
    typedef const value_type* const_pointer;
    typedef std::ptrdiff_t difference_type;
    typedef std::size_t size_type;

    /// const_iterator

    class const_iterator {
        friend Vector<T>;
    public:
        const_iterator();
        const_iterator(const const_iterator& rhv);
        ~const_iterator();
        const const_iterator& operator=(const const_iterator& rhv);
        bool operator==(const const_iterator& rhv) const;
        bool operator!=(const const_iterator& rhv) const;
        bool operator<(const const_iterator& rhv) const;
        bool operator>(const const_iterator& rhv) const;
        bool operator<=(const const_iterator& rhv) const;
        bool operator>=(const const_iterator& rhv) const;
        const_pointer operator->() const;
        const_reference operator*() const;
        const_iterator& operator++();
        const_iterator operator++(int);
        const_iterator& operator--();
        const_iterator operator--(int);
        const const_iterator& operator+=(const size_type position);
        const const_iterator& operator-=(const size_type position);
        const_iterator operator+(const size_type position) const;
        const_iterator operator-(const size_type position) const;
        const_reference operator[](const size_type index) const;
    protected:
        explicit const_iterator(pointer ptr);
        pointer getPtr() const;
        void setPtr(pointer ptr);
    private:
        pointer ptr_; /// pointer to the current element
    };

    /// iterator

    class iterator : public const_iterator {
        friend Vector<T>;
    public:
        iterator();
        iterator(const iterator& rhv);
        ~iterator();
        const iterator& operator=(const iterator& rhv);
        bool operator!=(const iterator& rhv) const;
        reference operator*() const;
        pointer operator->() const; /// dereference
        reference operator[](const size_type index) const; /// index operator
        iterator& operator++();
        iterator operator++(int);
        iterator operator+(const size_type position) const;
        iterator operator-(const size_type position) const;
    protected:
        explicit iterator(pointer ptr);
    };

    ///  const_reverse_iterator

    class const_reverse_iterator : public const_iterator {
        friend Vector<T>;
    public:
        const_reverse_iterator();
        const_reverse_iterator(const const_reverse_iterator& rhv);
        ~const_reverse_iterator();
        const const_reverse_iterator& operator=(const const_reverse_iterator& rhv);
        const_pointer operator->() const;
        const_reference operator*() const;
        const const_reverse_iterator& operator+=(const size_type position);
        const const_reverse_iterator& operator-=(const size_type position);
        const_reverse_iterator operator+(const size_type position);
        const_reverse_iterator operator-(const size_type position);
        const_reference operator[](const size_type index) const;
        const_reverse_iterator& operator++();
        const_reverse_iterator operator++(int);
        const_reverse_iterator& operator--();
        const_reverse_iterator operator--(int);
    protected:
        explicit const_reverse_iterator(pointer ptr);
    };

    /// reverse_iterator

    class reverse_iterator : public const_reverse_iterator {
        friend Vector<T>;
    public:
        reverse_iterator();
        reverse_iterator(const reverse_iterator& rhv);
        ~reverse_iterator();
        const reverse_iterator& operator=(const reverse_iterator& rhv);
        pointer operator->() const;
        reference operator*() const;
        reference operator[](const size_type index) const;
    protected:
        explicit reverse_iterator(pointer ptr);
    };

public:
    Vector();
    Vector(const size_type size);
    Vector(const int size, const_reference initialValue);
    Vector(const Vector<T>& rhv);
    template <typename InputIterator> Vector(InputIterator f, InputIterator l);
    ~Vector();
    
    const Vector<T>& operator=(const Vector<T>& rhv);
    void set(const size_type index, const_reference value);
    const T& get(const size_type index) const;
    const_reference operator[](const size_type index) const;
    reference operator[](const size_type index);
    bool operator==(const Vector<T>& rhv) const; 
    bool operator!=(const Vector<T>& rhv) const; 
    bool operator<(const Vector<T>& rhv) const;
    bool operator<=(const Vector<T>& rhv) const; 
    bool operator>(const Vector<T>& rhv) const;
    bool operator>=(const Vector<T>& rhv) const;
    
    size_type capacity() const;
    size_type size() const;
    size_type max_size() const;
    bool empty() const;
    void clear();
    void reserve(const size_type n);
    void resize(const size_type n, const_reference init);
    void resize(const size_type n);
    void push_back(const_reference element);
    void pop_back();
    
    const_reference front() const;
    reference front();
    const_reference back() const;
    reference back();

    const_iterator begin() const;
    iterator begin();
    const_iterator end() const;
    iterator end();
    const_reverse_iterator rbegin() const;
    reverse_iterator rbegin();
    const_reverse_iterator rend() const;
 
    /// insertion
    iterator insert(iterator pos, const_reference x);
    void insert(iterator pos, const size_type n, const_reference x);
    template <typename InputIterator> void insert(iterator pos, InputIterator f, InputIterator l);

    /// removal
    iterator erase(iterator pos);
    iterator erase(iterator f, iterator l);   reverse_iterator rend();

private:
    pointer begin_;
    pointer end_;
    pointer bufferEnd_;

};

} /// end of namespace cd05

#include <sources/Vector.cpp>

#endif /// __VECTOR_HPP__

