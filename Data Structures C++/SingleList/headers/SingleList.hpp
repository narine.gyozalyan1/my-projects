#ifndef __SINGLE_LIST_HPP__
#define __SINGLE_LIST_HPP__

#include <iostream>
#include <cassert>

namespace cd05 {

template <typename T> class SingleList;
template <typename T> std::istream& operator>>(std::istream& in, SingleList<T>& v);
template <typename T> std::ostream& operator<<(std::ostream& out, const SingleList<T>& v);

template <typename T>
class SingleList
{
    template <typename T1> friend std::istream& operator>>(std::istream& in, SingleList<T1>& v);
    template <typename T1> friend std::ostream& operator<<(std::ostream& out, const SingleList<T1>& v);
public:
    typedef T value_type;
    typedef value_type& reference;
    typedef const value_type& const_reference;
    typedef value_type* pointer;
    typedef const value_type* const_pointer;
    typedef std::ptrdiff_t difference_type;
    typedef std::size_t size_type;


private:
    struct Node {
        Node() {}
        Node(const_reference data, Node* next = NULL)
            : data_(data), next_(next) { }
        ~Node() { data_.~T(); next_ = NULL; } /// erb a kanchvum????
        T data_;
        Node* next_;
    };

public:

    /// const_iterator

    class const_iterator {
        friend SingleList<T>;
    public:
        const_iterator();
        const_iterator(const const_iterator& rhv);
        ~const_iterator();
        const const_iterator& operator=(const const_iterator& rhv);
        bool operator==(const const_iterator& rhv) const;
        bool operator!=(const const_iterator& rhv) const;
        bool operator<(const const_iterator& rhv) const;
        bool operator>(const const_iterator& rhv) const;
        bool operator<=(const const_iterator& rhv) const;
        bool operator>=(const const_iterator& rhv) const;
        const Node* operator->() const;
        const_reference operator*() const;
        const_iterator& operator++();
        const_iterator operator++(int);
    protected:
        explicit const_iterator(Node* ptr);
        Node* getPtr() const;
        void setPtr(Node* ptr);
    private:
        Node* ptr_; /// pointer to the current element
    };

    /// iterator

    class iterator : public const_iterator {
        friend SingleList<T>;
    public:
        iterator();
        iterator(const iterator& rhv);
        ~iterator();
        const iterator& operator=(const iterator& rhv);
        bool operator!=(const iterator& rhv) const;
        reference operator*() const;
        Node* operator->() const; /// dereference
        iterator& operator++();
        iterator operator++(int);
    protected:
        explicit iterator(Node* ptr);
    };

public:
    SingleList();
    SingleList(const size_type size);
    SingleList(const int size, const_reference value);
    SingleList(const SingleList& rhv);
    template <typename InputIterator>
    SingleList(InputIterator f, InputIterator l);
    ~SingleList();
    const SingleList& operator=(const SingleList& rhv);
    void swap(SingleList& rhv);
    size_type size() const;
    size_type max_size() const;
    bool empty() const;
    void clear();
    void resize(const size_type size);
    void resize(const size_type size, const_reference init);

    /// insertion

    iterator insert(iterator pos, const_reference x);
    void insert(iterator pos, const size_t n, const_reference x);
    template <typename InputIt>
    void insert(iterator pos, InputIt f, InputIt l);

    iterator insert_after(iterator pos);
    iterator insert_after(iterator pos, const_reference x);
    void insert_after(iterator pos, size_t n, const_reference x);
    template <typename InputIterator>
    void insert_after(iterator pos, InputIterator f, InputIterator l);

    /// deletion

    iterator erase(iterator pos);
    iterator erase(iterator f, iterator l);
    void remove(const_reference x);

    reference front(); /// Front Insertion Sequence
    const_reference front() const; /// Front Insertion
    void push_front(const_reference value); /// Front Insertion
    void pop_front(); /// Front Insertion
    void push_back(const_reference value); /// Front Insertion
    reference back(); /// Back Insertion Sequence
    const_reference back() const; /// Back Insertion

    iterator begin();
    iterator end();
    const_iterator begin() const;
    const_iterator end() const;
    
    bool operator==(const SingleList& rhv) const;
    bool operator!=(const SingleList& rhv) const;
    bool operator<(const SingleList& rhv) const;
    bool operator<=(const SingleList& rhv) const;
    bool operator>(const SingleList& rhv) const;
    bool operator>=(const SingleList& rhv) const;

    /// splice

    void splice(iterator pos, SingleList<T>& rhv);
    void splice(iterator pos, SingleList<T>& rhv, iterator f);
    void splice(iterator pos, SingleList<T>& rhv, iterator f, iterator l);

private:
    Node* begin_;
    Node* end_;
    size_type size_;
};

} /// end of namespace cd05

#include <sources/SingleList.cpp>

#endif /// __SINGLE_LIST_HPP__

