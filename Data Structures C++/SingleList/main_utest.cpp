#include "headers/SingleList.hpp"

#include <gtest/gtest.h>

TEST(SingleListTest, Constructor)
{
    cd05::SingleList<int> l(3);
    size_t s = 4;
    cd05::SingleList<int> l1(s, 6);
    
    std::stringstream out;
    out << l;
    EXPECT_STREQ(out.str().c_str(), " 0 0 0 ");

    std::stringstream out1;
    out1 << l1;
    EXPECT_STREQ(out1.str().c_str(), " 6 6 6 6 ");

//    cd05::SingleList<int> l2(l1);
    cd05::SingleList<int> l2(2, 5);
    l2 = l1;
    std::stringstream out2;
    out2 << l2;
    EXPECT_STREQ(out2.str().c_str(), " 6 6 6 6 ");
}

TEST(SingleListTest, RangeConstructor)
{
    size_t s = 5;
    cd05::SingleList<int> l1(s, 6);

    std::stringstream out1;
    out1 << l1;
    EXPECT_STREQ(out1.str().c_str(), " 6 6 6 6 6 ");
   
    int size = 5;
    std::vector<int> v(size, 3);
    std::vector<int>::iterator it1 = v.begin();
    std::vector<int>::iterator it2 = v.begin() + 3;

    cd05::SingleList<int> l2(it1, it2);
    std::stringstream out2;
    out2 << l2;
    EXPECT_STREQ(out2.str().c_str(), " 3 3 3 ");
}

TEST(SingleListTest, Operators)
{
    cd05::SingleList<int> l1(5, 5);
    cd05::SingleList<int> l2(3, 3);
    cd05::SingleList<int> l3(5, 5);

    EXPECT_EQ((l1 == l2), false);
    EXPECT_EQ((l1 == l3), true);
    EXPECT_EQ((l1 > l2), true);
}

TEST(SingleListTest, Size)
{
    cd05::SingleList<double> l;
    EXPECT_EQ(l.size(), 0);
    EXPECT_EQ(l.empty(), true);
    cd05::SingleList<int> l1(5);
    EXPECT_EQ(l1.size(), 5);
    EXPECT_EQ(l1.empty(), false);

    size_t s = 4;
    cd05::SingleList<int> l2(s, 6);
    EXPECT_EQ(l2.size(), 4);
}

TEST(SingleListTest, PushBack)
{
    cd05::SingleList<int> l;
    l.push_back(5);
    EXPECT_EQ(l.size(), 1);
    std::stringstream out;
    out << l;
    EXPECT_STREQ(out.str().c_str(), " 5 ");
    
    size_t s = 4;
    cd05::SingleList<int> l1(s, 6);
    l1.push_back(5);
    std::stringstream out1;
    out1 << l1;
    EXPECT_STREQ(out1.str().c_str(), " 6 6 6 6 5 ");
     EXPECT_EQ(l1.size(), 5);
}

TEST(SingleListTest, Clear)
{
    size_t s = 4;
    cd05::SingleList<int> l1(s, 6);
    EXPECT_EQ(l1.size(), 4);
    l1.clear();
    EXPECT_EQ(l1.size(), 0);

    cd05::SingleList<int> l2;
    EXPECT_EQ(l2.size(), 0);
    l1.clear();
    EXPECT_EQ(l1.size(), 0);
}

TEST(SingleListTest, Swap)
{
    cd05::SingleList<int> l1(3, 6);
    EXPECT_EQ(l1.size(), 3);

    std::stringstream out1;
    out1 << l1;
    EXPECT_STREQ(out1.str().c_str(), " 6 6 6 ");
    
    cd05::SingleList<int> l2(4, 1);
    EXPECT_EQ(l2.size(), 4);

    std::stringstream out2;
    out2 << l2;
    EXPECT_STREQ(out2.str().c_str(), " 1 1 1 1 ");
    
    l1.swap(l2);
    
    EXPECT_EQ(l1.size(), 4);
    std::stringstream out3;
    out3 << l1;
    EXPECT_STREQ(out3.str().c_str(), " 1 1 1 1 ");
 
    EXPECT_EQ(l2.size(), 3);
    std::stringstream out4;
    out4 << l2;
    EXPECT_STREQ(out4.str().c_str(), " 6 6 6 ");
}

TEST(SingleListTest, Resize)
{
    cd05::SingleList<int> l1(4, 6);
    EXPECT_EQ(l1.size(), 4);

    std::stringstream out1;
    out1 << l1;
    EXPECT_STREQ(out1.str().c_str(), " 6 6 6 6 ");
    l1.resize(7);
    EXPECT_EQ(l1.size(), 7);

    std::stringstream out2;
    out2 << l1;
    EXPECT_STREQ(out2.str().c_str(), " 6 6 6 6 0 0 0 ");

    l1.resize(3);
    EXPECT_EQ(l1.size(), 3);

    std::stringstream out3;
    out3 << l1;
    EXPECT_STREQ(out3.str().c_str(), " 6 6 6 ");
}

TEST(SingleListTest, ResizeInit)
{
    cd05::SingleList<int> l1(4, 6);
    EXPECT_EQ(l1.size(), 4);

    std::stringstream out1;
    out1 << l1;
    EXPECT_STREQ(out1.str().c_str(), " 6 6 6 6 ");
    l1.resize(7, 1);
    EXPECT_EQ(l1.size(), 7);

    std::stringstream out2;
    out2 << l1;
    EXPECT_STREQ(out2.str().c_str(), " 6 6 6 6 1 1 1 ");

    l1.resize(3, 1);
    EXPECT_EQ(l1.size(), 3);

    std::stringstream out3;
    out3 << l1;
    EXPECT_STREQ(out3.str().c_str(), " 6 6 6 ");
}

TEST(SingleListTest, MaxSize)
{
    cd05::SingleList<int> l1(1);
    EXPECT_EQ(l1.max_size(), 512409557603043072);

    cd05::SingleList<double> l2(1);
    EXPECT_EQ(l2.max_size(), 461168601842738816);
}

TEST(SingleListTest, PushFront)
{
    cd05::SingleList<int> l1(1, 1);
    l1.push_front(2);
    EXPECT_EQ(l1.size(), 2);

    std::stringstream out1;
    out1 << l1;
    EXPECT_STREQ(out1.str().c_str(), " 2 1 ");
}

TEST(SingleListTest, PopFront)
{
    cd05::SingleList<int> l1(1, 1);
    l1.pop_front();
    EXPECT_EQ(l1.size(), 0);

    std::stringstream out1;
    out1 << l1;
    EXPECT_STREQ(out1.str().c_str(), " ");
}

TEST(SingleListTest, Insert)
{
    cd05::SingleList<int> l1(3, 3);
    cd05::SingleList<int>::iterator pos = l1.begin();
    ++pos;
    ++pos;
    l1.insert(pos, 1);
    EXPECT_EQ(l1.size(), 4);

    std::stringstream out1;
    out1 << l1;
    EXPECT_STREQ(out1.str().c_str(), " 3 3 1 3 ");
}

TEST(SingleListTest, Inserttt)
{
    cd05::SingleList<int> l1(1, 7);
    cd05::SingleList<int> l2;
    l2.push_back(1);
    l2.push_back(2);
    l2.push_back(3);
    l2.push_back(4);
    l2.push_back(5);
    cd05::SingleList<int>::iterator it = l2.begin();
    ++it;
    ++it;
    l1.insert(l1.begin(), l2.begin(), it);
//    l1.insert(l1.begin(), 1);
//    EXPECT_EQ(l1.size(), 1);

    std::stringstream out1;
    out1 << l1;
    EXPECT_STREQ(out1.str().c_str(), " 1 2 7 ");
}

TEST(SingleListTest, InsertN)
{
    cd05::SingleList<int> l1(3, 3);
    cd05::SingleList<int>::iterator pos = l1.begin();
    ++pos;
    ++pos;
    size_t s = 3;
    l1.insert(pos, s, 1);
    EXPECT_EQ(l1.size(), 6);

    std::stringstream out1;
    out1 << l1;
    EXPECT_STREQ(out1.str().c_str(), " 3 3 1 1 1 3 ");
}

TEST(SingleListTest, InsertRange)
{
    cd05::SingleList<int> l1(6, 3);
    cd05::SingleList<int>::iterator f = l1.begin();
    cd05::SingleList<int>::iterator l = l1.begin();
    ++l;
    ++l;
    ++l;
    cd05::SingleList<int> l2(4, 4);
    cd05::SingleList<int>::iterator pos = ++l2.begin();
    l2.insert(pos, f, l);
    EXPECT_EQ(l2.size(), 7);

    std::stringstream out1;
    out1 << l2;
    EXPECT_STREQ(out1.str().c_str(), " 4 3 3 3 4 4 4 ");
}

TEST(SingleListTest, InsertAfter)
{
    cd05::SingleList<int> l1(3, 3);
    cd05::SingleList<int>::iterator pos = l1.begin();
    ++pos;
    size_t s = 2;

///    l1.insert_after(pos);
    l1.insert_after(pos, s, 4);
    EXPECT_EQ(l1.size(), 5);

    std::stringstream out1;
    out1 << l1;
    EXPECT_STREQ(out1.str().c_str(), " 3 3 4 4 3 ");
}

TEST(SingleListTest, Remove)
{
    cd05::SingleList<int> l1;
    l1.push_back(4);
    l1.push_back(2);
    l1.push_back(3);
    l1.push_back(2);
    l1.push_back(2);
    l1.push_back(6);
    l1.push_back(1);
    l1.remove(2);

    std::stringstream out1;
    out1 << l1;
    EXPECT_STREQ(out1.str().c_str(), " 4 3 6 1 ");
}

TEST(SingleListTest, Splice1)
{
    cd05::SingleList<int> l1(5, 5);
    cd05::SingleList<int> l2(7, 7);
    cd05::SingleList<int>::iterator pos = l1.begin();
    ++pos;
    ++pos;
    l1.splice(pos, l2);
    std::stringstream out1;
    out1 << l1;
    EXPECT_STREQ(out1.str().c_str(), " 5 5 7 7 7 7 7 7 7 5 5 5 ");
}

TEST(SingleListTest, Splice11)
{
    cd05::SingleList<int> l1(5, 5);
    cd05::SingleList<int> l2(7, 7);
    cd05::SingleList<int>::iterator pos = l1.begin();
    l1.splice(pos, l2);
    std::stringstream out1;
    out1 << l1;
    EXPECT_STREQ(out1.str().c_str(), " 7 7 7 7 7 7 7 5 5 5 5 5 ");
}

TEST(SingleListTest, Splice2)
{
    cd05::SingleList<int> l1(5, 5);
    cd05::SingleList<int> l2(7, 7);
    cd05::SingleList<int>::iterator pos = l1.begin();
    ++pos;
    ++pos;
    ++pos;

    cd05::SingleList<int>::iterator pos2 = l2.begin();
    ++pos2;
    l1.splice(pos, l2, pos2);
    std::stringstream out1;
    out1 << l1;
    EXPECT_STREQ(out1.str().c_str(), " 5 5 5 7 5 5 ");
}

TEST(SingleListTest, Splice3)
{
    cd05::SingleList<int> l1(5, 5);
    cd05::SingleList<int> l2(7, 7);
    cd05::SingleList<int>::iterator pos = l1.begin();
    ++pos;
    ++pos;
    ++pos;

    cd05::SingleList<int>::iterator f = l2.begin();
    cd05::SingleList<int>::iterator l = l2.begin();
    ++l;
    ++l;
    ++l;
    l1.splice(pos, l2, f, l);
    std::stringstream out1;
    out1 << l1;
    EXPECT_STREQ(out1.str().c_str(), " 5 5 5 7 7 7 5 5 ");
}

TEST(SingleListTest, Erase)
{
    cd05::SingleList<int> l1(4, 4);
    cd05::SingleList<int>::iterator pos = l1.begin();
    ++pos;
    ++pos;
    l1.insert(pos, 1);
    EXPECT_EQ(l1.size(), 5);

    std::stringstream out1;
    out1 << l1;
    EXPECT_STREQ(out1.str().c_str(), " 4 4 1 4 4 ");
    
    cd05::SingleList<int>::iterator pos1 = l1.begin();
    ++pos1;
    ++pos1;
    l1.erase(pos1);

    EXPECT_EQ(l1.size(), 4);
    
    std::stringstream out2;
    out2 << l1;
    EXPECT_STREQ(out2.str().c_str(), " 4 4 4 4 ");
}

TEST(SingleListTest, EraseIt)
{
    cd05::SingleList<int> l1(4, 4);
    cd05::SingleList<int>::iterator pos = l1.begin();
    ++pos;
    ++pos;
    l1.insert(pos, 1);
    EXPECT_EQ(l1.size(), 5);

    std::stringstream out1;
    out1 << l1;
    EXPECT_STREQ(out1.str().c_str(), " 4 4 1 4 4 ");
    
    cd05::SingleList<int>::iterator pos1 = l1.begin();
    cd05::SingleList<int>::iterator pos2 = pos1;
    ++pos2;
    ++pos2;
    l1.erase(pos1, pos2);

    EXPECT_EQ(l1.size(), 3);
    
    std::stringstream out2;
    out2 << l1;
    EXPECT_STREQ(out2.str().c_str(), " 1 4 4 ");
}


TEST(SingleListTest, ConstIterator)
{
    typedef cd05::SingleList<int> L;
    typedef L::const_iterator It;

    L l;
    L l1(4, 4);
    l.resize(3, 3);

    for (It it = l1.begin(); it != l1.end(); ++it) {
        EXPECT_EQ(*it, 4);
    }
    It it = l.begin();
    EXPECT_EQ(*it, 3);
    EXPECT_EQ(*++it, 3);
    EXPECT_EQ(*it, 3);
    EXPECT_EQ(*it++, 3);
    EXPECT_EQ(*it, 3);
    It it1 = l.begin();
    It it3 = l.begin();
    It it2 = l.begin();
    ++it2;
    EXPECT_EQ(it1 != it2, true);
    EXPECT_EQ(it1 == it3, true);
}

TEST(SingleListTest, Iterator)
{
    typedef cd05::SingleList<int> L;
    typedef L::iterator It;

    L l;
    L l1(4, 4);
    l.resize(3, 3);

    for (It it = l1.begin(); it != l1.end(); ++it) {
        EXPECT_EQ(*it, 4);
    }
    It it = l.begin();
    EXPECT_EQ(*it, 3);
    EXPECT_EQ(*++it, 3);
    EXPECT_EQ(*it, 3);
    EXPECT_EQ(*it++, 3);
    EXPECT_EQ(*it, 3);
    It it1 = l.begin();
    It it3 = l.begin();
    It it2 = l.begin();
    ++it2;
    EXPECT_EQ(it1 != it2, true);
    EXPECT_EQ(it1 == it3, true);
}

int
main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

