#include "headers/SingleList.hpp"

#include <cassert>
#include <cstddef>
#include <cmath>
#include <algorithm>

namespace cd05 {

template <typename T>
std::ostream&
operator<<(std::ostream& out, const SingleList<T>& l)
{
    out << ' ';
    typename cd05::SingleList<T>::Node* p = l.begin_;
    while (p != NULL) {
        out << p->data_ << ' ';
        p = p->next_;
    }
    return out;
}

template <typename T>
SingleList<T>::SingleList()
    : begin_(NULL)
    , end_(NULL)
    , size_(0)
{
}

template <typename T>
SingleList<T>::SingleList(const size_type size)
    : begin_(NULL)
    , end_(NULL)
    , size_(0)
{
    resize(size);
}

template <typename T>
SingleList<T>::SingleList(const int size, const_reference value)
    : begin_(NULL)
    , end_(NULL)
    , size_(0)
{
    assert(size >= 0);
    resize(static_cast<size_type>(size), value);
}

template <typename T>
SingleList<T>::SingleList(const SingleList& rhv)
    : begin_(NULL)
    , end_(NULL)
    , size_(0)
{
    Node* rhvCurrent = rhv.begin_;

    while (rhvCurrent != NULL) {
        push_back(rhvCurrent->data_);
        rhvCurrent = rhvCurrent->next_;
    }
}

template <typename T>
template <typename InputIterator>
SingleList<T>::SingleList(InputIterator f, InputIterator l)
    : begin_(NULL)
    , end_(NULL)
    , size_(0)
{
    while (f != l) {
        push_back(*f);
        ++f;
    }
}

template <typename T>
SingleList<T>::~SingleList()
{
    clear();
}

template <typename T>
const typename SingleList<T>::SingleList&
SingleList<T>::operator=(const SingleList& rhv)
{
    if (&rhv == this) {
        return *this;
    }
    size_t size1 = size();
    size_t size2 = rhv.size();

    while (size1 > size2) { 
        pop_front();
        --size1;
    }

    Node* current = begin_;
    Node* rhvCurrent = rhv.begin_;

    while (current != NULL && rhvCurrent != NULL) {
        current->data_ = rhvCurrent->data_;
        current = current->next_;
        rhvCurrent = rhvCurrent->next_;
    }
    while (rhvCurrent != NULL) {
        push_back(rhvCurrent->data_);
        rhvCurrent = rhvCurrent->next_;
    }
    return *this;
}

template <typename T>
void
SingleList<T>::clear()
{
    while (begin_ != NULL) {
        pop_front();
    }
}

template <typename T>
void
SingleList<T>::push_back(const_reference value)
{
    end_ = new Node(value, end_);
    if (NULL == begin_) {
        begin_ = end_;
        ++size_;
        return;
    }
    end_->next_->next_ = end_;
    end_->next_ = NULL;
    ++size_;
}

template <typename T>
void
SingleList<T>::push_front(const_reference value)
{
    begin_ = new Node(value, begin_);
    if(NULL == end_) {
        end_ = begin_;
    }
    ++size_;
}

template <typename T>
void
SingleList<T>::pop_front()
{
    assert(begin_ != NULL);

    if (NULL == begin_->next_) {
        delete begin_;
        begin_ = end_ = NULL;
        --size_;
        return;
    }
    Node* temp = begin_;
    begin_ = begin_->next_;
    delete temp;

    --size_;
}

template <typename T>
void
SingleList<T>::swap(SingleList<T>& rhv)
{
    Node* temp = begin_;
    Node* temp1 = end_;
    size_type s = size_;
    end_ = rhv.end_;
    begin_ = rhv.begin_;
    size_ = rhv.size_;
    rhv.begin_ = temp;
    rhv.end_ = temp1;
    rhv.size_ = s;
}

template <typename T>
bool
SingleList<T>::empty() const
{
    return NULL == begin_;
}

template <typename T>
typename SingleList<T>::size_type
SingleList<T>::size() const
{
    return size_;
}

template <typename T>
typename SingleList<T>::size_type
SingleList<T>::max_size() const
{
    return ::pow(2, sizeof(pointer) * 8) / (sizeof(T) + 24 + sizeof(size_t)); /// 16-y begin, end, 8y next 
}

template <typename T>
void
SingleList<T>::resize(const size_type s)
{
    if (0 == s) {
        clear();
        return;
    }

    for (; size_ > s;) {
        iterator it(end_);
        erase(it);
    }
    for (; size_ < s;) {
        push_back(T());
    }
}

template <typename T>
void
SingleList<T>::resize(const size_type s, const_reference init)
{
    if (0 == s) {
        clear();
        return;
    }

    while (size_ > s) {
        iterator it(end_);
        erase(it);
    }
    while (size_ < s) {
        push_back(init);
    }
}


template <typename T>
typename SingleList<T>::iterator
SingleList<T>::insert(iterator pos, const_reference x)
{
    if (NULL == begin_) {
        push_back(x);
        return begin();
    }

    iterator it = insert_after(pos, x);
    if (size() != 1) {
        std::swap(*pos, *it);
    }

    return pos;
}

template <typename T>
void
SingleList<T>::insert(iterator pos, const size_type n, const_reference x)
{   
    for (size_type i = 0; i < n; ++i) {
        pos = insert(pos, x);
    }
}

template <typename T>
template <typename InputIterator>
void
SingleList<T>::insert(iterator pos, InputIterator f, InputIterator l)
{
    assert(f.ptr_ != NULL);
    for (; f != l; ++f, ++pos) {
        pos = insert(pos, *f);
    }
}

template <typename T>
typename SingleList<T>::iterator
SingleList<T>::insert_after(iterator pos)
{
    assert(pos.ptr_ != NULL);
    Node* newNode = new Node(T(), pos->next_);
    pos->next_ = newNode;
    if (pos.ptr_ == end_) {
        end_ = newNode;
    }
    iterator it(newNode);
    ++size_;
    return it;
}

template <typename T>
typename SingleList<T>::iterator
SingleList<T>::insert_after(iterator pos, const_reference x)
{
    assert(pos.ptr_ != NULL);
    Node* newNode = new Node(x, pos->next_);
    pos->next_ = newNode;
    if (pos.ptr_ == end_) {
        end_ = newNode;
    }
    iterator it(newNode);
    ++size_;
    return it;
}

template <typename T>
void
SingleList<T>::insert_after(iterator pos, const size_type n, const_reference x)
{   
    assert(pos.ptr_ != NULL);
    for (size_type i = 0; i < n; ++i) {
        insert_after(pos, x);
        ++pos;
    }
}

template <typename T>
template <typename InputIterator>
void
SingleList<T>::insert_after(iterator pos, InputIterator f, InputIterator l)
{
    for (InputIterator it = f; it != l; ++it) {
        insert_after(pos, *it);
        ++pos;
    }
}

template <typename T>
typename SingleList<T>::iterator
SingleList<T>::erase(iterator pos)
{
    if (pos.getPtr() == begin_) {
        ++pos;
        pop_front();
        return pos;
    }
    iterator it = begin();
    for (; it.ptr_->next_ != pos.ptr_; ++it) { }
    it.ptr_->next_ = pos.ptr_->next_;
    if (end_ == pos.ptr_) {
        end_ = it.ptr_;
    }
    delete pos.ptr_;
    --size_;
    return iterator(it.ptr_->next_);
}

template <typename T>
typename SingleList<T>::iterator
SingleList<T>::erase(iterator f, iterator l)
{
    for (iterator it = f; it != l;) {
        it = erase(it);
    }
    return l;
}

template <typename T>
void
SingleList<T>::remove(const_reference value)
{
    for (iterator it = begin(); it != end(); ) {
        (*it == value) ? it = erase(it) : ++it; 
    }
}

template <typename T>
bool
SingleList<T>::operator==(const SingleList<T>& rhv) const
{
    if (size() != rhv.size()) {
        return false;
    }
    const_iterator it = rhv.begin();
    for (const_iterator it1 = begin(); it1 != end(); ++it1, ++it) {
        if (*it1 != *it) {
            return false;
        }
    }
    return true;
}

template <typename T>
bool
SingleList<T>::operator>(const SingleList<T>& rhv) const
{
    if (size() <= rhv.size()) {
        return false;
    }
    const_iterator it = begin();
    for (const_iterator it1 = rhv.begin(); it1 != rhv.end(); ++it1, ++it) {
        if (*it <= *it1) {
            return false;
        }
    }
    return true;
}

// splice

template <typename T>
void
SingleList<T>::splice(iterator pos, SingleList<T>& rhv)
{
    assert(pos.ptr_ != NULL);
    if (begin_ == pos.ptr_) {
        begin_ = rhv.begin_;
    } else {
        iterator it = begin();
        while (it.ptr_->next_ != pos.ptr_) {
            ++it;
        }
        it.ptr_->next_ = rhv.begin_;
    }
    rhv.end_->next_ = pos.ptr_;
    rhv.begin_ = NULL;
    rhv.end_ = NULL;
}

template <typename T>
void
SingleList<T>::splice(iterator pos, SingleList<T>& rhv, iterator f)
{
    assert(f.ptr_ != NULL);
    insert(pos, *f);
    rhv.erase(f);
}

template <typename T>
void
SingleList<T>::splice(iterator pos, SingleList<T>& rhv, iterator f, iterator l)
{
    assert(f.ptr_ != NULL && l.ptr_ != NULL && f.ptr_ != l.ptr_);
    insert(pos, f, l);
    rhv.erase(f, l);
}

template <typename T>
typename SingleList<T>::const_reference
SingleList<T>::front() const
{
    assert(!empty());
    return begin_->data_;
}

template <typename T>
typename SingleList<T>::reference
SingleList<T>::front()
{
    assert(!empty());
    return begin_->data_;
}

template <typename T>
typename SingleList<T>::const_reference
SingleList<T>::back() const
{
    assert(!empty());
    return end_->data_;
}

template <typename T>
typename SingleList<T>::reference
SingleList<T>::back()
{
    assert(!empty());
    return end_->data_;
}

template <typename T>
typename SingleList<T>::const_iterator
SingleList<T>::begin() const
{
    return const_iterator(begin_);
}

template <typename T>
typename SingleList<T>::iterator
SingleList<T>::begin()
{
    return iterator(begin_);
}

template <typename T>
typename SingleList<T>::const_iterator
SingleList<T>::end() const
{
    return const_iterator(NULL); /// NULL
}

template <typename T>
typename SingleList<T>::iterator
SingleList<T>::end()
{
    return iterator(NULL);
}

/// const_iterator

template <typename T>
SingleList<T>::const_iterator::const_iterator()
    : ptr_(NULL)
{
}

template <typename T>
SingleList<T>::const_iterator::const_iterator(const const_iterator& rhv)
    : ptr_(rhv.ptr_)
{
}

template <typename T>
SingleList<T>::const_iterator::const_iterator(Node* ptr)
    : ptr_(ptr)
{
}

template <typename T>
const typename SingleList<T>::const_iterator&
SingleList<T>::const_iterator::operator=(const const_iterator& rhv)
{
    ptr_ = rhv.ptr_;
    return *this;
}

template <typename T>
SingleList<T>::const_iterator::~const_iterator()
{
    ptr_ = NULL;
}

template <typename T>
typename SingleList<T>::Node*
SingleList<T>::const_iterator::getPtr() const
{
    return ptr_;
}

template <typename T>
void
SingleList<T>::const_iterator::setPtr(Node* ptr)
{
    ptr_ = ptr;
}

template <typename T>
bool
SingleList<T>::const_iterator::operator==(const const_iterator& rhv) const
{
    return ptr_ == rhv.ptr_;
}

template <typename T>
bool
SingleList<T>::const_iterator::operator!=(const const_iterator& rhv) const
{
    return this->ptr_ != rhv.ptr_;
}

template <typename T>
typename SingleList<T>::const_reference
SingleList<T>::const_iterator::operator*() const
{
    assert(ptr_ != NULL);
    return ptr_->data_;
}

template <typename T>
const typename SingleList<T>::Node*
SingleList<T>::const_iterator::operator->() const
{
    return ptr_;
}

template <typename T>
typename SingleList<T>::const_iterator&
SingleList<T>::const_iterator::operator++()
{
    assert(ptr_ != NULL);
    ptr_ = ptr_->next_;
    return *this;
}

template <typename T>
typename SingleList<T>::const_iterator
SingleList<T>::const_iterator::operator++(int)
{
    assert(ptr_ != NULL);
    const_iterator temp = *this;
    ptr_ = ptr_->next_;
    return temp;
}

/// iterator

template <typename T>
SingleList<T>::iterator::iterator()
    : const_iterator()
{
}

template <typename T>
SingleList<T>::iterator::iterator(const iterator& rhv)
    : const_iterator(rhv)
{
}

template <typename T>
SingleList<T>::iterator::iterator(Node* ptr)
    : const_iterator(ptr)
{
}

template <typename T>
const typename SingleList<T>::iterator&
SingleList<T>::iterator::operator=(const iterator& rhv)
{
    const_iterator::setPtr(rhv.const_iterator::getPtr());
    return *this;
}

template <typename T>
SingleList<T>::iterator::~iterator()
{
}

template <typename T>
bool
SingleList<T>::iterator::operator!=(const iterator& rhv) const
{
    return const_iterator::getPtr() != rhv.const_iterator::getPtr();
}

template <typename T>
typename SingleList<T>::reference
SingleList<T>::iterator::operator*() const
{
    assert(const_iterator::getPtr() != NULL);
    return const_iterator::getPtr()->data_;
}

template <typename T>
typename SingleList<T>::Node*
SingleList<T>::iterator::operator->() const
{
    return const_iterator::getPtr();
}

template <typename T>
typename SingleList<T>::iterator&
SingleList<T>::iterator::operator++()
{
    const_iterator::setPtr(const_iterator::getPtr()->next_);
    return *this;
}

template <typename T>
typename SingleList<T>::iterator
SingleList<T>::iterator::operator++(int)
{
    iterator temp = *this;
    const_iterator::setPtr(const_iterator::getPtr()->next_);
    return temp;
}

}

