#ifndef __STACK_HPP__
#define __STACK_HPP__

#include <iostream>

namespace mentor {

template <typename T>
class Stack
{
private:
    struct Node 
    {
        Node(T data = T(), Node* next = NULL) : data_(data), next_(next){}
        ~Node() {}
        T data_;
        Node* next_;
    };

public:
    Stack();
    ~Stack();
    void push(const T& item);
    void pop();
    T getTop() const;
    bool isEmpty() const;
    void print();

private:
    Node* top_;
};

} /// end of namespace mentor

#include <sources/Stack.cpp>

#endif /// __STACK_HPP__

