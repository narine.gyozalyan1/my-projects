#include "headers/Stack.hpp"

#include <gtest/gtest.h>

TEST(Stack, StackTest)
{
    mentor::Stack<int> s;
    s.push(10);
    std::cout << s.getTop() << std::endl; /// 10
    s.push(15);
    s.push(14);
    if (!s.isEmpty()) {
        s.pop();
        std::cout << s.getTop() << std::endl; /// 15
    }
}

int
main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

