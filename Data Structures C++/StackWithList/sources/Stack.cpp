#include "headers/Stack.hpp"

#include <cstdlib>

namespace mentor {

template <typename T>
Stack<T>::Stack()
    : top_(NULL)
{ }

template <typename T>
Stack<T>::~Stack()
{ }

template <typename T>
void
Stack<T>::push(const T& item)
{
    Node* newNode = new Node;
    newNode->data_ = item;
    newNode->next_ = top_;
    top_ = newNode;
}

template <typename T>
void
Stack<T>::pop()
{
    if (isEmpty()) {
        std::cout << "Error 2: Stack is empty!" << std::endl;
        ::exit(2);
    }
    Node* n = top_;
    top_ = top_->next_;
    delete n;
}

template <typename T>
T
Stack<T>::getTop() const
{
    if (isEmpty()) {
        std::cout << "Error 2: Stack is empty!" << std::endl;
        ::exit(2);
    }
    return top_->data_;
}

template <typename T>
bool
Stack<T>::isEmpty() const
{
    return NULL == top_;
}

template <typename T>
void
Stack<T>::print()
{
    Node* n = top_;
    while (n != NULL) {
        std::cout << n->data_ << " ";
        n = n->next_;
    }
    std::cout << std::endl;
}

}

