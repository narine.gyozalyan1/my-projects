#include "headers/Queue.hpp"

#include <list>
#include <cassert>
#include <cstddef>
#include <cmath>

namespace cd05 {

template <typename T1, typename Sequence1>
bool
operator==(const Queue<T1, Sequence1>& lhv, const Queue<T1, Sequence1>& rhv)
{
    return (static_cast<Sequence1>(lhv) == static_cast<Sequence1>(rhv));
}

template <typename T1, typename Sequence1>
bool
operator<(const Queue<T1, Sequence1>& lhv, const Queue<T1, Sequence1>& rhv)
{
    return (static_cast<Sequence1>(lhv) < static_cast<Sequence1>(rhv));
}

/*
template <typename T, typename Sequence>
bool
operator<(const_reference lhv, const_reference rhv)
{
    return Sequence::operator<(rhv);
}
*/
template <typename T, typename Sequence>
Queue<T, Sequence>::Queue()
    : Sequence()
{
}

template <typename T, typename Sequence>
Queue<T, Sequence>::Queue(const_reference rhv)
    : Sequence(rhv)
{
}

template <typename T, typename Sequence>
typename Queue<T, Sequence>::reference
Queue<T, Sequence>::operator=(const_reference rhv)
{
    Sequence::operator=(rhv);
    return *this;
}

template <typename T, typename Sequence>
void
Queue<T, Sequence>::push(const_reference rhv)
{
    Sequence::push_back(rhv);
}

template <typename T, typename Sequence>
void
Queue<T, Sequence>::pop()
{
    Sequence::pop_front();
}

template <typename T, typename Sequence>
typename Queue<T, Sequence>::reference
Queue<T, Sequence>::top()
{
    return *(Sequence::begin());
}

template <typename T, typename Sequence>
typename Queue<T, Sequence>::const_reference
Queue<T, Sequence>::top() const
{
    return *(Sequence::begin());
}

template <typename T, typename Sequence>
bool
Queue<T, Sequence>::empty() const
{
    return Sequence::empty();
}

template <typename T, typename Sequence>
typename Queue<T, Sequence>::size_type
Queue<T, Sequence>::size() const
{
    return Sequence::size();
}

}

