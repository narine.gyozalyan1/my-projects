#ifndef __QUEUE_HPP__
#define __QUEUE_HPP__

#include <list>
#include <iostream>

namespace cd05 {

template <typename T, typename Sequence = std::list<T> >
class Queue : private Sequence
{
    template <typename T1, typename Sequence1>
    friend bool operator==(const Queue<T1, Sequence1>& lhv, const Queue<T1, Sequence1>& rhv);
    template <typename T1, typename Sequence1>
    friend bool operator<(const Queue<T1, Sequence1>& lhv, const Queue<T1, Sequence1>& rhv); 
public:
    typedef T value_type;
    typedef value_type& reference;
    typedef const value_type& const_reference;
    typedef std::size_t size_type;

public:
    Queue();
    Queue(const_reference rhv);
    reference operator=(const_reference rhv);
    void push(const_reference rhv);
    void pop();
    reference top();
    const_reference top() const;
    bool empty() const;
    size_type size() const;
};

}

#include "sources/Queue.cpp"

#endif /// __QUEUE_HPP__

