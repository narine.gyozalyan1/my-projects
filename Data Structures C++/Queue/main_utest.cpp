#include "headers/Queue.hpp"

#include <gtest/gtest.h>

TEST(QueueTest, DefaultCunstructor)
{
    cd05::Queue<int> q;
    EXPECT_EQ(q.size(), 0);
}

TEST(QueueTest, CopyCunstructor)
{
    cd05::Queue<int> q;
    q.push(5);
    EXPECT_EQ(q.size(), 1);
    cd05::Queue<int> q1(q);
    EXPECT_EQ(q.size(), q1.size());
    EXPECT_EQ(q.top(), q1.top());
}

TEST(QueueTest, Assignment)
{
    cd05::Queue<int> q;
    q.push(5);
    EXPECT_EQ(q.size(), 1);
    cd05::Queue<int> q1;
    q1 = q;
    EXPECT_EQ(q.size(), q1.size());
    EXPECT_EQ(q.top(), q1.top());
}

TEST(QueueTest, Fuction)
{
    cd05::Queue<int> q;
    q.push(5);
    q.push(2);
    EXPECT_EQ(q.top(), 5);
    q.top() = 6;
    EXPECT_EQ(q.top(), 6);
    const cd05::Queue<int> q1 = q;
    EXPECT_EQ(q1.top(), 6);
}

TEST(QueueTest, Empty)
{
    cd05::Queue<int> q;
    
    q.push(5);
    q.push(2);
    EXPECT_EQ(q.top(), 5);
    EXPECT_EQ(q.empty(), false);

    cd05::Queue<int> q1;
    EXPECT_EQ(q1.empty(), true);
}

TEST(QueueTest, Size)
{
    cd05::Queue<int> q;
    
    q.push(5);
    q.push(2);
    EXPECT_EQ(q.size(), 2);
}

TEST(QueueTest, Operator)
{
    cd05::Queue<int> q1;
    q1.push(5);
    q1.push(2);

    cd05::Queue<int> q2;
    q2.push(5);
    q2.push(2);

    EXPECT_EQ(q1 == q2, true);

    cd05::Queue<int> q3;
    q3.push(3);

    EXPECT_EQ(q3 < q2, true);
    EXPECT_EQ(q1 < q2, false);
}

int
main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

