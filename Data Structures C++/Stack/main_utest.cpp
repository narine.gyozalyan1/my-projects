#include "headers/Stack.hpp"

#include <gtest/gtest.h>

TEST(StackTest, DefaultCunstructor)
{
    cd05::Stack<int> s;
    EXPECT_EQ(s.size(), 0);
}

TEST(StackTest, CopyCunstructor)
{
    cd05::Stack<int> s;
    s.push(5);
    EXPECT_EQ(s.size(), 1);
    cd05::Stack<int> s1(s);
    EXPECT_EQ(s.size(), s1.size());
    EXPECT_EQ(s.top(), s1.top());
}

TEST(StackTest, Assignment)
{
    cd05::Stack<int> s;
    s.push(5);
    EXPECT_EQ(s.size(), 1);
    cd05::Stack<int> s1;
    s1 = s;
    EXPECT_EQ(s.size(), s1.size());
    EXPECT_EQ(s.top(), s1.top());
}

TEST(StackTest, Fuction)
{
    cd05::Stack<int> s;
    s.push(5);
    s.push(2);
    EXPECT_EQ(s.top(), 2);
    s.top() = 6;
    EXPECT_EQ(s.top(), 6);
    const cd05::Stack<int> s1 = s;
    EXPECT_EQ(s1.top(), 6);
}

TEST(StackTest, Empty)
{
    cd05::Stack<int> s;
    
    s.push(5);
    s.push(2);
    EXPECT_EQ(s.top(), 2);
    EXPECT_EQ(s.empty(), false);

    cd05::Stack<int> s1;
    EXPECT_EQ(s1.empty(), true);
}

TEST(StackTest, Size)
{
    cd05::Stack<int> s;
    
    s.push(5);
    s.push(2);
    EXPECT_EQ(s.size(), 2);
}

TEST(StackTest, Operator)
{
    cd05::Stack<int> s1;
    s1.push(5);
    s1.push(2);

    cd05::Stack<int> s2;
    s2.push(5);
    s2.push(2);

    EXPECT_EQ(s1 == s2, true);

    cd05::Stack<int> s3;
    s3.push(3);

    EXPECT_EQ(s3 < s2, true);
    EXPECT_EQ(s1 < s2, false);
}

int
main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

