#include "headers/Stack.hpp"

#include <vector>
#include <cassert>
#include <cstddef>
#include <cmath>

namespace cd05 {

template <typename T1, typename Sequence1>
bool
operator==(const Stack<T1, Sequence1>& lhv, const Stack<T1, Sequence1>& rhv)
{
    return (static_cast<Sequence1>(lhv) == static_cast<Sequence1>(rhv));
}

template <typename T1, typename Sequence1>
bool
operator<(const Stack<T1, Sequence1>& lhv, const Stack<T1, Sequence1>& rhv)
{
    return (static_cast<Sequence1>(lhv) < static_cast<Sequence1>(rhv));
}

/*
template <typename T, typename Sequence>
bool
operator<(const_reference lhv, const_reference rhv)
{
    return Sequence::operator<(rhv);
}
*/
template <typename T, typename Sequence>
Stack<T, Sequence>::Stack()
    : Sequence()
{
}

template <typename T, typename Sequence>
Stack<T, Sequence>::Stack(const_reference rhv)
    : Sequence(rhv)
{
}

template <typename T, typename Sequence>
typename Stack<T, Sequence>::reference
Stack<T, Sequence>::operator=(const_reference rhv)
{
    Sequence::operator=(rhv);
    return *this;
}

template <typename T, typename Sequence>
void
Stack<T, Sequence>::push(const_reference rhv)
{
    Sequence::push_back(rhv);
}

template <typename T, typename Sequence>
void
Stack<T, Sequence>::pop()
{
    Sequence::pop_back();
}

template <typename T, typename Sequence>
typename Stack<T, Sequence>::reference
Stack<T, Sequence>::top()
{
    return Sequence::back();
}

template <typename T, typename Sequence>
typename Stack<T, Sequence>::const_reference
Stack<T, Sequence>::top() const
{
    return Sequence::back();
}

template <typename T, typename Sequence>
bool
Stack<T, Sequence>::empty() const
{
    return Sequence::empty();
}

template <typename T, typename Sequence>
typename Stack<T, Sequence>::size_type
Stack<T, Sequence>::size() const
{
    return Sequence::size();
}

}

