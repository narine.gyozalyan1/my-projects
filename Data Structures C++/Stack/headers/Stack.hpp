#ifndef __STACK_HPP__
#define __STACK_HPP__

#include <vector>
#include <iostream>

namespace cd05 {

template <typename T, typename Sequence = std::vector<T> >
class Stack : private Sequence
{
    template <typename T1, typename Sequence1>
    friend bool operator==(const Stack<T1, Sequence1>& lhv, const Stack<T1, Sequence1>& rhv);
    template <typename T1, typename Sequence1>
    friend bool operator<(const Stack<T1, Sequence1>& lhv, const Stack<T1, Sequence1>& rhv); 
public:
    typedef T value_type;
    typedef value_type& reference;
    typedef const value_type& const_reference;
    typedef std::size_t size_type;

public:
    Stack();
    Stack(const_reference rhv);
    reference operator=(const_reference rhv);
    void push(const_reference rhv);
    void pop();
    reference top();
    const_reference top() const;
    bool empty() const;
    size_type size() const;
};

}

#include "sources/Stack.cpp"

#endif /// __STACK_HPP__

